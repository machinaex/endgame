const ipc = require('electron').ipcRenderer;
const $ = require('jquery');
const fs = require('fs');

// on 'did-finish-load' aufgerufen
ipc.on('tileload', (event, message) =>
{

  $('body').on('drop', function(event) { event.preventDefault(); });
  $('body').on('dragstart', function(event) { event.preventDefault(); });


  // Fenster nach Dateiname umbenennen
  document.title = message.file;

  // true wenn eine Datei einen Tile auslösen kann
  let trigger = false;
  if (message.tile != undefined)
  {
    trigger = true;
  }
  console.log(message);

  // fügt Datei in Pagecontent hinzu
  let type = message.type;
  if (type == "text" || type ==  "excel" || type ==  "html" || type == "word")
  {
      loadHTML(message.path, trigger);
  } else if (type == "image")
  {
      loadImage(message.path, trigger);
  } else
  {
      console.error("Type Error in new BrowserWindow: " + type);
  }

  // adds Eventlisterner 'Click' to all Elements of 'trigger' class
  initTrigger(message.file, message.tile);
});

function loadHTML(_path, _trigger)
{
  let content = fs.readFileSync(_path, "utf8");
  if (_trigger)
  {
    content = addTriggerTag(content);
  } else {
    content = removeTriggerTag(content);
  }
  let node = $('<span>');
  node.attr('src', _path);
  node.html(content);
  node.appendTo('#pagecontent');
}

function loadImage(_path, _trigger)
{
  let img = $(_trigger ? '<img class="trigger">' : '<img>');
  img.attr('src', _path);
  img.appendTo('#pagecontent');
}

/////////////////////////////////////////////////////////////////
// helper functions
/////////////////////////////////////////////////////////////////

// on click callback
function triggerCallback(_file, _tile)
{
  let message = {'inventory':{'newTile':_tile}};
  ipc.send("forwarding",message);

  let message2 = {'fileexplorer':{'remove':_file}};
  ipc.send("forwarding",message2);
  console.log("Sent trigger message:");
  console.log(message);
}

// adds Eventlisterner 'Click' to all Elements of 'trigger' class
function initTrigger(_file, _tile)
{
  $(".trigger").each(function ()
  {
    let trigger = this;
    trigger.addEventListener("click", function()
    {
      triggerCallback(_file, _tile);
      $(".trigger").removeClass('trigger');
      $(".trigger").addClass('found');
    });
  });
}

// replce Tags in Text with HTML trigger-div
function addTriggerTag(_html)
{
  let text = _html.replaceAll('{{{', '<span class="trigger">');
  text = text.replaceAll('}}}', '</span>');
  return text;
}

// replce Tags in Text with HTML trigger-div
function removeTriggerTag(_html)
{
  console.log(_html);
  let text = _html.replaceAll('{{{', '');
  text = text.replaceAll('}}}', '');
  return text;
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
