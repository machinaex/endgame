// modified from the original example from https://github.com/zcbenz/nw-sample-apps/tree/master/file-explorer
//global.$ = $;

const $ = require("jquery");
//const electron = require('electron');
//const {remote} = require('electron');
const {Menu, BrowserWindow, MenuItem, shell} = require("electron").remote;

var abar = require('address_bar');
var folder_view = require('folder_view');
var fs = require('fs');
//console.log(main);

document.addEventListener('auxclick', callback, false);
  function callback(e) {
    console.log('not left mouse click');
    e.preventDefault();
    return
  }

// forbid drag and drop
$('body').on('drop', function(event) { event.preventDefault(); });
$('body').on('dragstart', function(event) { event.preventDefault(); });
//// arrays of files with special rules:
// default: dragable, click opens standard
var forbiddenTypes = ["folder"]; // can't be clicked
var forbiddenFiles = ["welcomeHome.file"];  // can't be dblclicked for default operations
var hiddenTypes = [];
var hiddenFiles = [".DS_Store"];
var openedWindows = [];
// the values will be executed if filename matches. (even when on forbidden Array)

var specialFiles = {
  /*
  "welcomeHome.file":"hiddenFiles.remove('hiddenfile.penis');folder.open('home');",
  "Kickstarter.jpg":"kickstarter();hiddenFiles.pushIfNotExist('hiddenfile.penis',hiddenFiles)",
  "body_black.png":"sendToRenderer('body_black was clicked. Do sth!')"
  */
};

// to be implemented:
var dropEvents = {
  "":"",
}

var drag = "";


let tilesByFiles;
let PATH;
let PERSON;
let LEVEL;

main.init = function (appdata, lan)
{
  console.log(appdata);
  LEVEL = ('level' in appdata) ? appdata.level :'TROLL';
  PERSON = appdata.person;
  $("#targetname").html("<b> "+appdata.person + "s Computer: </b>");
  PATH = appdata.path +'/'+appdata.person+'/'+lan.toLowerCase();
  tilesByFiles = JSON.parse(fs.readFileSync("./tilesByFiles.json", "utf8"));
  let tempTiles = [];
  tilesByFiles.forEach(function(item, i){
    if (item.targetname.toLowerCase() == PERSON.toLowerCase())
    {
      tempTiles.push(item);
    }
  });
  tilesByFiles = tempTiles;
  console.log(tilesByFiles);
  start();
}

main.receive = function(mydata){
  if('remove' in mydata)
  {
    console.log(mydata);
    tilesByFiles.splice(indexByValue(tilesByFiles, 'filename', mydata.remove), 1)
  }
  if ('level' in mydata) {
    LEVEL = mydata.level;
    for (var i=0; i<openedWindows.length; i++) {
      openedWindows[i].close();
    }
    openedWindows = [];
  }
  if ('closewindows' in mydata) {
    for (var i=0; i<openedWindows.length; i++) {
      try {
        openedWindows[i].close();
      } catch (e) {
        console.log('Window already closed');
      }

    }
    openedWindows = [];
  }

}

/*
////////////////////////////////////////////////
//// COMMUNICATION WITH RENDERER > /////////////

function sendToRenderer(input)
{
 electron.ipcRenderer.send("fromWindow",input); // in case we need to do sth that goes outside of this browser window:
}


//  listener for message/event "ping" from main (index.js)
electron.ipcRenderer.on('fromMain', (event, message) => {

  console.log(message);  // Prints whatever comes  in
  // with Async fromMain events.

})

//// < COMMUNICATION WITH RENDERER  ///////////
///////////////////////////////////////////////
*/


function dispatchEvent(name, msg)
{
  var event = new CustomEvent(
    name,
    {
      detail:{message: msg},
      bubbles: true,
      cancelable: true
    });
  document.dispatchEvent(event);
}

document.addEventListener("dragEvent", newMessageHandler, false);

function newMessageHandler(e)
{

  //console.log(e);
  console.log(e.detail.message);

}



// add remove method to Arrays to remove and add by value;
Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

// adds an element to the array if it does not already exist
Array.prototype.pushIfNotExist = function(element) {
    this.indexOf(element) === -1 ? this.push(element) : console.log("This item already exists");
};





// append default actions to menu for OSX
var initMenu = function () {
  try {
    var nativeMenuBar = new Menu();
    if (process.platform == "darwin") {
      nativeMenuBar.createMacBuiltin && nativeMenuBar.createMacBuiltin("FileExplorer");
    }
  } catch (error) {
    console.error(error);
    setTimeout(function () { throw error }, 1);
  }
};

var aboutWindow = null;
var App = {
  // show "about" window
  about: function () {
    var params = {toolbar: false, resizable: false, show: true, height: 150, width: 400};
    aboutWindow = new BrowserWindow(params);
    aboutWindow.loadURL('file://' + __dirname + '/about.html');
  },

  // change folder for sidebar links
  cd: function (anchor) {
    anchor = $(anchor);

    $('#sidebar li').removeClass('active');
    $('#sidebar i').removeClass('icon-white');

    anchor.closest('li').addClass('active');
    anchor.find('i').addClass('icon-white');

    this.setPath(PATH+'/'+anchor.attr('nw-path'));

  },

  // set path for file explorer
  setPath: function (path) {
    if (path.indexOf('~') == 0) {
      path = path.replace('~', process.env['HOME']);

    }
    this.folder.open(path);
    this.addressbar.set(path);
  }
};
// this is needed for drag and drop operations in order to
// get the drop data
document.ondragover = document.ondrop = (ev) => {
  ev.preventDefault();
  //console.log(ev);
}

function refreshAfterDrag()
{
  //refresh the view to see that sth moved.
  // THIS PART SEEMS FISHY, BUT WORKS ATM:
  var my_file_name = drag.replace(/^(.*)\//,"");
  var my_path = drag.replace(my_file_name,"");
  //console.log(my_path);
  global.App.setPath(my_path);
  // empty drag
  drag = "";

}


//$(document).ready(function() {
function start(){
  initMenu();

  var folder = new folder_view.Folder($('#files'));
  var addressbar = new abar.AddressBar($('#addressbar'));

  //folder.open(process.cwd());
  folder.open(PATH+"/home");
  //addressbar.set(process.cwd());
  addressbar.set(PATH+"/home");

  App.folder = folder;
  App.addressbar = addressbar;

  folder.on('navigate', function(dir, mime) {
    if (mime.type == 'folder') {
      addressbar.enter(mime);
    } else
    {
      ///////////////// !!!!! /////////////////
      // here we can control how to open a file that is not a dir !!!
      var type = mime.type;

      var my_file_name = mime.path.replace(/^(.*)\//,"");

      /////// exec code from SPECIALFILES > //////
      if(my_file_name in specialFiles)
      {
        eval(specialFiles[my_file_name]);
      }
      /////// < exec code from SPECIALFILES //////



      /////// filter FORBIDDENFILES > ////////////
      /////// && define standard behavior > //////
      if (forbiddenTypes.indexOf(type) === -1
        && forbiddenFiles.indexOf(my_file_name) === -1
        )
      {
          if (type == "music")
          {
            //open in new window:
            var win = new BrowserWindow({title:mime.path, width: 500, height: 500 });
            win.loadURL("file://"+process.cwd()+"/"+mime.path);
            /*console.log("MOVIES!!!");*/
            openedWindows.push(win);
          }
          else if (type == "penis")
          {
              // do weird stuff via unix shell: (DEMO)
              var shellUnix = require('shelljs');

              shellUnix.config.execPath = "/opt/local/bin/node";
              console.log(shellUnix.exec("ls").stdout);

          }
          else if (type == "image" || type == "word" || type == "text" || type ==  "excel" || type ==  "html")
          {
            let win = new BrowserWindow({x:100, y:100, width: 600, height: 500});
            win.setAlwaysOnTop(true);
            //win.webContents.openDevTools()
            win.loadURL('file://' + __dirname + '/index2.html');
            win.webContents.on('did-finish-load', () => {
              win.webContents.send('tileload', {
                'path':process.cwd()+"/"+mime.path,
                'type':type,
                'file':my_file_name,
                'tile':objectBy3Values(tilesByFiles,"filename",my_file_name, "targetname",PERSON, 'level',LEVEL)
              });
            });

            win.on('closed', () => {
              //console.log("CLOSED BROWSERWINDOW: " + index);
              //openedWindows.splice(index, 1);
            });

            openedWindows.push(win);
          }
      }
      else
      {
        console.log(my_file_name+" was filtered because its blacklisted for opening")
      }
      /////// < && define standard behavior //////
      /////// < filter FORBIDDENFILES ////////////

    }
  });

  addressbar.on('navigate', function(dir) {
    folder.open(dir);
  });

  // sidebar favorites
  $('[nw-path]').bind('click', function (event) {
    //event.preventDefault();
    App.cd(this);

  });
    // sidebar drag and drop
  $('[nw-path]').bind('drop', function (event) {
    event.preventDefault();

    moveTo = this.getAttribute("nw-path");
    newPath = moveTo+"/" +drag.replace(/^(.*)\//,"")


    dispatchEvent("dragEvent", drag +" dopped on "+ moveTo+" (this is an eventmsg)");

    // UNCOMMENT TO ENABLE DRAG AND DROP 4 FILES
    //fs.renameSync(drag, newPath);

    refreshAfterDrag();

  });
  ////////////////////////////////
      // random glitch effects:

    function randRange(data) {
          var newTime = data[Math.floor(data.length * Math.random())];
          return newTime;
    }

    function toggleSomething() {
          var timeArray = new Array(6000, 3000, 1500, 2250, 600, 3000, 1000, 3500);

          // do stuff, happens to use jQuery here (nothing else does)
          let rando = Math.random()*2;
          //console.log(rando);
          if(rando <= 0.33 )
          {
             $("#os-boxes").toggleClass("glitch1");
             $("#os-boxes").removeClass("glitch2");
             $("#os-boxes").removeClass("glitch3");
          }
          else if (rando > 0.33 && rando < 0.66) {
             $("#os-boxes").toggleClass("glitch2");
             $("#os-boxes").removeClass("glitch1");
             $("#os-boxes").removeClass("glitch3");
          }
          else if (rando > 0.66 && rando < 1) {
            $("#os-boxes").toggleClass("glitch3");
            $("#os-boxes").removeClass("glitch1");
            $("#os-boxes").removeClass("glitch2");
          }

          //console.log("glitch");

          clearInterval(timer);
          timer = setInterval(toggleSomething, randRange(timeArray));
    }

    var timer = setInterval(toggleSomething, 1000);
    // 1000 = Initial timer when the page is first loaded

    ////////////////////////////

};


// gets object based on 3 key value matches in one flat json
objectBy3Values = function(_json, _key, _value, _key2, _value2, _key3, _value3)
{
    for(var i=0; i < _json.length; i++) {
        if(_json[i][_key] === _value && _json[i][_key2].toLowerCase() === _value2.toLowerCase() && _json[i][_key3] === _value3) {
            return _json[i]["tile"];
        }
    }
    return undefined;
}

// gets index based on key value in one flat json
indexByValue = function(_json, _key, _value) {
    for(var i = 0; i < _json.length; i += 1) {
        if(_json[i][_key] === _value) {
          console.log("corresponding passage index: " + i);
            return i;
        }
    }
    console.log("no corresponding passage");
    return undefined;
}
