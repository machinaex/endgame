fs = require("fs")


let dateienImport = [
    {
        "filename": "1001_Mail_Marco_Alexander.docx",
        "name": "alexanderkassenbon",
        "targetname": "Marco Knochenwirt",
        "level": "TROLL"
    },
    {
        "filename": "kundenbeleg_sexytimes.docx",
        "name": "kassenbonknebel",
        "targetname": "Cornelius Werner",
        "level": "TROLL"
    },
    {
        "filename": "1001_Versanduebersicht.xlsx",
        "name": "knebeluebercrowd",
        "targetname": "Denise Schicker",
        "level": "TROLL"
    },
    {
        "filename": "MK_uebercrowd.xlsx",
        "name": "uebercrowdanrufe",
        "targetname": "Melanie Niedermeyer",
        "level": "TROLL"
    },
    {
        "filename": "Manuskript73.docx",
        "name": "anrufealexander",
        "targetname": "Arne Brauer",
        "level": "TROLL"
    },
    {
        "filename": "0902_Mail_Marco_Jenny.docx",
        "name": "bewegungjenny",
        "targetname": "Marco Knochenwirt",
        "level": "FIN"
    },
    {
        "filename": "0915_Mail_Jenny_Melanie.docx",
        "name": "jennyzahlung",
        "targetname": "Melanie Niedermeyer",
        "level": "FIN"
    },
    {
        "filename": "0905_Marco_Kontoauszug.docx",
        "name": "zahlungabhebung",
        "targetname": "Marco Knochenwirt",
        "level": "FIN"
    },
    {
        "filename": "0906_Beglaubigung.docx",
        "name": "abhebungsumme",
        "targetname": "Denise Schicker",
        "level": "FIN"
    },
    {
        "filename": "0901_Treibel_Rechnung.docx",
        "name": "summezwoelf",
        "targetname": "Cornelius Werner",
        "level": "FIN"
    },
    {
        "filename": "KatalogTreibel.xlsx",
        "name": "zwoelfbestellung",
        "targetname": "Denise Schicker",
        "level": "FIN"
    },
    {
        "filename": "0802_Mail_Arne_Denise.docx",
        "name": "bestellungtag",
        "targetname": "Arne Brauer",
        "level": "FIN"
    },
    {
        "filename": "0812_Chat_Arne_Melanie.docx",
        "name": "tagbewegung",
        "targetname": "Melanie Niedermeyer",
        "level": "FIN"
    }
]



let tilesImport = [
    {
        "name": "alexanderkassenbon",
        "match1": "alexander",
        "match2": "kassenbon",
        "textlinkeseite": "<key>Alexander</key> macht Buchhaltung im Center",
        "textrechteseite": "Cornelius hat <key>Beleg</key>",
        "endposx": 2,
        "endposy": 3
    },
    {
        "name": "kassenbonknebel",
        "match1": "kassenbon",
        "match2": "knebel",
        "textlinkeseite": "<key>Beleg</key> aus einem Sex-Shop",
        "textrechteseite": "Kassenbon über 5 <key>Knebelbälle</key>",
        "endposx": 3,
        "endposy": 5
    },
    {
        "name": "knebeluebercrowd",
        "match1": "knebel",
        "match2": "uebercrowd",
        "textlinkeseite": "5 <key>Knebelbälle</key> wurden versendet",
        "textrechteseite": "<key>übercrowd</key> gehört zu den Empfängern",
        "endposx": 5,
        "endposy": 5
    },
    {
        "name": "uebercrowdanrufe",
        "match1": "uebercrowd",
        "match2": "anrufe",
        "textlinkeseite": "Drohungen gegen <key>bercrowd</key> kommen von Bewegung",
        "textrechteseite": "Pakete und <key>Anrufe</key> werden koordiniert",
        "endposx": 6,
        "endposy": 4
    },
    {
        "name": "anrufealexander",
        "match1": "anrufe",
        "match2": "alexander",
        "textlinkeseite": "<key>Anrufe</key> sind mit Führung abgestimmt",
        "textrechteseite": "<key>Alexander</key> lektoriert Drohanruf-Manuskripte ",
        "endposx": 8,
        "endposy": 4
    },
    {
        "name": "bewegungjenny",
        "match1": "bewegung",
        "match2": "jenny",
        "textlinkeseite": "Die Bewegung unterstützt<key>Bewaffnung</key>",
        "textrechteseite": "<key>Jenny</key> hat persönlich überwiesen",
        "endposx": 9,
        "endposy": 3
    },
    {
        "name": "jennyzahlung",
        "match1": "jenny",
        "match2": "zahlung",
        "textlinkeseite": "<key>Jenny</key> hat persönlich überwiesen",
        "textrechteseite": "<k>Marco</key> hat Geld von der Bewegung erhalten",
        "endposx": 10,
        "endposy": 2
    },
    {
        "name": "zahlungabhebung",
        "match1": "zahlung",
        "match2": "abhebung",
        "textlinkeseite": "<k>Marco</key> hat Geld von der Bewegung erhalten",
        "textrechteseite": "Marco hat das Geld <key>abgehoben</key>",
        "endposx": 3,
        "endposy": 5
    },
    {
        "name": "abhebungsumme",
        "match1": "abhebung",
        "match2": "summe",
        "textlinkeseite": "Marco hat Geld <key>abgehoben</key>",
        "textrechteseite": "Cornelius hat <key>5.890 €</key> in bar erhalten",
        "endposx": 5,
        "endposy": 5
    },
    {
        "name": "summezwoelf",
        "match1": "summe",
        "match2": "zwoelf",
        "textlinkeseite": "Waffenrechnung über <key>5.890 €</key>",
        "textrechteseite": "<key>12 Waffen</key> bestellt",
        "endposx": 6,
        "endposy": 4
    },
    {
        "name": "zwoelfbestellung",
        "match1": "zwoelf",
        "match2": "bestellung",
        "textlinkeseite": "<key>12 Waffen</key> bestellt",
        "textrechteseite": "Denise hat <key>Bestellungen</key> aufgenommen",
        "endposx": 8,
        "endposy": 4
    },
    {
        "name": "bestellungtag",
        "match1": "bestellung",
        "match2": "tag",
        "textlinkeseite": "Denise hat <key>Bestellungen</key> aufgenommen",
        "textrechteseite": "Bestellung ist für <key>Tag X</key>",
        "endposx": 9,
        "endposy": 3
    },
    {
        "name": "tagbewegung",
        "match1": "tag",
        "match2": "bewegung",
        "textlinkeseite": "Bestellung ist für <key>Tag X</key>",
        "textrechteseite": "Die Bewegung unterstützt<key>Bewaffnung</key>",
        "endposx": 10,
        "endposy": 2
    }
]

// this is the function to parse from the tableexport into the usable/sendable json:
function parseTiles(inputArray)
{
  let tiles = [];

    tilesImport.forEach(function(item){

        let newTile;
        newTile =
        {
          "name": item.name,
          "endpos":{"x":item.endposx,"y":item.endposy},
          "match":[],
          "nodes":[{"name":"a", "text":item.textlinkeseite},{"name":"b","text":item.textrechteseite}]
        }

        if ("match1" in item){newTile.match.push(item.match1)}
        if ("match2" in item){newTile.match.push(item.match2)}
        if ("match3" in item){newTile.match.push(item.match3)}

        tiles.push(newTile);

    })

    return tiles;
}

function combineTables(_tiles, _dateien)
{
    _dateien.forEach(
      function(datei){
        datei['tile'] = objectByValue(_tiles, "name", datei.name);
        datei['tile']['level'] = datei.level;
      }
    )

    return _dateien;

}


// gets object based on key value in one flat json
objectByValue = function(_json, _key, _value)
{
    for(var i = 0; i < _json.length; i += 1) {
        if(_json[i][_key] === _value) {
          console.log("corresponding passage index: " + i);
            return _json[i];
        }
    }
    console.log("no corresponding passage");
    return -1;
}





let output = combineTables(parseTiles(tilesImport), dateienImport);
console.log(output);


fs.writeFile("tilesByFiles.json", JSON.stringify( output, null, 4), function(suc,err) {
  if(err) {
      return console.error(err);
  }
  else {
    console.log("file 'tilesByFiles.json' was successfully written");
  }
});
