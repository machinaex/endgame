const electron = require('electron');
const {app, BrowserWindow} = require('electron');

let mainWindow;

// Quit when all windows are closed.
app.on('window-all-closed', function() {
  if (process.platform != 'darwin')
    app.quit();
});


electron.ipcMain.on('fromWindow', (event, arg) => {

      console.log("main received from Window: "+arg)  // prints "ping"

      //event.sender.send('ping', 'ping') // this is for asynch

      if (arg == "body_black was clicked. Do sth!")
      {
        mainWindow.webContents.send('fromMain', 'ok. i did sth.\nI send you a message. That alright?');
      }
  })


// This method will be called when Electron has done everything
// initialization and ready for creating browser windows.
app.on('ready', function() {
  // Create the browser window.
  mainWindow = new BrowserWindow({width: 800, height: 600});

  // and load the index.html of the app.
  mainWindow.loadURL('file://' + __dirname + '/index.html');

  // Emitted when the window is closed.
  mainWindow.on('closed', function() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
    app.quit();
  });
});
