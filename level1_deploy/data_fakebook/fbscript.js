let config = {};
let names = [];

loadConfig();

searchfield = document.getElementById("searchfield");

// forbid drag and drop
$('body').on('dragstart', function(event) { event.preventDefault(); });


function loadConfig() {
    loadFile('../config.json', function(response) {
        // Parse JSON string into object
        config = JSON.parse(response);
        //console.log(config);
        //names = config.clickables.map(item => item.name);
        names = config.personen;
      
    });
}

function loadFile(path,callback) {
	console.log("loading JSON");
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', path, true);
    
    xobj.onreadystatechange = function() {
        
        if (xobj.readyState == 4)
        {
            callback(xobj.responseText);
        }
    };
    xobj.send(null);
}

function searchbarTriggered()
{
	console.log("searching for: " + searchfield.value);
	let exists = names.indexOf(searchfield.value) > -1;
	if (exists)
	{
		window.location.href = searchfield.value.replace(" ","_")+".html";
	}
}

// to Do: get list of choices from config.json
var demo1 = new autoComplete({
        selector: '#searchfield',
        minChars: 9,
        source: function(term, suggest){
            term = term.toLowerCase();
            //var choices = config.clickables.map(item => item.name);
            var choices = config.personen;
            var suggestions = [];
            for (i=0;i<choices.length;i++)
                if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
            suggest(suggestions);
        }
    });

var inDrag = "";


searchfield.addEventListener("keydown", function(e){
	if(event.key === 'Enter') 
	{
    	searchbarTriggered();        
	}
})
document.getElementById("searchbar_button").addEventListener("click",function(){
		searchbarTriggered();

		//console.log(indexOf(searchfield.value);
});
document.getElementById("searchbar_button").addEventListener("mouseover",function(){});
document.getElementById("searchbar_button").addEventListener("mouseout",function(){});

document.getElementById("notifications").addEventListener("click",function(){
    console.log("NOTIFICATIONS!!!");
});

document.getElementById("messages").addEventListener("click",function(){console.log("MESSAGES!!!")});
/*
document.getElementById("name").addEventListener("drag", function(e){
	//console.log(e.target.innerHTML + " is being dragged!")
	inDrag = e.target.innerText;
})
*/