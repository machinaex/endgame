var childProcess = require('child_process'); // for running the fbbuilder from within here

require('howler');

$ = require('jquery');

const swal = require('sweetalert');

var sound_3done = new Howl({
  src: ['./data_picAnalyse/level1_sechsgeschafft.wav'],
  volume: 0.7
});

var sound_right = new Howl({
  src: ['./data_picAnalyse/right.mp3'],
  volume: 0.3
});

$('body').on('drop', function(event) { event.preventDefault(); });
$('body').on('dragstart', function(event) { event.preventDefault(); });

var sound_logged = new Howl({
  src: ['./data_picAnalyse/level1_type.wav'],
  volume: 0.5
});

document.addEventListener('auxclick', callback, false);
  function callback(e) {
    console.log('not left mouse click');
    e.preventDefault();
    return
  }

const totalpoints = 210;

let startbild;

let config;

let myAppdata;

let unusedStartPics = ["1_2_3_A_bus.png","A_4_5_6_bus.png","7_8_9_B_bus.png","B_10_11_12_bus.png","13_14_15_C_bus.png","C_16_17_18_bus.png","19_20_21_D_bus.png","D_22_23_24_bus.png","25_26_27_E_bus.png","E_28_29_30_bus.png"];

main.init = function(input, len){

  myAppdata = input;
  console.log(myAppdata);

  try{
    config = require('./config.json'); //with path
    console.log(config.name);
  }
  catch(e){
    console.log("\n#####ERROR#####\nYour config file config.json is broken\n");
  }

/*
  // build HTMLs
  runScript('./fakebookbuilder/FromIniBuildFakebook.js', function (err) {
      if (err) throw err;
      console.log('finished running fakebookbuilder.js');
  });
//*/
  console.log(myAppdata.startbild);
  startbild = myAppdata.startbild;
  $(".inputoverlay").remove();
  loadInfoBoxes(); // uses startbild

  console.log("Ini done")

}



//main.init(); // comment out after dev!



document.body.onload = function(){
  console.log("html body tag loaded")
}



main.receive = function(mydata){

  if ("active" in mydata)
  {
    if (mydata.active == true)
    {
      main.send(
        {
          "questbook":
          {
            "command" : "checkbox",
            "tags" : ["level1 level1_alldone", "level1 level1_one"],
            "content" : ["Identifiziere alle Personen!", "Finde eine Person im sozialen Netzwerk und schreibe ihren Namen in eine der Box auf der linken Seite."]
          }

        }
      );
    }
    else if (mydata.active == false)
    {
      main.send(
        {
          "questbook":
          {
            "command" : "remove",
            "tags" : "level1"
          }

        }
      );
    }
  }

}

// other possible functions
// main.send(message);
// main.sendTCP(url,port,message);
// main.sendUDP(url,port,message);
// player.addAttribute(attribute);
// player.removeAttribute(attribute);
// player.setAppdata(json);


///////////////////////////////////////

let identifiedTargets = [];

document.body.addEventListener("dragover",function(e){allowDrop(e);});
document.body.addEventListener("drop",function(e){
  //console.log("DROP!!!");
  //console.log(e);
  var check = document.getElementById("iframeFakebook").contentWindow.inDrag;
  var droppedon = overHotspot(e.clientX, e.clientY);
  console.log("Dropped\n"+ check + "\non  coords "+ droppedon);

  // write Dropped Text into input of droppedon:
  var inpname = droppedon.replace(" ","_")+"_input";
  document.getElementById(inpname).value= check;


  // check Inputfield if correct:
  checkInputBox(inpname);

});

document.getElementById("btn_prev").addEventListener("click",function(){
  document.getElementById('iframeFakebook').contentWindow.history.back(-1);
});

document.getElementById("btn_next").addEventListener("click",function(){
  document.getElementById('iframeFakebook').contentWindow.history.forward(1);
});

document.body.addEventListener("click", function(e){
  //console.log(e);
  picWidth = document.getElementById("basepic").clientWidth;
  picHeight = document.getElementById("basepic").clientHeight;
  relX = parseInt(picWidth/e.clientX*100);
  relY = parseInt(picHeight/e.clientY*100);
  console.log(relX + " : " + relY);

  var target = overHotspot(e.clientX, e.clientY);
  if (target != undefined)
  {
    console.log("clicked on: " + target);
  }


});

function getPicWidth()
{
  return document.getElementById("basepic").clientWidth;
}

function getPicHeight()
{
  return document.getElementById("basepic").clientHeight;
}

function allowDrop(e)
{
  e.preventDefault();
}


function overHotspot(clientX, clientY)
{
  var clicked;
  picWidth = document.getElementById("basepic").clientWidth;
  picHeight = document.getElementById("basepic").clientHeight;

  config.clickables[startbild].forEach(function(hotspot, i)
    {
      var relX = picWidth/clientX*100;
      var relY = picHeight/clientY*100
      //console.log(hotspot + " " + i);

      if (relX < config.clickables[startbild][i].xol && relX > config.clickables[startbild][i].xur
        &&
        relY < config.clickables[startbild][i].yol && relY > config.clickables[startbild][i].yur)
      {
        clicked = config.clickables[startbild][i].name;
        //console.log(clicked);
      }

    });
  return clicked
}

let one_solved = false;
let three_solved = false;
let two_solved = false;

function checkInputBox(id)
{
  var valueshort = document.getElementById(id).value.replace(" ","").toLowerCase().trim();
  idshort = id.replace("_input","").replace("_","").toLowerCase();
  console.log("checking "+ idshort + " with val " + valueshort);

  if (thisNameExists(valueshort))
  {
     //document.getElementById(id).style.color = "darkgreen";

  }
  else
  {
     document.getElementById(id).style.color = "black";
  }

  // check if correct input
  if (idshort == valueshort)
  {
    player.score(totalpoints/3);
    sound_right.play();
    document.getElementById(id).style.color = "green";
    document.getElementById(id).style.fontSize = "1.3em";
    document.getElementById(id).disabled = true;
    console.log(id.replace("input","title"))
    let idtext = id.replace("input","title");
    document.getElementById(idtext).style.color = "rgba(0,255,0,1)";
    document.getElementById(idtext).innerHTML = "IDENTIFIZIERT<br>";

    // add to identifiedTargets if not already in:
    identifiedTargets.indexOf(idshort) === -1 ? identifiedTargets.push(valueshort) : console.log("This item already exists");

    // if at least 3 targets are identified lets make a note in the DB about our player being awesome:
    if (identifiedTargets.length > 2)
    {
      if (identifiedTargets.length > 5)
      {
        swal({
          title: "YOU ARE ÜBER AWESOME!",
          text: "Du hast dir was aus der Kaffeeecke verdient! \n Oder hilf deinen Kolleg_innen!",
          icon: "success",
          buttons: false,
          closeModal: false,
          closeOnClickOutside: false,
          closeOnEsc: false
        })
      }

      if (!three_solved)
      {
         player.score(totalpoints/3);
         three_solved = true;
         player.addAttribute("streberin_1");
      }

      main.send(
        {
          "questbook":
          {
            "command" : "set",
            "tags" : "level1_alldone",
            "status" : "checked"
          }
        }
      );
    }
    if (identifiedTargets.length % 3 == 0)
    {
        sound_3done.play();
        swal("WOW! Du bist schnell! Willst du mehr? \nDu kannst auch erstmal einen Kaffee holen oder eine_r Nachbar_in helfen...", {
          buttons:
          {
            ok:{
              text: "Ja, bitte!",
              value: "ok"
            },
            ne:{
              text: "Oh noez!",
              value: "ne"
            }
          }
        }).then((value) => {
          if(value == "ok")
          {
            loadNextStartBild();
          }
          else
          {
            swal(
              "Okay. Dann hol dir doch erstmal einen Kaffee, oder hilf deinem Team.",{
                button: "Weiter geht's!",
                closeOnClickOutside: false,
                closeOnEsc: false
              }).then((value) => {
                loadNextStartBild();
              }
            )
          }
        });
    }
    if (identifiedTargets.length > 2 && !two_solved)
    {
      two_solved = true;
      //player.score(-(totalpoints/3));
    }
    if (identifiedTargets.length > 0)
    {
      if (!one_solved)
      {
         //player.score(totalpoints/3);
         one_solved = true;
      }
      main.send(
        {
          "questbook":
          {
            "command" : "set",
            "tags" : "level1_one",
            "status" : "checked"
          }
        }
      );
    }
    if (identifiedTargets >= 3)
    {
      //player.score(totalpoints/3);
    }
  }
  else
  {
    sound_logged.play();
    // delete from identifiedTargets if in:
    var index = identifiedTargets.indexOf(idshort); if (index !== -1) { identifiedTargets.splice(index, 1)}

    // if our number of identifiedTargets falls below 3 lets delete the note in the player DB:
    if (identifiedTargets.length < 3 && three_solved)
    {
      three_solved = false;
      //player.score(-(totalpoints/3));
      player.removeAttribute("streberin_1");
    }
    if (identifiedTargets.length < 1 && one_solved)
    {
      one_solved = false;
      //player.score(-(totalpoints/3));
    }
    if (identifiedTargets.length < 2 && two_solved)
    {
      two_solved = false;
      //player.score(-(totalpoints/3));
    }
    //console.log("WRONG!!!");
  }
  console.log("got "+ identifiedTargets.length +" right already");
  console.log(identifiedTargets);

  // save all identified targets into appdata and write it to DB;
  if (myAppdata.identifiedTargets != identifiedTargets)
  {
     myAppdata.identifiedTargets = identifiedTargets;
     player.setAppdata(myAppdata);
  }

}


function thisNameExists(name)
{
  let exists = false;
  config.clickables[startbild].forEach(function(item){
          if (item.name.replace(" ","").toLowerCase() == name)
          {
            console.log("this person exists");
            exists = true;
          }

      });
  return exists;
}


function loadInfoBoxes()
{
  // Put inputs and other info htmls on Targets based on their config:

  config.clickables[startbild].forEach(function(p,i){

    pagecontent = document.getElementById("pagecontent");

    if(p.known)
    {
      console.log(p.name + " is known");

      let overlay = document.createElement("DIV");
      overlay.setAttribute("class", "inputoverlay");
      pagecontent.appendChild(overlay);

      let text = document.createElement("SPAN");
      text.style.fontFamily = "Consolas, Arial";
      text.style.color = "rgba(255,255,255,1)";
      text.style.backgroundColor = "rgba(0,0,0,0.5)";
      text.innerHTML = "<b>IDENTIFIZIERT:<br> "+p.name+" </b><br>";
      overlay.appendChild(text);

      text = document.createElement("A");
      text.style.fontFamily = "Consolas, Arial";
      text.style.fontSize = "1em";
      text.style.color = "rgba(0,0,250,1)";
      text.style.backgroundColor = "rgba(255,255,255,0.5)";
      text.innerHTML = "<< social media <br>profil gefunden >>";
      text.setAttribute("href", "data_fakebook/"+p.name.replace(" ","_")+".html");
      text.setAttribute("target", "ifbrowser");
      overlay.appendChild(text);

      console.log("opening "+p.name+"s fakebookprofile...");
      document.getElementById("basepic").src = "data_fakebook/"+startbild;
      document.getElementById("iframeFakebook").src  = "data_fakebook/"+p.name.replace(" ","_")+".html";
      unusedStartPics.splice(unusedStartPics.indexOf(startbild),1);
      console.log(unusedStartPics);

      pagecontent.insertBefore(overlay, pagecontent.firstChild);

      overlay.style.left = getPicWidth()*100/config.clickables[startbild][i].xol+2+'px';
      overlay.style.top = getPicHeight()*100/config.clickables[startbild][i].yol+2+'px';

    }
    else if (p.known == false)
    {
      // create target div and fill it with input and texts:
      console.log(i+ " "+ p.name + " is target");

      let overlay = document.createElement("DIV");
      overlay.setAttribute("class", "inputoverlay");
      pagecontent.appendChild(overlay);

      let text = document.createElement("SPAN");
      text.id = (p.name.replace(" ","_")+"_title");
      text.style.fontFamily = "Futura, Helvetica";
      text.style.color = "rgba(255,0,0,1)";
      text.style.backgroundColor = "rgba(0,0,0,0.5)";
      text.innerHTML = "<b>UNBEKANNT</b> <br>";
      overlay.appendChild(text);

      let newinput = document.createElement("TEXTAREA");
      newinput.setAttribute("id", p.name.replace(" ","_")+"_input");
      newinput.setAttribute("type", "text");
      newinput.style.width ="140px";
      newinput.style.height ="50px";
      newinput.style.fontSize ="1em";
      newinput.placeholder = "Hier Namen eintragen";
      overlay.appendChild(newinput);

      pagecontent.insertBefore(overlay, pagecontent.firstChild);

      newinput.addEventListener("input", function(e){
          checkInputBox(this.id);
          //checkTargetForName(e.target.value, this.id);
      });

      // position overlay object on page:
      overlay.style.left = getPicWidth()*100/config.clickables[startbild][i].xol+2+'px';
      overlay.style.top = getPicHeight()*100/config.clickables[startbild][i].yol+2+'px';

    }

  });
}

function loadNextStartBild()
{
  let next = unusedStartPics[Math.floor(Math.random() * unusedStartPics.length)]; // random from list
  startbild = next;
  //delete all contents from the left side:
  $(".inputoverlay").remove();
  // load new contents
  loadInfoBoxes();
}


function runScript(scriptPath, callback) {

    // keep track of whether callback has been invoked to prevent multiple invocations
    var invoked = false;

    var process = childProcess.fork(scriptPath);

    // listen for errors as they may prevent the exit event from firing
    process.on('error', function (err) {
        if (invoked) return;
        invoked = true;
        callback(err);
    });

    // execute the callback once the process has finished running
    process.on('exit', function (code) {
        if (invoked) return;
        invoked = true;
        var err = code === 0 ? null : new Error('exit code ' + code);
        callback(err);
    });

}

document.addEventListener("keydown",function(e){

  //console.log(e)
  if (e.key == "ArrowRight")
  {
 //   loadNextStartBild();
  }

});
