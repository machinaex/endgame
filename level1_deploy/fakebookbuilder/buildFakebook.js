const Mustache = require("mustache");
const fs = require("fs");

const templatepath = 'template.html';

if(process.argv[2] != undefined)
{
	var table = require("./"+process.argv[2]);
	console.log("loading .json file: "+process.argv+"\n")
}
else
{	
	console.log("no .json file argument provided.\nloading inputtable.json");
	var table = require("./inputtable.json");
}

const contentpath = "../data_fakebook/";
const basepath = "content/";

var template;




initTemplate(templatepath, function(r){
	
	content = prepareForMustache(table);

	content.forEach(function(person){

		let temp = r;
		console.log("\n//////////building////////////");
		console.log(person);

		temp = buildFakebook(temp, person);

		let filename = contentpath+person.name.replace(" ","_")+".html"

		writeFile(filename,temp)

	});


});








function prepareForMustache(json)
{
	// prepare content for parsing:

	var contentSorted = [];

	//console.log(content[0].name);

	json.forEach(function(item, i){

	var pos; // position of write
	var posPost; // position of writePosts

		if (item.name != undefined) // if row contains profiledata:
			{
				// correct paths	
				item.profilepic = item.profilepic;
				item.profileheader = item.profileheader;
				// start a list or sth:
				contentSorted.push(item);
				pos = contentSorted.length -1; // update position
				contentSorted[pos].inArray = i;
				contentSorted[pos].posts = [];
			}
		else if (item.comment == undefined) // if row contains postdata:
		{
			pos = contentSorted.length -1; // update position
			item.comments = [];

			if (item.postpic == undefined)
			{item.postpic = "nopic.png"}
			else
			{item.postpic = item.postpic}

			item.profilepic = contentSorted[pos].profilepic;

			contentSorted[pos].posts.push(item);
			posPost = contentSorted[pos].posts.length -1;
		}
		else // if row contains commentdata
		{
			pos = contentSorted.length -1; // update position
			posPost = contentSorted[pos].posts.length -1; // get position posts
			
			// add path version from commenter:
			item.commenterpath = item.commenter.replace(" ","_");
			// push the comment into the right post.comments array:
			contentSorted[pos].posts[posPost].comments.push(item);
		}

	});
	
	contentSorted.basepath = basepath;
/*
	contentSorted = contentSorted[0].posts

	// make it into list for mustache to iterate through:
	contentSorted = {list:contentSorted};
*/	
	//console.log("////////////////////////////////")
	//console.log(contentSorted);
	
	return contentSorted;
}






function initTemplate(filepath, callback){

	fs.readFile(filepath, 'utf8', function (err,data) {
	  if (err) {
	    return console.log(err);
	  }
	  //console.log(data);
	  callback(data);
	});

}







function writeFile(filename, content, callback)
{
	fs.writeFile(filename, content, function(err) {
		if(err) {
		    return console.log(err);
		}

		console.log("file " + filename + " was successfully written");
	});
}





function buildFakebook(template, json)
{
	return Mustache.render(template, json);
}




