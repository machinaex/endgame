////-----------------------------Power-Point-Karaoke-----------------------////
///---Todo---///
/*
- Satzzeichen mit extrahieren - Special delimiters
- Textteil in Rede Highlighten - textsatzproblem
- Layout
- Anbindung ans Framework
*/

//const fs = require('fs');

///---Variabls----////
var cnv;
var textblock,sentencebox;
var imgs = [];
var c,bg,frame,logo,menubar;
var speaches = [[""],[""]];
var redetext ="";
var sentences ="";
var sentencesArr =[""];
var sentencenumber = 0;
var speach=[""];
var speachArr=[""];
var imgcount = 7; // Hier die Anzahl der originalen Slides eintragen
var speachcount = 6; // Hier die Anzahl der Rededateien eintragen
var sentencescount = 6; //Hier die Anzahl der austauschbaren Sätze eintragen
var speachnumber = 0;
var delimiters = '|'; // Hier die genutzten Trenner eintragen
var wordcount = 1, slidecount = 1, countcash = 0;
var fontsizespeach = 8;
var fontsizesentence = 18;
var hackedtext = false;
var hackedslide = false;
var marktext = false;
var textclear = false;

var angle = 90;
var DspWidth = 1280;
var DspHeight = 960;

////-----------------------------Preload und Setup----------------------////

function preload() {
  for (var i=0; i<imgcount; i++) {
    imgs[i] = loadImage('assets/slides/slide_'+i+'.jpg');
  }
  for (var i=0; i<speachcount; i++) {
    speaches[i] = loadStrings('assets/speaches/speach_'+i+'.txt');
  }

  sentences = loadStrings('assets/speaches/sentences.txt');

  logo = loadImage('assets/logoweiß.png');
  menubar = loadImage('assets/menubar.png');
  frame = createImg('assets/ipadframe.png');
}

function centerCanvas() {
  var x = (windowWidth - width) / 2;
  var y = (windowHeight - height) / 2;
  cnv.style("z-index", "1")
  cnv.position(x, y);
}

function setup() {  // setup() waits until preload() is done

  setupOsc(9000, 8000); //OSCPort Input , Output

  createCanvas(DspWidth,DspHeight);
  cnv = createCanvas(DspWidth,DspHeight)
  centerCanvas();
  background(255, 255, 255);
  cnv.parent('IpadContainer');
  print(sentences);

  textblock = createDiv('textrede');
  //sentencebox = createDiv('satz');

  frame.position((windowWidth - width)/2, (windowHeight - height)/2);
  frame.size(DspWidth, DspHeight);
  frame.style("z-index", "3");

  for (var i=0; i<imgcount; i++) {
    imgs[i].resize(DspWidth/4, DspHeight/4);
  }
  logo.resize(30,30);
  menubar.resize(1075,45);

  changeSpeach(speachnumber);

  sentencesArr = splitTokens(sentences[0],delimiters);

  update();
  //updateSentence()
}

////-----------------------------Draw-Function----------------------////

function draw() {
  background(0,0);

  if (hackedtext) {
    textFullscreen();
  }else if (hackedslide) {
    slideFullscreen();
  }else if (marktext) {
    textDelete();
  }else if (textclear) {
    clearText();
  }else{
    display();
  }

}

////-----------------------------Display---------------------------///
function display(){
  fill(28, 38, 35);
  rect(20,20,DspWidth-40, DspHeight-40);
  fill(202,202,202);
  rect(DspWidth/2,100,120, wordcount*9);
  fill(255, 255, 255, 255);
  rect(90, 100, DspWidth/2, DspHeight-60);

  rect(DspWidth/2+120, DspHeight/2+160, DspWidth/2-160,DspHeight/4+20);
  textSize(fontsizesentence);
  fill(234, 93, 65);
  text(sentencesArr[sentencenumber]+"",DspWidth/2+140,DspHeight/2+175,DspWidth/2-300,DspHeight/4+10);

  displaySlide(slidecount,DspWidth/2+120,100);
  tint(255,200);
  image(menubar,100,51);
  image(logo,DspWidth/2,68);
}

////-----------------------------Display-Slides---------------------////
function displaySlide(number, xpos, ypos){
  textSize(24);
  fill(252, 235, 182, 225);
  rect(xpos, ypos, DspWidth/2-160, DspHeight/4+20);

  if (slidecount!=countcash) {
    imgs[number].resize(DspWidth/4, DspHeight/4);
    imgs[number+1].resize(DspWidth/4, DspHeight/4);
    countcash = slidecount;
  }

  image(imgs[number], xpos+50, ypos+10);
  fill(28, 38, 35,255);
  push();
  translate(xpos+15,ypos+180);
  rotate(-HALF_PI);
  text("Current Slide",0,0,200,DspHeight/4+10);
  pop();
  //text("Current Slide",xpos+15,ypos+190,10,DspHeight/4+10);
  fill(152, 220, 204, 225);
  rect(xpos, ypos+20+DspHeight/4, DspWidth/2-160, DspHeight/4+20);
  if (number<imgcount-1) {
    image(imgs[number+1], xpos+50, ypos+30+DspHeight/4);
  }
  push();
  translate(xpos+15,ypos+200+DspHeight/4);
  rotate(-HALF_PI);
  fill(28, 38, 35,255);
  text("Next Slide",0,0,200,DspHeight/4+10);
  pop();

}

/////---------------------------FullScreen----------------------//////
function slideFullscreen(){
  clear();
  textblock.remove();
  fill(252, 235, 182, 255);
  rect(20,20,DspWidth-40, DspHeight-40);
  if (slidecount != countcash) {
    imgs[slidecount].resize(DspWidth/2+200, DspHeight/2+200);
    countcash = slidecount;
  }
  image(imgs[slidecount], DspWidth/2-DspWidth/4-100, DspHeight/2-DspWidth/4);
}

function textFullscreen(){
  clear();
  textblock.remove();
  fill(255, 255, 255, 255);
  rect(20,20,DspWidth-40, DspHeight-40);
  textSize(fontsizesentence*3);
  fill(234, 93, 65);
  text(sentencesArr[sentencenumber]+"",200,100,DspWidth-400, DspHeight-400);
}

function textDelete(){
  clear();
  textblock.remove();
  fill(255, 255, 255, 255);
  rect(20,20,DspWidth-40, DspHeight-40);
  textSize(fontsizesentence*3);
  fill(77, 164, 235);
  for (var i = 0; i < ((textWidth(sentencesArr[sentencenumber])-400)/(DspHeight-400))-2; i++) {
    rect(190,100+i*((fontsizesentence*3)+15),DspWidth-400,fontsizesentence*3);
  }
  fill(234, 93, 65);
  text(sentencesArr[sentencenumber]+"",200,100,DspWidth-400, DspHeight-400);
}

function clearText(){
  clear();
  textblock.remove();
  fill(255, 255, 255, 255);
  rect(20,20,DspWidth-40, DspHeight-40);
}

/////----------------------------Update-Text-------------------------////
function update(){
  var fontcolor,fsize,align,backg,pbr;
  textblock.remove();
  textblock = createDiv('');
  textblock.parent('TextContainer');
  //textblock.style("font-size", "100vh");
  textblock.style("margin-top", "1px");
  textblock.style("margin-bottom", "1px");
  textblock.style("padding-top", "1px");
  textblock.style("padding-bottom", "1px");
  textblock.style("z-index", "2");
  textblock.size(DspWidth/2-60, DspHeight-50);
  textblock.position((windowWidth - width)/2+140,(windowHeight - height)/2+100);


  for (var i = 0; i < speachArr.length; i++) {
      if (i === wordcount) {
        fontcolor = "#5CCEEE";
        backg = "white";
        align = "none";
      }else if (match(speachArr[i],sentencesArr[sentencenumber]) != null) {
        fontcolor = "#FA023C";
        backg = "white";
        align = "none";
      }else if(i<wordcount) {
        fontcolor="#CACACA"; //#CACACA
        backg = "#CACACA"; //#5CCEEE
        align = "none";
      }else {
        fontcolor="#1C2623";
        backg = "white";
        align = "none";
      }

    if(i===speachArr.length-1) {
        pbr = "<p>";
        fsize = "24px";
        fontcolor="#1C2623";
        align = "right";
      }else if (match(speachArr[i],sentencesArr[sentencenumber]) != null) {
        pbr = "<br>";
        fsize = "22px";
      }
      else {
        fsize = "0.9vh";
        pbr = "";
        align = "none";
      }

    var textparagraph = createSpan(pbr+speachArr[i]);
    textparagraph.parent(textblock);
    textparagraph.style("margin", "0px");
    textparagraph.style("background-color", backg);
    textparagraph.style("color", fontcolor);
    textparagraph.style("font-size", fsize); //"0.6vh" //fontsizespeach+fsize+"px"
    textparagraph.style("font-weight", "bold");
    textparagraph.style("float", align);

  }
}

/*function updateSentence(){
  sentencebox.remove();
  sentencebox=createDiv('');
  sentencebox.style("font-size", fontsizesentence);
  sentencebox.style("z-index", "4");
  sentencebox.style("box-sizing", "border-box");
  sentencebox.style("border-color","#EA5DF0");
  sentencebox.size(200, 400);
  sentencebox.position(20,20);
  sentencebox.parent('QuestContainer');

  var sentenceinbox = createSpan("<br>"+sentencesArr[sentencenumber]);
  sentenceinbox.style("font-size", fontsizesentence);
  sentenceinbox.style("box-sizing", "border-box");
  sentenceinbox.style("border-color","#EA5DF0");
  sentenceinbox.parent(sentencebox);
}*/

////-----------------------------KeyBoard-Input-for-testing----------------------////

function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    if (slidecount > 0){
      slidecount = slidecount-1;
    } else {

    }
  } else if (keyCode === RIGHT_ARROW) {
    if (slidecount < imgcount-1){
      slidecount = slidecount+1;
    } else {

    }
  } else if (keyCode === UP_ARROW) {
    if (wordcount > 0){
      wordcount = wordcount-1;
      update();
    } else {

    }
  } else if (keyCode === DOWN_ARROW) {
    if (wordcount < speachArr.length-1){
      wordcount = wordcount+1;
      update();
    } else {

    }
  }else if (keyCode === TAB) {
    changeSpeach(1);
  }else if (keyCode === ENTER) {
    changeSlide(1,11);
  }else if (keyCode === SHIFT) {
    changeSentence(sentencenumber+1);
  }

}

//Funktionen von Philip
function prevSentence()
{
  if (wordcount > 0){
    wordcount = wordcount-1;
    update();
  }
}

function nextSentence()
{
  if (wordcount < speachArr.length-1){
    wordcount = wordcount+1;
    update();
  }
}


///-----------------------Rede ändern----------------------------///

function changeSpeach(number){
    speach = speaches[number];
    print(speach);
    redetext = join(speach, '\n');
    print(redetext);
    speachArr=splitTokens(redetext,delimiters); ///-- hier kommen die Trenner von Oben für das Zerschneiden des Textes rein
    update();
    //updateSentence()
}

///---------------------VerändertdenSatzderausgetauschtwird-------////
function changeSentence(number){
    if (number < sentencesArr.length) {
      sentencenumber = number;
      update();
      //updateSentence()
    }
}

///-----------------------Folie ändern----------------------------///
function changeSlide(oldslide, newslide){
  countcash = -1;
  imgs[oldslide] = loadImage('assets/slides/slide_'+newslide+'.jpg');
  imgs[oldslide].resize(DspWidth/4, DspHeight/4);
}

///----------------OSC-Funktionen--------------------------------////
function receiveOsc(address, value) {
	print("received OSC: " + address + ", " + value);

	if (address == '/IPAD/slide') { // /slide mit nummer aufrufen
    if (value[0] > 0||value[0] < imgcount-1){
      slidecount=value[0];
    }
	}
  if (address == '/IPAD/changeslide') { // /slide 2 10 (Beispiel OSC Message - tauscht slide 2 gegen 10 aus)
    changeSlide(value[0],value[1]);
	}
  if (address == '/IPAD/speach') { // /speach 1 (Beispiel OSC Message - läd Rede Nummer 2)
		changeSpeach(value[0]);
	}
  if (address == '/IPAD/sentence') { // /sentence 1 (Beispiel OSC Message - markiert Satz Nummer 2)
    if (value[0] > 0||value[0] < speachArr.length-1){
    wordcount=value[0];
    update();
    }
	}
  if (address == '/IPAD/swap') { // /swap 1 (Beispiel OSC Message - markiert Austauschbaren Satz Nummer 2)
    changeSentence(value[0]);
	}
  if(address == '/IPAD/next') {
    nextSentence();
  }
  if(address == '/IPAD/prev') {
    prevSentence();
  }
  if(address == '/IPAD/slidehack') {
    hackedslide = value[0];
    countcash = -1;
    update();
  }
  if(address == '/IPAD/texthack') {
    hackedtext = value[0];
    countcash = -1;
    update();
  }
  if(address == '/IPAD/marktext') {
    marktext = value[0];
    update();
  }
  if(address == '/IPAD/textclear') {
    textclear = value[0];
    update();
  }
}

function sendOsc(address, value) {
	socket.emit('message', [address].concat(value));
}

function setupOsc(oscPortIn, oscPortOut) {
	var socket = io.connect('http://127.0.0.1:8081', { port: 8081, rememberTransport: false });
	socket.on('connect', function() {
		socket.emit('config', {
			server: { port: oscPortIn,  host: '127.0.0.1'},
			client: { port: oscPortOut, host: '127.0.0.1'}
		});
	});
	socket.on('message', function(msg) {
		if (msg[0] == '#bundle') {
			for (var i=2; i<msg.length; i++) {
				receiveOsc(msg[i][0], msg[i].splice(1));
			}
		} else {
			receiveOsc(msg[0], msg.splice(1));
		}
	});
}
