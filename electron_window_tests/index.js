
const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

app.on('ready', () => {
  mainWindow = new BrowserWindow({
  	//preload:"script.js",
  	title: "I LOVE YOU", // will only show after packaging
  	//icon: NativeImage, // see: https://github.com/electron/electron/blob/master/docs/api/native-image.md
    height: 400,
    width: 400,
    //backgroundColor: "#80FFFFFF",
    //transparent:true,
    //darkTheme: true, // only works on some GTK+3 desktop environments
    //frame: false,
    //useContentSize: true,
    center: true,
    resizable: false,
    //movable: false,
    minimizable: false,
    maximizable: false,
    //closable: false,
    //focusable: false,
    //alwaysOnTop:true,
    //fullscreen: true,
    fullscreenable:true,
    //autoHideMenuBar: true, // maybe only after packaging?
    //skipTaskbar:true,  // maybe only after packaging?
    //titleBarStyle:"hidden",
    kiosk:false, //!!!!!
    //acceptFirstMouse: true, // frist click in unfocused window will be counted
    //vibrancy:"sidebar", // MacOS only
    //webPreferences.preload:"",
    //webPreferences.nodeIntegrationInWorker:true,
    //webSecurity:false, // could become interesting for us in terms of fake websites etc.
  	
  })

  let url = require('url').format({
    protocol: 'file',
    slashes: true,
    pathname: require('path').join(__dirname, 'index.html')
  })  

  var time = 1000
  mainWindow.loadURL(url),

  console.log(mainWindow.getBounds()),

  setTimeout(function(){mainWindow.hide()},time),

  setTimeout(function(){mainWindow.show()},time*2),

  setTimeout(function(){mainWindow.blur()},time*3),
  // before the next, click into another window to focus that one.
  setTimeout(function(){mainWindow.showInactive()},time*4),

  setTimeout(function(){mainWindow.focus()},time*5),

  setTimeout(function(){mainWindow.minimize()},time*6),

  setTimeout(function(){mainWindow.restore()},time*7),

  setTimeout(function(){mainWindow.setFullScreen(true)},time*8),

  setTimeout(function(){mainWindow.setFullScreen(false)},time*10),

  setTimeout(function(){mainWindow.setBounds(
  	{x:30,y:30,height:500, width:500})},time*12),

  setTimeout(function(){mainWindow.setBounds(
  	{x:120,y:120,height:500, width:500})},time*14),

  setTimeout(function(){mainWindow.setBounds(
  	{x:30,y:830,height:500, width:500})},time*15),

  setTimeout(function(){mainWindow.setBounds(
  	{x:1400,y:120,height:900, width:300})},time*17),

  // before the next, click into another window to focus that one.
  // window should flash or (in Mac OS): icon in taskbar should jump
  setTimeout(function(){mainWindow.flashFrame(true)},time*18),
  setTimeout(function(){mainWindow.flashFrame(true)},time*19)
  setTimeout(function(){mainWindow.flashFrame(true)},time*24)

  setTimeout(function(){mainWindow.close()},time*26)  

  //more at: 
  //https://github.com/electron/electron/blob/master/docs/api/browser-window.md


})

