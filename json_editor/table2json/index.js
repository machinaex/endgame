
/// MAKE THE FOLLOWING IMPORTABLE FOR OTHER SCRIPTS!!!!


//htmlString = '<tr><th contenteditable="true">name</th><th contenteditable="true">age</th><th contenteditable="true">hobby</th><th contenteditable="true">location</th></tr><tr><td contenteditable="">abc</td><td contenteditable="">[{"test":[{"and":"so forth","plus":"more"}]}]</td><td contenteditable=""></td><td contenteditable=""></td></tr><tr><td contenteditable=""></td><td contenteditable="">25</td><td contenteditable="">swimming</td><td contenteditable=""></td></tr><tr><td contenteditable="">xyz</td><td contenteditable=""></td><td contenteditable="">programming</td><td contenteditable="">here</td></tr>'

//$('#table').html(htmlString);

var $TABLE = $('#table');
var $BTN = $('#export-btn');
var $EXPORT = $('#export');


// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;


document.getElementById("export-btn").onclick = function() {exportClicked()};

function importHtml(input)
{
  $TABLE.html(input);
}

function isObject(val) {
    if (val === null) { return false;}
    return ( (typeof val === 'function') || (typeof val === 'object') );
}

function exportClicked() {
  
  var $rows = $TABLE.find('tr:not(:hidden)');
  var headers = [];
  var data = [];
  
  // Get the headers (add special header logic here)
  $($rows.shift()).find('th:not(:empty)').each(function () {
    headers.push($(this).text().toLowerCase());
  });
  
  // Turn all existing rows into a loopable array
  $rows.each(function () {
    var $td = $(this).find('td');
    
    var h = {};
    
    // Use the headers from earlier to name our hash keys
    headers.forEach(function (header, i) {
      // check for recursive json structures in order to not 
      // get errors from escape characters:
      try
      {
        //console.log("object parsed from JSON: " + h[header]);
        if(isObject(JSON.parse($td.eq(i).text()))){
          h[header] = JSON.parse($td.eq(i).text());
        }
        else
        {
          h[header] = $td.eq(i).text(); 
        }
      }
      catch(err)
      {
        if ($td.eq(i).text() != "")
        {
          h[header] = $td.eq(i).text();  
        }
      }
      
    });
    
    data.push(h);

  });


  // Output the result
  $EXPORT.text(JSON.stringify(data));
};