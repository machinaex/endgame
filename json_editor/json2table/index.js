/*
	#### to do: ####
  # add possibility to choose level of table (upper most level default)
  # add possibility to import non-table json and interpret them as one object in the table
  # add table in a table option instead of tranlating objects into raw json inside a cell
  			# add possibility to choose level for that as well (default???)
	# add linebreak after x characters
  			# at next charachter y
  # add table 2 json export
  # add button to export/trigger single rows of the table
*/

var outputHTML = "";

var myList = [{"name" : "abc", "age" : [{"test":[{"and": "so forth", "plus":"more"}]}]},
            	{"age" : "25", "hobby" : "swimming"},
            	{"name" : "xyz", "hobby" : "programming", "location":"here"}];

function isObject(val) {
    if (val === null) { return false;}
    return ( (typeof val === 'function') || (typeof val === 'object') );
}


// Builds the HTML Table out of input json data
 function buildHtmlTableFromList(input) {

     var headerdata = addAllColumnHeadersFromList(input);

     var columns = headerdata[0];

     outputHTML =  headerdata[1] + addAllRowsFromList(input,columns);
     
     $("#excelDataTable").html(outputHTML);

     console.log(outputHTML);
     return outputHTML;

 }


function addAllRowsFromList(input, columns)
{
    var output = "";

       for (var i = 0 ; i < input.length ; i++) {
        var row$ = $(' <tr/ >');
        for (var colIndex = 0 ; colIndex < columns.length ; colIndex++) {
             var cellValue = input[i][columns[colIndex]];

             if (cellValue == null) { cellValue = ""; }
             if (isObject(cellValue))
              {
                cellValue = JSON.stringify(cellValue);

                // uncomment in case we want to have tables inside of tables recursively:
                //cellValue = "<table border=1>" + buildHtmlTableFromList(cellValue) + "</table>";
              }
             
              row$.append($('<td/ contenteditable>').html(cellValue));
             
        }
        output = output + row$[0].outerHTML;
        
     }

     return output;
}

 // Adds a header row to the table and returns the set of columns.
 // Need to do union of keys from all records as some records may not contain
 // all records
 function addAllColumnHeadersFromList(input)
 {
     var columnSet = [];
     var headerTr$ = $('<tr/>');


     for (var i = 0 ; i < input.length ; i++) {
         var rowHash = input[i];
         for (var key in rowHash) {
             if ($.inArray(key, columnSet) == -1){
                 columnSet.push(key);
                 headerTr$.append($('<th/ contenteditable=true>').html(key));
             }
         }
     }
     outputHTML = outputHTML + headerTr$[0].outerHTML;

     return [columnSet , headerTr$[0].outerHTML];
 }
