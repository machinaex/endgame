

/*
  #### to do: ####
  # solve issues with 
      # number export from table to json
      # null export from table to json
      # bool import and export

  # add possibility to choose level of table (upper most level default)
  # add possibility to import non-table json and interpret them as one object in the table
  # add table in a table option instead of tranlating objects into raw json inside a cell
        # add possibility to choose level for that as well (default???)
  # add linebreak after x characters
  # add button to export/trigger single rows of the table

  # try implementing http://jsoneditoronline.org/ instead of json-tinker for tree-view and raw-editor
  # try implementing https://jaredforsyth.com/treed/ as additional view
*/

var outputHTML = "";

var myList = [{"name" : "abc", "age" : [{"test":[{"and": "so forth", "plus":"more"}]}]},
              {"age" : "25", "hobby" : "swimming"},
              {"name" : "xyz", "hobby" : "programming", "location":"here"}];

document.onLoad = buildHtmlTableFromList(myList);

function isObject(val) {
    if (val === null) { return false;}
    return ( (typeof val === 'function') || (typeof val === 'object') );
}


// Builds the HTML Table out of input json data
 function buildHtmlTableFromList(input) {

     var headerdata = addAllColumnHeadersFromList(input);

     var columns = headerdata[0];

     outputHTML =  headerdata[1] + addAllRowsFromList(input,columns);
     
     $("#excelDataTable").html(outputHTML);

     //console.log(outputHTML);
     return outputHTML;

 }


function addAllRowsFromList(input, columns)
{
    var output = "";

       for (var i = 0 ; i < input.length ; i++) {
        var row$ = $(' <tr/ >');
        for (var colIndex = 0 ; colIndex < columns.length ; colIndex++) {
             var cellValue = input[i][columns[colIndex]];

             if (cellValue === typeof null) { cellValue = ""; console.log("null: "+columns[colIndex])}
             if (cellValue == true) {cellValue = "true"; console.log("true: "+columns[colIndex])};
             if (cellValue == false) {cellValue = "false"; console.log("false: "+columns[colIndex])};

             if (isObject(cellValue))
              {
                cellValue = JSON.stringify(cellValue);
                // uncomment in case we want to have tables inside of tables recursively:
                //cellValue = "<table border=1>" + buildHtmlTableFromList(cellValue) + "</table>";
              }
             
              row$.append($('<td/ contenteditable>').html(cellValue));
             
        }
        output = output + row$[0].outerHTML;
        
     }

     return output;
}

 // Adds a header row to the table and returns the set of columns.
 // Need to do union of keys from all records as some records may not contain
 // all records
 function addAllColumnHeadersFromList(input)
 {
     var columnSet = [];
     var headerTr$ = $('<tr/>');


     for (var i = 0 ; i < input.length ; i++) {
         var rowHash = input[i];
         for (var key in rowHash) {
             if ($.inArray(key, columnSet) == -1){
                 columnSet.push(key);
                 headerTr$.append($('<th/ contenteditable=true>').html(key));
             }
         }
     }
     outputHTML = outputHTML + headerTr$[0].outerHTML;

     return [columnSet , headerTr$[0].outerHTML];
 }







////////////////////////////////////////////////////////
// TABLE2JSON //

//htmlString = '<tr><th contenteditable="true">name</th><th contenteditable="true">age</th><th contenteditable="true">hobby</th><th contenteditable="true">location</th></tr><tr><td contenteditable="">abc</td><td contenteditable="">[{"test":[{"and":"so forth","plus":"more"}]}]</td><td contenteditable=""></td><td contenteditable=""></td></tr><tr><td contenteditable=""></td><td contenteditable="">25</td><td contenteditable="">swimming</td><td contenteditable=""></td></tr><tr><td contenteditable="">xyz</td><td contenteditable=""></td><td contenteditable="">programming</td><td contenteditable="">here</td></tr>'

//$('#excelDataTable').html(htmlString);

var $TABLE = $('#excelDataTable');
var $BTN = $('#export-btn');
var $EXPORT = $('#export');


// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;


document.getElementById("export-btn").onclick = function() {exportClicked()};

function importHtml(input)
{
  $TABLE.html(input);
}

function isObject(val) {
    if (val === null) { return false;}
    return ( (typeof val === 'function') || (typeof val === 'object') );
}

function exportClicked() {
  
  var $rows = $TABLE.find('tr:not(:hidden)');
  var headers = [];
  var data = [];
  
  // Get the headers (add special header logic here)
  $($rows.shift()).find('th:not(:empty)').each(function () {
    headers.push($(this).text().toLowerCase());
  });
  
  // Turn all existing rows into a loopable array
  $rows.each(function () {
    var $td = $(this).find('td');
    
    var h = {};
    
    // Use the headers from earlier to name our hash keys
    headers.forEach(function (header, i) {
      // check for recursive json structures in order to not 
      // get errors from escape characters:
      try
      {
        //console.log("object parsed from JSON: " + h[header]);
        if(isObject(JSON.parse($td.eq(i).text()))){
          h[header] = JSON.parse($td.eq(i).text());
        }
        else
        {
          // HERE WE SHOULD FILTER FOR BOOL AND NUMBERS AND NULL etc.!!!
          // AS WELL AS FOR wrong escape characters or the other way
          console.log($td.eq(i).text());
          h[header] = JSON.parse($td.eq(i).text()); 
          //h[header] = $td.eq(i).text(); 
        }
      }
      catch(err)
      {
        if ($td.eq(i).text() != "")
        {
          h[header] = $td.eq(i).text();  
        }
      }
      
    });
    
    data.push(h);

  });

  //console.log(data);
  //console.log($('#json-input').val);

  // Output the result
  //$EXPORT.text(JSON.stringify(data));

  //UPDATE TREEVIEW AS WELL AS JSONEDITOR
  var $input = $('#json-input');
  var $editor = $('#json-editor')
  var json = JSON.stringify(data);
  $editor.tinker({ json: json });
  json = $editor.tinker('json');
  $input.val(json);

  buildHtmlTableFromList(JSON.parse(json));
};





///////////////////////////////////////////////////////////////////////
// JSON TINKER

$(function () {
  var $input = $('#json-input');
  var $editor = $('#json-editor').tinker({ json: '[{"greeting":"haro"}]' });
  
  $(document).bind('contextmenu', function () {
    return false;
  }); 

  $('#load-json').on('click', function () {
    var json = $input.val();
    $editor.tinker({ json: json });

    // this is for the table:
    buildHtmlTableFromList(JSON.parse(json));
    //

  });

  $('#extract-json').on('click', function () {
    // UPDATE JSON EDITOR:
    var json = $editor.tinker('json');
    $input.val(json);
    // THEN UPDATE TABLE VIEW:
    buildHtmlTableFromList(JSON.parse(json));
  });

  $input.click(function () {
    $(this).focus();
    $(this).select();
  });
});

