Chattool
=========

A fake chat client written with node/electron.

----------

TODO
-------------
1. Bugfixes/Dokumentation (viele Funktionen sind noch nicht robust)
2. Time anzeigen in Messages
3. Layout und Design des Clients
4. Umgang mit Decisions
5. Anzeigen von Bildern
6. Autosave Feature
7. Boilderplate und Event Callback

Funktionen
-------------

1. **Anzeigen von mehreren Konversationsverläufen** aus der Vergangenheit (Text, Emoji & Bilder)
2. **Empfangen von Nachrichten** während des Stücks (mit System Benachrichtigungen)
3. ***Interaktive Konversationen*** in denen die SpielerInnen vorgefertigten Text tippen können und darauf automatisiert Antworten angezeigt werden.
4. ***Ferngesteuerte Kontrolle*** über den Client (geöffnete Konversationen, Reset, Alert)
5. ***Entscheidungen treffen*** indem eine von mehreren Nachrichten ausgewählt werden kann
6. ... (Nachrichten verschicken!?)  

API
------------
Chat Clients können von Außen mit Inhalt befüllt werden. Die Clients hören auf **Port 6969** (solange noch keine Boilerplate für die Electron Apps vorhanden ist). Erwartet werden Befehle im **Json** Datenformat.

Nachrichten an den Client haben *Tags* für die Art des Befehls (***Type***), den Modus des Befehls (***Mode***), die Betroffene Konversation (***Conversation***) und bei ggf. ein ***Messages*** *Object*.

***Types & Modes***:

- ```Type: MESSAGE``` : Eine, oder mehrere Textnachrichten, die in einer Konversation angezeigt werden sollen. Die einegehenden Messages werden in einem Stash zwischengespeicherter und dann nacheinander angezeigt (bei INTERACTIVE oder DELAY zeitverzögert). Das MESSAGE Json hat immer ein Array
- ```Conversations:[]```:
  - ***Conversation***: ID der Konversation. Darf keine Leer- und Sonderzeichen enthalten, muss immer mit angegeben werden und muss einzigartig sein.
  - ***Name***: Name mit dem die Konversation den SpielerInnen angezeigt wird.
  - ***Messages***:
    - ```Text:``` (String, oder Array mit Strings für Entscheidungen)
    - ```Time:``` (Zeitstempel)
    - ```Sender:``` (OWNER, wenn der Text vom Client kommt)
    - ```Mode:``` (INTERACTIVE, SHOW, DELAY, SILENT, DECISION)
  - ***Mode***:
    - ```APPEND``` : Fügt neue Messages am Ende des Nachrichten-Stash an.
    - ```OVERRIDE``` : Löscht den bestehenden Nachrichten-Stash und erzeugt mit den neuen Messages einen neuen.
    - ```INSERT``` : Fügt neue Messages am Anfang des Nachrichten-Stash an.
    - ```DUMP```  : Nachrichten-Stash wird komplett angezeigt und aus den neuen Messages ein neuer gebildet.
    - ```DUMPSILENT```  : Funktioniert wie DUMP nur ohne Benachrichtigung.



- ```Type: CONTROLL``` :
  - *Mode: FOCUS* : Kontrolliert welche Konversation angezeigt wird.
  - *Mode: ALERT* : Erzeugt eine Benachrichtigung für eine Konversation.
  - *Mode: RESET*  : Setzt eine, oder alle Konversationen zurück.



**Bsp:** siehe hierzu auch die Bsp. Nachricht im chat_remote_tester

```json
{
  "Type":"MESSAGE",
  "Conversations":
  [
    {
      "Conversation":"UNIQUE_ID",
      "Name": "NAME DER KONVERSATION",
      "Mode":"APPEND || OVERRIDE || INSERT || DUMP || DUMPSILENT",
      "Messages":
      [
        {
          "Sender": "OWNER || NAME DES ABSENDERS",
          "Mode": "INTERACTIVE || SHOW || DELAY || SILENT || DECISION",
          "Time": "NOW || BELIEBIGE ZEITANGABE",
          "Text": "DER ANZUZEIGENDE TEXT || {Default: 'Default TEXT', DECISIONS:[{ID: "1", Text:DECISIONTEXT1, Next:ID1}, {ID: "2", Text:DECISIONTEXT2, Next:ID2}, {ID:"3", Text:DECISIONTEXT3, Next:ID3}]}"
        },
        ...
      ]
    },
    ...
  ]
}
```
```json
{
  "Type": "MESSAGE",
  "Mode": "APPEND",
  "Conversation": "Admin Hilfechat",
  "Message": [
    {
      "Sender": "Admin",
      "Mode": "SHOW",
      "Time": "NOW",
      "Text": "BRAUCHST DU HILFE"
    },
    {
      "Sender": "OWNER",
      "Mode": "INTERACTIVE",
      "Time": "NOW",
      "Text": "JA BITTE HILF MIR"
    },
    {
      "Sender": "Admin",
      "Mode": "SHOW",  // wird erst angezeigt wenn spielerin vorherige nachricht getippt hat
      "Time": "NOW",
      "Text": "das ist meine hilfeantwort"
    },
    {
      "Sender": "OWNER",
      "Mode": "INTERACTIVE",
      "Time": "NOW",
      "Text": "danke"
    },
    {
      "Sender": "Admin",
      "Mode": "SHOW",
      "Time": "NOW",
      "Text": "bitte"
    }
  ]
}
```

```json
{
  "Type": "MESSAGE",
  "Conversations":
  [
    {
      "Conversation":"UNIQUE_ID",
      "Name":"ADMIN HILFECHAT",
      "Mode": "APPEND",
      "Messages":
      [
        {
          "Sender": "Admin",
          "Mode": "SHOW",
          "Time": "NOW",
          "Text": "BRAUCHST DU HILFE"
        },
        {
          "Sender": "OWNER",
          "Mode": "INTERACTIVE",
          "Time": "NOW",
          "Text": "JA BITTE HILF MIR"
        },
        {
          "Sender": "Admin",
          "Mode": "SHOW",  // wird erst angezeigt wenn spielerin vorherige nachricht getippt hat
          "Time": "NOW",
          "Text": "das ist meine hilfeantwort"
        },
        {
          "Sender": "OWNER",
          "Mode": "INTERACTIVE",
          "Time": "NOW",
          "Text": "danke"
        },
        {
          "Sender": "Admin",
          "Mode": "DELAY",
          "Time": "NOW",
          "Text": "bitte"
        }
      ],
      ...
    },
    ...
  ]
}
```

```json
{
  "Type":"CONTROLL",
  "Mode":"FOCUS",
  "Conversation": "UNIQUE_ID"
}
```

```json
{
  "Type":"CONTROLL",
  "Mode":"ALERT",
  "Conversation": "UNIQUE_ID",
  "Name": "NAME DER KONVERSATION"
}
```

```json
{
  "Type":"CONTROLL",
  "Mode":"RESET",
  "Conversation": "ALL || UNIQUE_ID"
}
```


Ordnerstruktur
------------

```
chat_client
├── app
|   ├── server.js
|   └── client.js
├── css
|   └── style.css
├── node_modules
|   ...
├── chat.html
├── main.js
└── package.json
```
