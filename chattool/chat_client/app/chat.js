////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GLOBALE VARIABLEN UND MODULES
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const fs = require('fs');
const twemoji = require('twemoji');
const mustache = require('mustache');

const template_sidebar = fs.readFileSync("./mustache/sidebar.html", "utf8");
const template_content = fs.readFileSync("./mustache/content.html", "utf8");
const template_message = fs.readFileSync("./mustache/message.html", "utf8");
const template_typing = fs.readFileSync("./mustache/typing.html", "utf8");
const template_decision = fs.readFileSync("./mustache/decision.html", "utf8");

const conversationStash = {}; // working object with not yet printed messages
const conversationShow = {}; // object with all the printed messages

var currConversation = ""; // speichert welche conversation gerade geöffnet ist

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CALLBACK FOR INCOMING MESSAGES
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// wird aufgerufen, wenn gerade ein neues JSON Paket empfangen wurde
function onNewMessage(data) {
  // geht durch alle Conversations die angekommen sind und verändert werden sollen
  for (var i = 0; i < data.Conversations.length; i++) {
    var item = data.Conversations[i];
    // überprüft, ob die conversation schon vorhanden ist und leitet sie entsprechend weiter
    conversationStash.hasOwnProperty(item.Conversation) ? updateConversation(item) : addConversation(item);
  }
}

function onNewControll(data) {
  var conversation = data.Conversation;
  var name = data.Name;
  var mode = data.Mode;

  switch (mode) {
    case 'FOCUS':
    document.getElementById('Button_'+conversation).click();
    break;
    case 'ALERT':
    triggerNotification(conversation, {Sender:name});
    break;
    case 'RESET':
    resetContent(conversation);
    break;
    default:
    console.log("no expected controll mode");
  }
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BUILD SIDEBAR AND APPEND NEW MESSAGES TO HTML AND UPDATE STASH
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function addConversation(conversation){
  // add Name and Content of Conversation to local message stash
  var name = conversation.Conversation;
  var messages = conversation.Messages;
  conversationStash[name] = messages;

  // adds new entity to HTML sidebar
  var element_sidebar = mustache.to_html(template_sidebar, conversation);
  document.getElementById("sidebarParent").innerHTML += element_sidebar;

  // adds new entity to HTML contentview
  var element_content = mustache.to_html(template_content, conversation);
  document.getElementById("contentParent").innerHTML += element_content;

  // nachdem alles angelegt wurde wird diese function aufgerufen um messages anzuzeigen
  triggerNext(name);
}

function updateConversation(conversation){
  var name = conversation.Conversation;
  var messages = conversation.Messages;

  switch (conversation.Mode) {
    case "APPEND":
    conversationStash[name].push.apply(conversationStash[name], messages);
    resetContentSettings(name);
    triggerNext(name);
    break;
    case "OVERRIDE":
    conversationStash[name] = messages;
    triggerNext(name);
    break;
    case "INSERT":
    conversationStash[name].unshift.apply(conversationStash[name], messages);
    resetContentSettings(name);
    triggerNext(name);
    break;
    case "DUMP":
    //TODO: handle decisions
    for (var i = 0; i < conversationStash[name].length; i++) {
      messageShow(name, conversationStash[name][i])
    }
    transferFromStashToShow(name,0,conversationStash[name].length);
    conversationStash[name] = messages;
    triggerNext(name);
    break;
    case "DUMPSILENT":
    //TODO: handle decisions
    for (var i = 0; i < conversationStash[name].length; i++) {
      messageSilent(name, conversationStash[name][i])
    }
    transferFromStashToShow(name,0,conversationStash[name].length);
    conversationStash[name] = messages;
    triggerNext(name);
    break;
    default:
    console.log("no mode recognized");
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MANGES MESSAGES AND CONVERSATIONS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// handles next message in message stash
function triggerNext(conversation){
  var mode = null;
  // get next message from stash
  if (conversationStash[conversation].length > 0) {
    var msg = conversationStash[conversation][0];
    mode = msg["Mode"];
  }

  switch (mode) {
    case "SHOW":
    messageShow(conversation, msg);
    break;
    case "INTERACTIVE":
    messageInteractive(conversation);
    break;
    case "DELAY":
    messageDelay(conversation, msg);
    break;
    case "SILENT":
    messageSilent(conversation, msg);
    transferFromStashToShow(conversation,0);
    triggerNext(conversation);
    break;
    case "DECISION":
    messageDecision(conversation, msg);
    break;
    default:
    console.log("stash empty");
  }

  // change all browser style emojis to colorful svg images
  twemoji.parse(document.getElementById("List_"+conversation), {
    base: '',
    ext: '.svg',
    folder: './node_modules/twemoji/2/svg'
  });

  // auto scroll to bottom of conversation list on new messages
  var conversation_list = document.getElementById("List_"+conversation)
  conversation_list.scrollTop = conversation_list.scrollHeight;
}

function messageShow(conversation, msg){
  var message_list = document.getElementById("List_"+conversation);
  if (msg.Sender.toUpperCase() != "OWNER") {
    setTimeout(function(){
      var element_message = mustache.to_html(template_typing, {Conversation:conversation, Sender:msg.Sender});
      message_list.innerHTML += element_message;
      setTimeout(function(){
        message_list.removeChild(document.getElementById("Typing_"+conversation));
        var element_message = mustache.to_html(template_message, msg);
        message_list.innerHTML += element_message;
        triggerNotification(conversation, msg);
        transferFromStashToShow(conversation,0);
        triggerNext(conversation);
      }, msg.Text.length*100);}, 1000);
  } else {
    // adds messages to contentview
    var element_message = mustache.to_html(template_message, msg);
    message_list.innerHTML += element_message;
    transferFromStashToShow(conversation,0);
    triggerNext(conversation);
  }
}

function messageInteractive(conversation){
  document.getElementById("Textarea_"+conversation).disabled = false;
}

function messageDelay(conversation, msg) {
  setTimeout(function(){
    messageShow(conversation, msg);
  }, 2000);
}

function messageSilent(conversation, msg){
  console.log("in messageSilent");
  // adds messages to contentview
  var message_list = document.getElementById("List_"+conversation);
  var element_message = mustache.to_html(template_message, msg);
  message_list.innerHTML += element_message
}

function messageDecision(conversation, msg){
  document.getElementById("Textarea_"+conversation).style.display = "none";
  document.getElementById("Sender_"+conversation).style.display = "none";
  document.getElementById("List_"+conversation).style.height = '50%';

  msg["Conversation"] = conversation;
  var element_decision = mustache.to_html(template_decision, msg);
  document.getElementById("Content_"+conversation).innerHTML += element_decision;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// HELPER FUNCTIONS TO MANAGE STASH AND NOTIFICATIONS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function resetContentSettings(conversation){
  try {
    document.getElementById("Textarea_"+conversation).style.display = "block";
    document.getElementById("Sender_"+conversation).style.display = "block";
    document.getElementById("Textarea_"+conversation).disabled = true;
    document.getElementById("Sender_"+conversation).disabled = true;
    document.getElementById("List_"+conversation).style.height = '80%';
    var decisions = document.getElementById("Decision_"+conversation);
    decisions.parentNode.removeChild(decisions);

  } catch (e) {
    console.log(e);
    console.log("fix endlich die reset routine");
  }
}

function resetContent(conversation) {
  var names = (conversation == 'ALL') ? Object.keys(conversationStash): [conversation];
  for (var i = 0; i < names.length; i++) {
    delete conversationStash[names[i]];
    delete conversationShow[names[i]];

    var button = document.getElementById("Button_"+names[i]);
    button.parentNode.removeChild(button);
    var content = document.getElementById("Content_"+names[i]);
    content.parentNode.removeChild(content);
  }
  currConversation = "";
}

function transferFromStashToShow(conversation,start,deleteCount){
  deleteCount = (deleteCount == undefined) ? 1 : deleteCount;

  // fügt die conversation key dem show object hinzu
  if (!conversationShow.hasOwnProperty(conversation)) {
    conversationShow[conversation] = [];
  }

  // fügt messages aus dem stash in das show object ein und löscht sie aus stash
  for (start; start < deleteCount; start++) {
    conversationShow[conversation].push(conversationStash[conversation][start]);
    conversationStash[conversation].splice(start, 1);
  }
}

// play notification sound
function triggerNotification(conversation, msg){
  var audio = new Audio('notification.mp3');
  audio.play();

  var sidebarButton = document.getElementById('Button_'+conversation);
  if (sidebarButton.classList.contains("notification")) {
    // TODO: add to counter of element
  } else if (currConversation != conversation) {
    sidebarButton.className += " notification";
  }

  var options = {
    title: "NEW MESSAGE in #Lifehack Team Chat",
    body: msg.Sender + " hat dir eine Nachricht geschickt!",
  };
  new Notification(options.title, options);
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CALLBACKS FOR USER INTERACTION
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function openConversation(conversation, event){
  // remove all selected classes from buttons; will be added later again
  var buttons = document.getElementsByClassName("sidebar");
  for (var i = 0; i < buttons.length; i++) {
    buttons[i].classList.remove("selected");
  }

  // hide all conversation except the on selected
  var contents = document.getElementsByClassName("content");
  for (i = 0; i < contents.length; i++) {
    if (contents[i].classList.contains(conversation)) {
      currConversation = conversation;
      contents[i].style.display = "block";
      event.currentTarget.classList.add("selected");
      event.currentTarget.classList.remove("notification");
    } else {
      contents[i].style.display = "none";
    }
  }
}

function textareaCallback(conversation, event){
  var nextMsg = conversationStash[conversation][0];
  // replace letter in textarea with next messages text
  event.srcElement.value = nextMsg["Text"].substring(0, event.srcElement.value.length)

  // activate the send button
  if (event.srcElement.value.length >= nextMsg["Text"].length) {
    document.getElementById("Sender_"+conversation).disabled = false;
  }
}

function senderCallback(conversation, event){
  // default settings for content elements
  event.srcElement.disabled = true;
  document.getElementById("Textarea_"+conversation).value = "";
  document.getElementById("Textarea_"+conversation).disabled = true;

  // show message from textarea
  messageShow(conversation, conversationStash[conversation][0]);
}

function decisionCallback(conversation, text, next, id, event){
  var msg = conversationStash[conversation][0];
  msg.Text = text;
  //conversationStash[conversation][0].filter(function( item ) {return item.ID == id;});

  var message_list = document.getElementById("List_"+conversation);
  var element_message = mustache.to_html(template_message, msg);
  message_list.innerHTML += element_message;

  console.log("Entscheidung getroffen: " + id);
  // TODO: alternative messages aus dem Stash entfernenen um nur noch den Strang der Auswahl zu haben
  transferFromStashToShow(conversation,0);
  resetContentSettings(conversation);
  triggerNext(conversation);
}
