const net = require('net');
// default Port Chatclients listen to
const PORT = 6969;
// creates a new Server
const server = net.createServer();

// callback method whene there is a connection
server.on('connection', connectionHandler);

// start listening at PORT 6969
server.listen(PORT, function() {
  var address = server.address()
  console.log("server listening on "+address.address+" "+address.port);
});

// callback on connection, data and disconnect
function connectionHandler(conn) {

  var remoteAddress = conn.remoteAddress + ':' + conn.remotePort;
  console.log('\n1. new client connection from %s', remoteAddress);

  conn.setEncoding('utf8');
  conn.on('data', onConnData);
  conn.once('close', onConnClose);
  conn.on('error', onConnError);

  function onConnData(d) {
    console.log('2. connection data from %s', remoteAddress);

    // checks if json is correct and handles the messages
    if (validateData(d)) {
      console.log("3. successfully parsed json");
      conn.write("1"); // answer client with "1" if everything went fine
    } else {
      console.log("3. could not handle json.");
      conn.write("0"); // answer client with "0" if someting went wrong
    }
  }

  function onConnClose() {
    console.log('4. connection from %s closed', remoteAddress);
  }

  function onConnError(err) {
    console.log('Connection %s error: %s', remoteAddress, err.message);
  }
}

// function checks if the recived message is a json of the expected Type
// TODO: implement callback on messages in non blocking way
// TODO: really check if data has correct format
function validateData(d) {
  try {
    var json = JSON.parse(d);

    if (json.Type == "MESSAGE") {
      onNewMessage(json);
      return true;
    }
    else if (json.Type == "CONTROLL") {
      onNewControll(json);
      return true;
    }
    else {
      return false;
    }

  } catch (e) {
    console.log(e);
    return false;
  }
}
