var _table_ = document.createElement('table'),
_tr_ = document.createElement('tr'),
_th_ = document.createElement('th'),
_td_ = document.createElement('td');

// Builds the HTML Table out of myList json data
function buildHtmlTable(arr, id) {
  var table = _table_.cloneNode(false),
  columns = addAllColumnHeaders(arr, table);
  for (var i=0; i < arr.length; ++i) {
    var tr = _tr_.cloneNode(false);
    for (var j=0; j < columns.length ; ++j) {
      var td = _td_.cloneNode(false);
      cellValue = arr[i][columns[j]];
      td.appendChild(document.createTextNode(arr[i][columns[j]] || ''));
      tr.appendChild(td);
    }
    table.appendChild(tr);
    table.setAttribute("id", id);
  }
  return table;
}

// Adds a header row to the table and returns the set of columns.
// Need to do union of keys from all records as some records may not contain
// all records
function addAllColumnHeaders(arr, table)
{
  var columnSet = [],
  tr = _tr_.cloneNode(false);
  for (var i=0, l=arr.length; i < l; i++) {
    for (var key in arr[i]) {
      if (arr[i].hasOwnProperty(key) && columnSet.indexOf(key)===-1) {
        columnSet.push(key);
        var th = _th_.cloneNode(false);
        th.appendChild(document.createTextNode(key));
        tr.appendChild(th);
      }
    }
  }
  table.appendChild(tr);
  return columnSet;
}

function loadConversations(conn, name){
  var p = document.createElement('div');
  p.setAttribute("id", name);
  document.body.appendChild(p);
  var h = document.createElement('h3');
  h.innerHTML = name;
  p.appendChild(h);
  for (var conversation in conn) {
    p.appendChild(buildHtmlTable(conn[conversation], conversation));
  }
}

function replaceStatus(conn,host,port){
  var tables =  document.getElementById(conn).getElementsByTagName('table');
  for (var i = 0; i < tables.length; i++) {
    var table = tables[i];
    var id = table.getAttribute("id");
    for (var j = 0; j < table.rows.length; j++) {
      var status = table.rows[j].cells[5];
      if (status.innerText == 1) {
        status.innerText = '';
        var newCheckBox = document.createElement('input');
        newCheckBox.type = 'checkbox';
        newCheckBox.checked = true;
        newCheckBox.setAttribute('onchange', "sendCmd('"+id+"',"+(j-1)+","+j+",'"+host+"',"+port+")");
        status.appendChild(newCheckBox);
      }
    }
  }

}
