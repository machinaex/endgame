var spreadsheet = {
    "Gesa": [
        {
            "owner": "Marcella",
            "sender": "Gesa",
            "counter": 0,
            "timestamp": "1899-12-30T19:30:00.000Z",
            "text": "Jetzt sag schon: Wie läuft's?",
            "status": 1
        },
        {
            "owner": "Marcella",
            "sender": "Marcella",
            "counter": 1,
            "timestamp": "1899-12-30T19:45:00.000Z",
            "text": "Total gut, eigentlich!",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Gesa",
            "counter": 2,
            "timestamp": "1899-12-30T19:47:00.000Z",
            "text": "Eigentlich?",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Marcella",
            "counter": 3,
            "timestamp": "1899-12-30T20:05:00.000Z",
            "text": "Naja, ich weiß nicht genau, ob ich mit den anderen mithalten kann. ",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Gesa",
            "counter": 4,
            "timestamp": "1899-12-30T20:06:00.000Z",
            "text": "Ach Marci! Das ist doch Quatsch!",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Marcella",
            "counter": 5,
            "timestamp": "1899-12-30T20:12:00.000Z",
            "text": "Okay, ich übertreibe. Aber die anderen blicken glaube ich voll durch. Und ich versteh irgendwie nur die Hälfte.",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Gesa",
            "counter": 6,
            "timestamp": "1899-12-30T20:25:00.000Z",
            "text": "Es wird ja wohl einen Grund geben, dass die dich eingestellt haben. ",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Marcella",
            "counter": 7,
            "timestamp": "1899-12-30T20:30:00.000Z",
            "text": "Vielleicht haben sie sich vertan ...",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Gesa",
            "counter": 8,
            "timestamp": "1899-12-30T20:31:00.000Z",
            "text": "Genau. Weil sie in ihrem dreistufigen Auswahlverfahren ja kaum die Möglichkeit hatten, dich auf Herz und Nieren zu prüfen. * Ironie aus.",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Marcella",
            "counter": 9,
            "timestamp": "1899-12-30T20:50:00.000Z",
            "text": "Jaja, du hast ja Recht. Ist nur komisch für mich, so als Quereinsteigerin. Die anderen haben doch bestimmt alle die krassesten Qualifikationen.",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Gesa",
            "counter": 10,
            "timestamp": "1899-12-30T20:55:00.000Z",
            "text": "DEINE krasse Qualifikation ist, dass du dir das alles selber angeeignet hast. ",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Marcella",
            "counter": 11,
            "timestamp": "1899-12-30T21:23:00.000Z",
            "text": "Mir blieb ja nichts anderes übrig.",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Gesa",
            "counter": 12,
            "timestamp": "1899-12-30T21:40:00.000Z",
            "text": "Na klar wär dir was anderes übrig geblieben. Du hättest auch bis in alle Ewigkeit weiter bei irgendwelchen Arschlöchern die Wohnung putzen können.",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Marcella",
            "counter": 13,
            "timestamp": "1899-12-30T21:44:00.000Z",
            "text": "Gut, im Vergleich dazu wirkt der Job hier echt in Ordnung :)",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Gesa",
            "counter": 14,
            "timestamp": "1899-12-30T21:50:00.000Z",
            "text": "Wenn du was nicht sofort kapierst, kannst du doch auch nachfragen. Die werden dir schon nicht den Kopf abreißen!",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Marcella",
            "counter": 15,
            "timestamp": "1899-12-30T22:02:00.000Z",
            "text": "Wer weiß! Heißt schießlich Raubtier-Kapitalismus ...",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Gesa",
            "counter": 16,
            "timestamp": "1899-12-30T22:12:00.000Z",
            "text": "Haha, wenn du meinst! Aber da bist du die mit den kräftigen Tatzen. Und nicht das Lamm ... (um im Bild zu bleiben)",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Marcella",
            "counter": 17,
            "timestamp": "1899-12-30T22:32:00.000Z",
            "text": "Grrrrrroah!",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Gesa",
            "counter": 18,
            "timestamp": "1899-12-30T22:56:00.000Z",
            "text": "Das ist die richtige Einstellung! Sehen wir uns später noch?",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Marcella",
            "counter": 19,
            "timestamp": "1899-12-29T23:12:00.000Z",
            "text": "Mal gucken ... Kann nicht so richtig einschätzen, wie lange das hier noch dauert",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Gesa",
            "counter": 20,
            "timestamp": "1899-12-29T23:15:00.000Z",
            "text": "Alles klar. Schreib mir einfach, wenn du raus bist. Ich bin noch ein bisschen wach. Und warte auf dich *",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Marcella",
            "counter": 21,
            "timestamp": "1899-12-30T00:58:00.000Z",
            "text": "Hey, bist du noch wach? Sorry, hat ewig gedauert ...",
            "status": 0
        }
    ],
    "Mark": [
        {
            "owner": "Marcella",
            "sender": "Mark",
            "counter": 0,
            "timestamp": "1899-12-30T20:15:00.000Z",
            "text": "Hey Marcella, tut mir leid, dass ich mich so lange nicht mehr gemeldet habe ... hatte viel um die Ohren. Und zuhause gab's auch Stress. Naja, wem erzähl ich das. Ich hoffe, dir geht's gut? ",
            "status": 1
        },
        {
            "owner": "Marcella",
            "sender": "Marcella",
            "counter": 1,
            "timestamp": "1899-12-30T00:22:00.000Z",
            "text": "Schön von dir zu hören! Hat sich tatsächlich einiges verändert bei mir: Ich weiß gar nicht, ob ich das mal erzählt habe, aber ich habe ja nach Feierabend immer diese Online-Kurse gemacht und mir da ein bisschen programmieren beigebracht. Vor einem Vierteljahr hab mich aus Spaß auf einen Developerer-Job beworben und mir keine großen Chancen ausgerechnet, aber klang zu gut, ums nicht zu versuchen. War ein ewiges Bewerbungsverfahren, aber am Ende haben die mich tatsächlich genommen ... Direkt mit einem unbefristeten Vertrag, ich kann mein Glück noch gar nicht fassen. Und mit dem Gehalt bin ich die Schulden wahrscheinlich in spätestens einem Jahr los. Heute war der erste Tag, ziemlich aufregend alles, muss mich erstmal zurechtfinden. Ist halt doch ein bisschen anders als putzen ;) Und bei dir?",
            "status": 0
        }
    ],
    "Gruppenchat": [
        {
            "owner": "Marcella",
            "sender": "Administrator",
            "counter": 0,
            "timestamp": "1899-12-30T16:05:00.000Z",
            "text": "Herzlich Willkommen bei #Lifehack! In diesem Gruppenchat stehen wir deinem Team mit Rat und Tat zur Seite. Schön, dass ihr da seid!",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Marcella",
            "counter": 1,
            "timestamp": "1899-12-30T18:26:00.000Z",
            "text": "Danke, ich freu mich auch, dass ich hier sein darf!",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Administrator",
            "counter": 2,
            "timestamp": "1899-12-30T20:00:00.000Z",
            "text": "Wenn ihr meine Hilfe braucht, chattet mich einfach an.",
            "status": 1
        },
        {
            "owner": "Marcella",
            "sender": "Marcella",
            "counter": 3,
            "timestamp": "1899-12-30T20:30:00.000Z",
            "text": "Bisschen peinlich, aber ich hab mit der Software noch nie gearbeitet ... wie funktioniert das nochmal?",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Administrator",
            "counter": 4,
            "timestamp": "1899-12-30T21:00:00.000Z",
            "text": "Ganz einfach und kein Problem :) Benutze einfach die Tasten W (oben), A (links), S (unten) und D (rechts), um den Distributor zu steuern. Mit C kannst du die Partikel anziehen, dann folgen sie dir treu - und mit V wieder abstoßen (aber das brauchst du heute eigentlich gar nicht).",
            "status": 1
        },
        {
            "owner": "Marcella",
            "sender": "Marcella",
            "counter": 5,
            "timestamp": "1899-12-30T21:05:00.000Z",
            "text": "Jetzt erinnere ich mich wieder! Dann kann ich ja jetzt die Partikel gleichmäßig über die drei Zentren verteilen. ",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Administrator",
            "counter": 6,
            "timestamp": "1899-12-30T21:07:00.000Z",
            "text": "Genau. Solange sie grün sind, flottieren sie frei, wenn sie gelb werden, binden sie sich an den Distributor. Weißt du ja :)",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Administrator",
            "counter": 7,
            "timestamp": "1899-12-30T21:15:00.000Z",
            "text": "Na, funktioniert es?",
            "status": 1
        },
        {
            "owner": "Marcella",
            "sender": "Marcella",
            "counter": 8,
            "timestamp": "1899-12-30T21:22:00.000Z",
            "text": "Nein, ich hab leider leichte Schwierigkeiten ... hast du vielleicht noch einen Tipp für mich?",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Administrator",
            "counter": 9,
            "timestamp": "1899-12-30T21:25:00.000Z",
            "text": "Na klar. Probier es doch mal mit der Taktik, erst ein paar Partikel in den Puls am Handgelenk zu bringen. Da werden die wenigsten benötigt, um die Daten auszulesen, also ist es nicht schlimm, wenn sie sich wieder verteilen, während du die Hirn- und Herzwerte ausliest ...",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Administrator",
            "counter": 10,
            "timestamp": "1899-12-30T21:27:00.000Z",
            "text": "Und denk dran: Alle drei Balken am oberen Bildschirmrand müssen gleichzeitig voll geladen sein, dann können genug Daten ausgelesen werden und du hast es geschafft!",
            "status": 0
        },
        {
            "owner": "Marcella",
            "sender": "Marcella",
            "counter": 11,
            "timestamp": "1899-12-30T21:35:00.000Z",
            "text": "Okay, wenn jetzt noch etwas ist, frage ich einfach Matt. Danke dir!",
            "status": 0
        }
    ]
};
