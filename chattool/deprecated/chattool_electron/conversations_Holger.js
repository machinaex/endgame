var spreadsheet ={
    "Bernadette": [
        {
            "owner": "Holger",
            "sender": "Bernadette",
            "counter": 0,
            "timestamp": "1899-12-30T19:30:00.000Z",
            "text": "Jetzt sag schon: Wie läuft's?",
            "status": 1
        },
        {
            "owner": "Holger",
            "sender": "Holger",
            "counter": 1,
            "timestamp": "1899-12-30T19:45:00.000Z",
            "text": "Bin noch unschlüssig, wie ich das hier alles finden soll. Bisschen aufgesetzt kommt mir das schon alles vor. Irgendwie gehör ich hier nicht hin ...",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Bernadette",
            "counter": 2,
            "timestamp": "1899-12-30T19:47:00.000Z",
            "text": "Ach komm, jetzt sei mal nicht gleich so negativ! Dass der Tonfall in der Privatwirtschaft anders ist als an der Uni, war doch vorher klar.",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Holger",
            "counter": 3,
            "timestamp": "1899-12-30T20:05:00.000Z",
            "text": "Jaja, ich weiß. Ist wahrscheinlich nur die Umstellung nach drei Jahren Einsamkeit in der Bibliothek.",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Bernadette",
            "counter": 4,
            "timestamp": "1899-12-30T20:06:00.000Z",
            "text": "Wo du, wenn wir uns mal kurz zurückerinnern wollen, ja auch die meiste Zeit gelitten hast ...",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Holger",
            "counter": 5,
            "timestamp": "1899-12-30T20:12:00.000Z",
            "text": "Ich leide einfach gerne! Weißt du doch.",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Bernadette",
            "counter": 6,
            "timestamp": "1899-12-30T20:25:00.000Z",
            "text": "Allerdings ...",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Holger",
            "counter": 7,
            "timestamp": "1899-12-30T20:30:00.000Z",
            "text": "Ich MUSS erstmal alles schlecht reden! Das ist mein Verhaltensschema.",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Bernadette",
            "counter": 8,
            "timestamp": "1899-12-30T20:31:00.000Z",
            "text": "Das du vielleicht zur Abwechslung halber auch mal durchbrechen könntest. Nur so ein Vorschlag.",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Holger",
            "counter": 9,
            "timestamp": "1899-12-30T20:50:00.000Z",
            "text": "Da hast du natürlich völlig recht. Ich werde darüber nachdenken.",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Bernadette",
            "counter": 10,
            "timestamp": "1899-12-30T20:55:00.000Z",
            "text": "Dass ich das noch erleben darf!",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Holger",
            "counter": 11,
            "timestamp": "1899-12-30T21:23:00.000Z",
            "text": "Bei genauerer Betrachtung sehen die Kollegen eigentlich ganz okay aus. Gar nicht so die typischen Karrierehipster. ",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Bernadette",
            "counter": 12,
            "timestamp": "1899-12-30T21:40:00.000Z",
            "text": "Sondern?",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Holger",
            "counter": 13,
            "timestamp": "1899-12-30T21:44:00.000Z",
            "text": "Naja, um ehrlich zu sein, sind die mir alle ziemlich ähnlich. Wirken auch erstmal eher skeptisch. Und die Witze von den Obergurus kommen auch nur so mäßig gut an.",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Bernadette",
            "counter": 14,
            "timestamp": "1899-12-30T21:50:00.000Z",
            "text": "Haha, sehr gut! Und haben sie euch schon erzählt, wie dieses ominöse Körperding jetzt funktionieren soll?",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Holger",
            "counter": 15,
            "timestamp": "1899-12-30T22:02:00.000Z",
            "text": "In Ansätzen. Aber ist natürlich alles streng geheim!",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Bernadette",
            "counter": 16,
            "timestamp": "1899-12-30T22:04:00.000Z",
            "text": "Schon klar. Für Geheimhaltung haben sie sich mit dir ja genau den Richtigen ausgesucht ...",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Holger",
            "counter": 17,
            "timestamp": "1899-12-30T22:32:00.000Z",
            "text": "Ey! Ich habe noch unentdeckte Talente! Habe sie nur aus taktischen Gründen bisher vor dir verborgen.",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Bernadette",
            "counter": 18,
            "timestamp": "1899-12-30T22:56:00.000Z",
            "text": "Imponierend. Wann ist denn heute Feierabend?",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Holger",
            "counter": 19,
            "timestamp": "1899-12-29T23:12:00.000Z",
            "text": "Das weiß nur Gott. Bzw. Tom.",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Bernadette",
            "counter": 20,
            "timestamp": "1899-12-29T23:15:00.000Z",
            "text": "Okay, dann leg ich mich schon mal hin. Bis später. Oder gute Nacht.",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Holger",
            "counter": 21,
            "timestamp": "1899-12-29T23:32:00.000Z",
            "text": "Nachti!",
            "status": 0
        }
    ],
    "Leon": [
        {
            "owner": "Holger",
            "sender": "Leon",
            "counter": 0,
            "timestamp": "1899-12-30T20:15:00.000Z",
            "text": "Hey Holger, tut mir leid, dass ich mich so lange nicht mehr gemeldet habe ... hatte viel um die Ohren. Und zuhause gab's auch Stress. Naja, wem erzähl ich das. Ich hoffe, dir geht's gut? ",
            "status": 1
        },
        {
            "owner": "Holger",
            "sender": "Holger",
            "counter": 1,
            "timestamp": "1899-12-30T00:22:00.000Z",
            "text": "Schön von dir zu hören! Hat sich tatsächlich einiges verändert bei mir: Hab die Promotion an den Nagel gehängt, weil ich mich einfach nur noch damit gequält habe. Und was soll ich schon an der Uni? Auf die wissenschaftliche Karriere hab ich eh keinen Bock mehr nach allem, was ich da in den letzten Jahren mitbekommen habe. Jetzt also ein neuer Job: Heute ging's los, bei einem Start Up namens #Lifehack. Peinlicher Name, ich weiß. Aber ich find's ganz erholsam, nicht mehr alleine vor mich hinarbeiten zu müssen. Und aus dieser komischen Uni-Parallelwelt rauszukommen. Und ist ja nicht so, als hätte ich meine Seele jetzt dem Teufel verkauft ;) Berna ist glaube ich auch ganz froh, dass sie sich mein Bibliotheksgejammer nicht mehr anhören muss ... Naja, mal gucken wie's weitergeht. Wie isses bei dir? ",
            "status": 0
        }
    ],
    "Gruppenchat": [
        {
            "owner": "Holger",
            "sender": "Administrator",
            "counter": 0,
            "timestamp": "1899-12-30T16:05:00.000Z",
            "text": "Herzlich Willkommen bei #Lifehack! In diesem Gruppenchat stehen wir deinem Team mit Rat und Tat zur Seite. Schön, dass ihr da seid!",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Holger",
            "counter": 1,
            "timestamp": "1899-12-30T18:26:00.000Z",
            "text": "Bisher noch keine Probleme, danke!",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Administrator",
            "counter": 2,
            "timestamp": "1899-12-30T20:00:00.000Z",
            "text": "Wenn ihr meine Hilfe braucht, chattet mich einfach an.",
            "status": 1
        },
        {
            "owner": "Holger",
            "sender": "Holger",
            "counter": 3,
            "timestamp": "1899-12-30T20:30:00.000Z",
            "text": "Bisschen peinlich, aber ich hab mit der Software noch nie gearbeitet ... wie funktioniert das nochmal?",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Administrator",
            "counter": 4,
            "timestamp": "1899-12-30T21:00:00.000Z",
            "text": "Ganz einfach und kein Problem :) Benutze einfach die Tasten W (oben), A (links), S (unten) und D (rechts), um den Distributor zu steuern. Mit C kannst du die Partikel anziehen, dann folgen sie dir treu - und mit V wieder abstoßen (aber das brauchst du heute eigentlich gar nicht).",
            "status": 1
        },
        {
            "owner": "Holger",
            "sender": "Holger",
            "counter": 5,
            "timestamp": "1899-12-30T21:05:00.000Z",
            "text": "Jetzt erinnere ich mich wieder! Dann kann ich ja jetzt die Partikel gleichmäßig über die drei Zentren verteilen. ",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Administrator",
            "counter": 6,
            "timestamp": "1899-12-30T21:07:00.000Z",
            "text": "Genau. Solange sie grün sind, flottieren sie frei, wenn sie gelb werden, binden sie sich an den Distributor. Weißt du ja :)",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Administrator",
            "counter": 7,
            "timestamp": "1899-12-30T21:15:00.000Z",
            "text": "Na, funktioniert es?",
            "status": 1
        },
        {
            "owner": "Holger",
            "sender": "Holger",
            "counter": 8,
            "timestamp": "1899-12-30T21:22:00.000Z",
            "text": "Nein, ich hab leider leichte Schwierigkeiten ... hast du vielleicht noch einen Tipp für mich?",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Administrator",
            "counter": 9,
            "timestamp": "1899-12-30T21:25:00.000Z",
            "text": "Na klar. Probier es doch mal mit der Taktik, erst ein paar Partikel in den Puls am Handgelenk zu bringen. Da werden die wenigsten benötigt, um die Daten auszulesen, also ist es nicht schlimm, wenn sie sich wieder verteilen, während du die Hirn- und Herzwerte ausliest ...",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Administrator",
            "counter": 10,
            "timestamp": "1899-12-30T21:27:00.000Z",
            "text": "Und denk dran: Alle drei Balken am oberen Bildschirmrand müssen gleichzeitig voll geladen sein, dann können genug Daten ausgelesen werden und du hast es geschafft!",
            "status": 0
        },
        {
            "owner": "Holger",
            "sender": "Holger",
            "counter": 11,
            "timestamp": "1899-12-30T21:35:00.000Z",
            "text": "Okay, wenn jetzt noch etwas ist, frage ich einfach Matt. Danke dir!",
            "status": 0
        }
    ]
};
