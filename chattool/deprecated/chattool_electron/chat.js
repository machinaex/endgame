var db;
var currConversation;

// function to hide/load contentview of selected conversation
function openConversation(evt, name) {
  var i, tabcontent, tablinks;

  // hide all
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // deselect all buttons in sidebar
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    if (tablinks[i].className != "tablinks notification") {
      tablinks[i].className =  "tablinks";
    }
  }

  // show content and highlight selected button in sidebar
  document.getElementById(name).style.display = "block";
  evt.currentTarget.className = "tablinks active";
  currConversation = name;

  // empty textarea
  document.getElementById("inputField").value = '';
  document.getElementById("sender").disabled = true;
}

// entry function to load chat content from .js file
function loadConversations() {
  db = spreadsheet;

  // add unique id to conversations array (same as LI id in HTML)
  for (var item in db) {
    for (var i = 0; i < db[item].length; i++) {
      db[item][i].counter = item+"_"+db[item][i].counter;
    }
  }

  for(var contact in db) {
    createConversations(contact);
  }
}

// create entrys in sidebar and in contentview
function createConversations(name){
  // messages array from .json
  var conversation = db[name];
  //console.log(name);

  // create entry in tab (sidebar)
  var btn = document.createElement("BUTTON");
  btn.setAttribute("id", name+"_Button");
  btn.setAttribute("class", "tablinks");
  btn.setAttribute("onclick", "openConversation(event, '"+name+"')");
  var t = document.createTextNode(name);
  btn.appendChild(t);
  document.getElementById("sidebar").appendChild(btn);

  // create entrys in tabcontent
  var contentDiv = document.createElement("DIV");
  contentDiv.setAttribute("id", name);
  contentDiv.setAttribute("class", "tabcontent");

  var content = document.createElement("OL");
  content.setAttribute("class", "chat");

  for (var item in conversation) {
    //console.log(item);
    var msg = document.createElement("LI");
    msg.setAttribute("id", conversation[item]["counter"]);
    if (conversation[item]["owner"] == conversation[item]["sender"]) {
      msg.setAttribute("class", "self");
    } else {
      msg.setAttribute("class", "other");
    }

  //  if (conversation[item]["status"] != 0) {
  if(true){
      msg.setAttribute("style", "display:none");
    }

    var msgDIV = document.createElement("DIV");
    msgDIV.setAttribute("class", "msg");
    msg.appendChild(msgDIV);

    var userDIV = document.createElement("DIV");
    userDIV.setAttribute("class", "user");
    msgDIV.appendChild(userDIV);

    var msgTXT = document.createElement("P");
    var t = document.createTextNode(conversation[item]["text"]);
    msgTXT.appendChild(t);
    msgDIV.appendChild(msgTXT);

    content.appendChild(msg);
  }
  contentDiv.appendChild(content);
  document.getElementById("contenview").appendChild(contentDiv);
}

function updateDB(conversation, start, end){
  for (var i = start; i < end; i++) {
    if (db[conversation][i].status != 0) {
      db[conversation][i].status = 0;
    }
  }
  showNext();
}

function showNext(){
  for (var conversation in db) {
    for (var i = 0; i < db[conversation].length; i++) {
      if (db[conversation][i].status == 0) {
        if (document.getElementById(db[conversation][i].counter).hasAttribute("style")) {
          if (db[currConversation][i].owner != db[currConversation][i].sender) {
            document.getElementById(db[conversation][i].counter).removeAttribute("style");
            triggerNotification(conversation, db[currConversation][i]);
          } else {
            //document.getElementById("inputField").focus();
            break;
          }
        }
      }
    }
  }
}

function senderCallback(){
  // empty textarea
  document.getElementById("inputField").value = '';
  document.getElementById("sender").disabled = true;

  for (var i = 0; i < db[currConversation].length; i++) {
    if (document.getElementById(db[currConversation][i].counter).hasAttribute("style")) {
      if (db[currConversation][i].owner == db[currConversation][i].sender) {
        document.getElementById(db[currConversation][i].counter).removeAttribute("style");
        setTimeout(function(){ showNext(); }, 2000);
        break;
      }
    }
  }
}

// play notification sound
function triggerNotification(conversation, msg){
  var audio = new Audio('telegram.mp3');
  audio.play();

  document.getElementById(conversation+"_Button").className = "tablinks notification";

  var options = [
    {
      title: "NEW MESSAGE in #Lifehack Team Chat",
      body: msg.sender + " hat dir eine Nachricht geschickt!",
    }]
    new Notification(options[0].title, options[0]);
  }

  // replace user input in textarea with predefined textarea
  function checkNext(event){
    var nextMsg = getNext(db[currConversation]);
    //console.log(nextMsg);

    if (nextMsg && nextMsg.status == 0) {
      if (nextMsg.sender == nextMsg.owner) {
        replaceLetter(nextMsg.text);
        console.log("next msg is from user (input needed)");
      } else {
        console.log('next msg is from other');
        document.getElementById("inputField").value = '';
      }
    } else {
      document.getElementById("inputField").value = '';
      console.log("end of conversation");
    }
  }
  function replaceLetter(currText){

    var inputField = document.getElementById("inputField");
    var text = currText.substring(0, inputField.value.length);

    if (inputField.value.length >= currText.length) {
      document.getElementById("sender").disabled = false;
    } else {
      document.getElementById("sender").disabled = true;
    }

    inputField.value = text;
  }
  function getNext(messages){
    for (var i = 0; i < messages.length; i++) {
      if (document.getElementById(messages[i].counter).hasAttribute("style")) {
        if (db[currConversation][i].owner == db[currConversation][i].sender) {
          return messages[i];
        }
      }
    }
    return false;
  }
  /*
  function getNext(messages){
  for (var i = 0; i < messages.length; i++) {
  if (messages[i].status != 0) {
  return messages[i];
}
}
return false;
}
*/
