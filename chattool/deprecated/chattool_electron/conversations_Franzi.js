var spreadsheet = {
    "Petra": [
        {
            "owner": "Franzi",
            "sender": "Petra",
            "counter": 0,
            "timestamp": "1899-12-30T19:30:00.000Z",
            "text": "Jetzt sag schon: Wie läuft's?",
            "status": 1
        },
        {
            "owner": "Franzi",
            "sender": "Franzi",
            "counter": 1,
            "timestamp": "1899-12-30T19:45:00.000Z",
            "text": "Noch besser als erwartet! Die Stimmung ist super!",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Petra",
            "counter": 2,
            "timestamp": "1899-12-30T19:47:00.000Z",
            "text": "Das freut mich für dich! Sind nette Kollegen dabei?",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Franzi",
            "counter": 3,
            "timestamp": "1899-12-30T20:05:00.000Z",
            "text": "Hatte noch nicht so viel mit denen zu tun, heute ist ja nur Einarbeitung, wir kriegen die ganze Zeit Sachen erklärt.",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Petra",
            "counter": 4,
            "timestamp": "1899-12-30T20:06:00.000Z",
            "text": "Das Betriebsklima ist das wichtigste. Wenn das stimmt, ist auch die Arbeit nicht so schlimm! Also mein Tipp: Such dir erstmal ein paar Freunde!",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Franzi",
            "counter": 5,
            "timestamp": "1899-12-30T20:12:00.000Z",
            "text": "Boah Mama, ich hab die Stelle angenommen, weil ich meine Qualifikatonen hier optimal einsetzen kann. Und nicht um jemanden zum Heiraten zu finden!",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Petra",
            "counter": 6,
            "timestamp": "1899-12-30T20:25:00.000Z",
            "text": "Ach Spatz, so war das doch nicht gemeint. ",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Franzi",
            "counter": 7,
            "timestamp": "1899-12-30T20:30:00.000Z",
            "text": "Sorry. Aber du musst auch mal verstehen, dass ich mir was anderes von Arbeit verspreche als du. ",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Petra",
            "counter": 8,
            "timestamp": "1899-12-30T20:31:00.000Z",
            "text": "Das hab ich schon verstanden. Wollte doch nur sichergehen, dass du dich wohlfühlst.",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Franzi",
            "counter": 9,
            "timestamp": "1899-12-30T20:50:00.000Z",
            "text": "Ist doch kein Spa-Hotel hier ... Und auch nicht deine Steuerbehörde. Ich möchte mit meiner Arbeit gerne was verändern. Wohlfühlen ist zweitrangig.",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Petra",
            "counter": 10,
            "timestamp": "1899-12-30T20:55:00.000Z",
            "text": "Dieser Ehrgeiz! Also von mir hast du das nicht ;)",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Franzi",
            "counter": 11,
            "timestamp": "1899-12-30T21:23:00.000Z",
            "text": "Diese Technik, die die hier weiterentwickeln, könnte halt wirklich Leute vor dem Tod retten! Natürlich ist das ein Ansporn ...",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Petra",
            "counter": 12,
            "timestamp": "1899-12-30T21:40:00.000Z",
            "text": "Und wann hast du Feierabend?",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Franzi",
            "counter": 13,
            "timestamp": "1899-12-30T21:44:00.000Z",
            "text": "Weiß ich noch nicht.",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Petra",
            "counter": 14,
            "timestamp": "1899-12-30T21:50:00.000Z",
            "text": "Hoffentlich lassen sie euch bald gehen! Ihr könnt ja schlecht die ganze Nacht da sitzen.",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Franzi",
            "counter": 15,
            "timestamp": "1899-12-30T22:02:00.000Z",
            "text": "Mal sehen.",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Petra",
            "counter": 16,
            "timestamp": "1899-12-30T22:04:00.000Z",
            "text": "Ich leg mich jetzt hin. Bin gespannt auf deinen Bericht morgen!",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Franzi",
            "counter": 17,
            "timestamp": "1899-12-30T22:32:00.000Z",
            "text": "Gute Nacht, Mama.",
            "status": 0
        }
    ],
    "Fabian": [
        {
            "owner": "Franzi",
            "sender": "Fabian",
            "counter": 0,
            "timestamp": "1899-12-30T20:15:00.000Z",
            "text": "Hey Franzi, tut mir leid, dass ich mich so lange nicht mehr gemeldet habe ... hatte viel um die Ohren. Und zuhause gab's auch Stress. Naja, wem erzähl ich das. Ich hoffe, dir geht's gut? ",
            "status": 1
        },
        {
            "owner": "Franzi",
            "sender": "Franzi",
            "counter": 1,
            "timestamp": "1899-12-30T00:22:00.000Z",
            "text": "Schön von dir zu hören! Hier absoluter Wahnsinn: Bin bei #Lifehack genommen worden und jetzt Teil des Developer-Teams. Heute war der erste Tag, du wirst es nicht glauben: Tom und Lisa haben uns persönlich begrüßt! Sieht so als, als würde ich die jetzt jeden Tag sehen ... Wenn mir das einer vor fünf Jahren gesagt hätte, als wir beide zusammen im Lasermedizin-Seminar geschwitzt haben ... Hab das Gefühl, ich habe heute schon mehr gelernt als in einem Jahr Studium. Ein dreifaches Juchu! Und wo treibst du dich rum? Immer noch bei Google?",
            "status": 0
        }
    ],
    "Gruppenchat": [
        {
            "owner": "Franzi",
            "sender": "Administrator",
            "counter": 0,
            "timestamp": "1899-12-30T16:05:00.000Z",
            "text": "Herzlich Willkommen bei #Lifehack! In diesem Gruppenchat stehen wir deinem Team mit Rat und Tat zur Seite. Schön, dass ihr da seid!",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Franzi",
            "counter": 1,
            "timestamp": "1899-12-30T18:26:00.000Z",
            "text": "Bei mir läuft alles super, danke!",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Administrator",
            "counter": 2,
            "timestamp": "1899-12-30T20:00:00.000Z",
            "text": "Wenn ihr meine Hilfe braucht, chattet mich einfach an.",
            "status": 1
        },
        {
            "owner": "Franzi",
            "sender": "Franzi",
            "counter": 3,
            "timestamp": "1899-12-30T20:30:00.000Z",
            "text": "Bisschen peinlich, aber ich hab mit der Software zuletzt im 3. Semester gearbeitet ... wie funktioniert das nochmal?",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Administrator",
            "counter": 4,
            "timestamp": "1899-12-30T21:00:00.000Z",
            "text": "Ganz einfach und kein Problem :) Benutze einfach die Tasten W (oben), A (links), S (unten) und D (rechts), um den Distributor zu steuern. Mit C kannst du die Partikel anziehen, dann folgen sie dir treu - und mit V wieder abstoßen (aber das brauchst du heute eigentlich gar nicht).",
            "status": 1
        },
        {
            "owner": "Franzi",
            "sender": "Franzi",
            "counter": 5,
            "timestamp": "1899-12-30T21:05:00.000Z",
            "text": "Jetzt erinnere ich mich wieder! Dann kann ich ja jetzt die Partikel gleichmäßig über die drei Zentren verteilen. ",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Administrator",
            "counter": 6,
            "timestamp": "1899-12-30T21:07:00.000Z",
            "text": "Genau. Solange sie grün sind, flottieren sie frei, wenn sie gelb werden, binden sie sich an den Distributor. Weißt du ja :)",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Administrator",
            "counter": 7,
            "timestamp": "1899-12-30T21:15:00.000Z",
            "text": "Na, funktioniert es?",
            "status": 1
        },
        {
            "owner": "Franzi",
            "sender": "Franzi",
            "counter": 8,
            "timestamp": "1899-12-30T21:22:00.000Z",
            "text": "Nein, ich hab leider leichte Schwierigkeiten ... hast du vielleicht noch einen Tipp für mich?",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Administrator",
            "counter": 9,
            "timestamp": "1899-12-30T21:25:00.000Z",
            "text": "Na klar. Probier es doch mal mit der Taktik, erst ein paar Partikel in den Puls am Handgelenk zu bringen. Da werden die wenigsten benötigt, um die Daten auszulesen, also ist es nicht schlimm, wenn sie sich wieder verteilen, während du die Hirn- und Herzwerte ausliest ...",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Administrator",
            "counter": 10,
            "timestamp": "1899-12-30T21:27:00.000Z",
            "text": "Und denk dran: Alle drei Balken am oberen Bildschirmrand müssen gleichzeitig voll geladen sein, dann können genug Daten ausgelesen werden und du hast es geschafft!",
            "status": 0
        },
        {
            "owner": "Franzi",
            "sender": "Franzi",
            "counter": 11,
            "timestamp": "1899-12-30T21:35:00.000Z",
            "text": "Okay, wenn jetzt noch etwas ist, frage ich einfach Matt. Danke dir!",
            "status": 0
        }
    ]
};
