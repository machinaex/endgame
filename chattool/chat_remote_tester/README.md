Chattool Remote Tester
=========

A simple tester for the chat client.

----------

Funktionen
-------------

1. **Senden und Empfangen** von Daten an und vom Chattool via TCP
2. **Testen Chattool API** durch versenden von JSONs im entsprechenden Format
