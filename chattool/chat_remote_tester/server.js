var net = require('net');
var PORT = 6969;
var HOST = 'localhost';

function setPORT(){
  PORT = document.getElementById("PORTINPUT").value;
}

function setHOST(){
  HOST = document.getElementById("HOSTINPUT").value;
}

function sendJSON() {

  var msg = JSON.stringify({default:'0'}); // default msg when json is not valid

  try {
    var json = JSON.parse(document.getElementById("MESSAGEINPUT").value);
    console.log(json);
    msg = JSON.stringify(json);
    //console.log(msg);
  } catch (e) {
    console.log("NOT A VALID JSON!");
    return;
  }


  var client = new net.Socket();
  client.connect(PORT, HOST, function() {
    console.log('CONNECTED TO: ' + HOST + ':' + PORT);
    client.write(msg);
  });


  client.on('data', function(data) {
    console.log('DATA RECIVED: ' + data);
    client.destroy();
  });

  client.on('close', function() {
    console.log('Connection closed');
  });
}
