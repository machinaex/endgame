#!/bin/bash

# get ID with xinput -list

# Lock Keyboard Input
xinput set-prop 12 'Device Enabled' 1

# Lock Touchpad Input
xinput set-prop 10 'Device Enabled' 1
xinput set-prop 13 'Device Enabled' 1


# Hotkeys
# xinput set-prop 13 'Device Enabled' 1


# Blank Screen
# get Name with: xrandr --current
xrandr --output LVDS-1 --brightness 1
