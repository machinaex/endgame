#!/usr/bin/env python

# event chain:
# /etc/acpi/events headphone_plug or headphone_unplug >>
# /etc/acpi/headphone.sh with parameter >> this script >>
# message to app_handler via TCP

# setup:
# put event and script in /etc/acpi/events and /etc/acpi
# sudo chmod +x headphone.sh and sudo service acpid restart

import socket
import sys
import json

HOST, PORT = "localhost", 8002

if sys.argv[1] == 'plug':
    MSG = '{"all":{"audiojack": true}}'
else:
    MSG = '{"all":{"audiojack": false}}'


# Create a socket (SOCK_STREAM means a TCP socket)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    # Connect to server and send data
    sock.connect((HOST, PORT))
    sock.sendall(MSG)

    # Receive data from the server and shut down
    received = sock.recv(1024)
finally:
    sock.close()

print "Sent:     {}".format(MSG)
print "Received: {}".format(received)
