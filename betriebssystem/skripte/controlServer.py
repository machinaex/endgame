#!/usr/bin/env python


"""
To use:
$ alsamixer
then: use arrow keys and disable Auto-Mute-Mode
then: start this server and use it by sending
<absolutepathtoNPMapptorun> or {"headphones":true/false} and/or
{"speaker":true/false} to enable/disable sound coming out of the speakers or headphones
also legit:
{"npm":"<path>"}
"""
import socket
import sys
import os
import subprocess
import json

TCP_IP = '0.0.0.0'
TCP_PORT = 5005
BUFFER_SIZE = 1024  # Normally 1024, but we want fast response

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)



def startNPMapp(data):

    os.system("npm start --prefix %s &"%data)
    print "started"




while True:
    conn, addr = s.accept()

    print 'Connection address:', addr

    while True:
        data = conn.recv(BUFFER_SIZE)
        if not data: break
        print "received data:", data
        conn.send(data)

        try:
            data = json.loads(data);

            if "headphones" in data:
                print (data["headphones"])

                if data["headphones"] == True or data["headphones"] == "true":
                    print("headphones on")
                    os.system("amixer set 'Headphone' unmute")
                else:
                    print("headphones off")
                    os.system("amixer set 'Headphone' mute")
            if "speaker" in data:
                print (data["speaker"])

                if data["speaker"] == True or data["speaker"] == "true":
                    print("speaker on")
                    os.system("amixer set 'Speaker+LO' unmute")
                else:
                    print("speaker off")
                    os.system("amixer set 'Speaker+LO' mute")
            if "npm" in data:
                startNPMapp(data["npm"])
            if "lock" in data:
                if data["lock"] == True:
                    os.system("sh /home/endgame/APPS/betriebssystem/skripte/lock_all.sh")
                if data["lock"] == False:
                    os.system("sh /home/endgame/APPS/betriebssystem/skripte/unlock_all.sh")
            if "inactive" in data:
                if data["inactive"] == True:
                    os.system("sh /home/endgame/APPS/betriebssystem/skripte/inactive_all.sh")
                if data["inactive"] == False:
                    os.system("sh /home/endgame/APPS/betriebssystem/skripte/unlock_all.sh")
            if "inputs" in data:
                if data["inputs"] == True:
                    os.system("sh /home/endgame/APPS/betriebssystem/skripte/unlock_inputs.sh")
                if data["inputs"] == False:
                    os.system("sh /home/endgame/APPS/betriebssystem/skripte/lock_inputs.sh")
            if "screen" in data:
                if data["screen"] == True:
                    os.system("sh /home/endgame/APPS/betriebssystem/skripte/unlock_screen.sh")
                if data["screen"] == False:
                    os.system("sh /home/endgame/APPS/betriebssystem/skripte/lock_screen.sh")

        except:
            print (data)
            startNPMapp(data)

    conn.close()



print 'Argument List:', str(sys.argv)
#"""
