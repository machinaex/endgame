####Betriebssystem


####Autostart Skript

wird in LXSession configuration (in GUI: Einstellungen/Default Applications for LXSession/autostart

####skripte
beinhaltet diverse hilfe skripte, die Aufgaben auf Betriebssystem/shell ebene erledigen

#####controlServer.py

- startet via autostart.

- erwartet TCP *JSON-string* input auf Port 5005:

- akzeptierte Befehle:

`{"headphones":<boolean>}` switches sound stream to headphones on/off

`{"speaker":<boolean>}` switches sound stream to speakers on/off

`{"npm":<pathToNPMappToStart>}` starts npm app

`{"lock":<boolean>}` locks keyboard and mouse via skript/lockall.sh or skript/unlockall.sh (UNTESTED!)


#####Requirements controlServer.py

`$ alsamixer`

then: use arrow keys and disable Auto-Mute-Mode

####Audijack Plugged Event

Event Chain:

1. /etc/acpi/events headphone_plug or headphone_unplug wird vom System ausgel�st

2. /etc/acpi/headphone.sh wird mit den Parametern "plug" oder "unplug" aufgerufen

3. ./betriebssystem/skripte/headphone.py wird mit entsprechendem Parameter aufgerufen und sendet eine TCP Message an den App Handler '{"all":{"audiojack": false}}'

Setup: 

0. install acpi

1. put event 'headphone_plug' and 'headphone_unplug' in '/etc/acpi/events' 

2. put  'headphone.sh' in /etc/acpi

3. sudo chmod +x headphone.sh 

4. sudo service acpid restart
