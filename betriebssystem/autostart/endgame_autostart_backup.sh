#!/bin/bash

xrandr --output LVDS-1 --brightness 1 &

sleep 2

npm start --prefix ~/APPS/app_handler/ &

sleep 2

npm start --prefix ~/APPS/login_app/ &

sleep 2

npm start --prefix ~/APPS/level1_deploy/ &

sleep 2

npm start --prefix ~/APPS/lightclient/ &

sleep 2

npm start --prefix ~/APPS/level2_website_app/ &

sleep 2

npm start --prefix ~/APPS/level2_profil_app/ &

sleep 2

npm start --prefix ~/APPS/level2_phone_app/ &

sleep 2

npm start --prefix ~/APPS/statusbalken/ &

sleep 2

npm start --prefix ~/APPS/questbook/ &

sleep 2

npm start --prefix ~/APPS/level0/ &

sleep 2

npm start --prefix ~/APPS/level3_playground/ &

sleep 2

npm start --prefix ~/APPS/level3_inventory/ &

sleep 2

npm start --prefix ~/APPS/wolfenstein3d/ &

sleep 2

npm start --prefix ~/APPS/solitaire/ &

sleep 2

npm start --prefix ~/APPS/file-explorer/ &

sleep 2

python ~/APPS/betriebssystem/skripte/controlServer.py &



sleep 4

curlftpfs 'machinaeX:ProArd2010!@192.168.0.102/Async/Temp' ~/TEMP -o nonempty &
