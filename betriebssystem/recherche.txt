///////////////////////////////////////////////////////////////////////////////////////////////////////////
// lUBUNTU einrichten
///////////////////////////////////////////////////////////////////////////////////////////////////////////

Lubuntu:

  Einrichten:

  1.  Automatisches Anmelden bei OS-Installation wählen (User: player, pw: endgame || arduino2010)

      Install Virtualbox Guest Additions

  2.  install git (apt-get install git)
  3.  install curl (apt-get install curl)
  4.  install node (curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
                    sudo apt-get install -y nodejs)
  5.  install electron (sudo npm install electron -g)

  6.  git clone endgame repo (git clone in /home/player/)

  7.  Kontextmenü auf Desktop entfernen & Keybindings entfernen & Mousebindings entfernen
      - Desktop Einstellungen > Erweitert > "Beim Klicke auf den Desktop ..." anwählen
      - in "/home/player/.config/openbox" "menu.xml" und "lubuntu-rc.xml" ersetzen (siehe Ordner Config Files)
      - in "lubuntu-rc.xml" sind die Rechtsklicks mit der Maus auskommentiert und die Tastaturkürzel entfernt

  8.  Openbox Konfigurations Manager
      - zusätzliche Arbeitsfläschen entfernen
      - Verschieben auf Arbeitsflächen abwählen
      - Theme installieren ??????????

  9.  Desktop Symbole ausschalten
      - Desktop Einstellungen > Desktop Symbole abwählen
      - Desktop als Dateipfad abwählen

  10. Panel ausblenden & Autostart einrichten:
      - Einstellungen -> Default applications for LXSessions -> Core Applications (lxpanel löschen)
      - Default applications for LXSessions -> Autostart "/home/player/endgame/betriebssystem/autostart/endgame_autostart.sh" hinzufügen

  11. Energieverwaltung
      - XFCE Energieverwaltung alle Einstellungen auf "nie"
      - Light-Locker deaktivieren


  Nützliche Hilfe:
  https://forum.lxde.org/viewtopic.php?f=24&t=31525
  https://ubuntuforums.org/showthread.php?t=2197247
  https://wiki.ubuntuusers.de/Openbox/
  - str+alt+p > Terminal
  - lxpanel > startet GUIs
  - Tastatur/Touchpad sperren: mit "xinput -list" ID herausfinden und dann "xinput set-prop <ID> 'Device Enabled' 0"
  - Bildschirmhelligkeit regeln: mit "xrandr --current" Name herausfinden und dann "xrandr --output <NAME bspw.: LVDS-1> --brightness 0.3"

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// xUBUNTU einrichten
///////////////////////////////////////////////////////////////////////////////////////////////////////////

Xubuntu:

  Tipps:
      Einstellungen öffnen: xfce4-settings-manager

  Keyboard Shortcuts unterbinden:
    settings manager -> keyboard -> shortcuts (alle löschen außer: settings manager -> window manager -> keyboard)
    settings manager -> window manager -> keyboard (alle löschen)

  Panel ausblenden:
    settings manager -> Sitzung und Startverwaltung (xfce4-panel beenden und Sitzung speichern)
    (in /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-session.xml xfce4-panel auskommentieren)

  Schreibtisch aufräumen:
    Einstellung -> Schreibtisch -> Symbole (alle abwählen)
    KONTEXTMENÜ AUSSCHALTEN ????
