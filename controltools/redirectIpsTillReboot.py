#!/usr/bin/env python

import sys
import os

endgameDefault = ["192.168.0.2","192.168.0.3"]
for i in range(33):
	endgameDefault.append("192.168.0."+str(200+i))

data = sys.argv[1:]

if len(data) == 0:
	data = endgameDefault

print("##########################\nIN ORDER FOR THIS TO WORK: ")
print(" make sure /etc/sysctl.conf has the line ")
print("   'net.ipv4.ip_forward = 1' \n present and uncommented!\n##########################\n\n")


for item in data:

	os.system("sudo iptables -t nat -A OUTPUT -d %s -j DNAT --to-destination 127.0.0.1 &"%item)

	print ("redirecting %s to 127.0.0.1"%item)




print ("############################")
print ("done")
#"""