import socket
import sys


def Main():

	message = ""

	# first, check if there are arguments passed into the script
	# and fill them into the message var
	for arg in sys.argv[3:]:
	   message = message + " " + arg


	host = sys.argv[1]
	port = int(sys.argv[2])
#	host = "127.0.0.1" # zum testen auf derselben maschine
#	host = "192.168.178.47" # locales netzwerk
#	host = "zx3b7zdsov9e6dvt.myfritz.net" # verbinde dich via internetz...
#	port = 8030

	print "trying to connect to " + host

	s = socket.socket()
	s.connect((host, port))

	if message == "":
		message = raw_input("--> ")

	if message != 'q':
		s.send(message)
		data = s.recv(1024) #waits
		print 'Recieved from server: ' + str(data)
		#message = raw_input("--> ")
	s.close()

if __name__ == '__main__':
	Main()
