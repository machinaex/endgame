#!/usr/bin/env python

import socket
import sys
import os
import subprocess

TCP_IP = 'localhost'
TCP_PORT = 5005
BUFFER_SIZE = 1024  # Normally 1024, but we want fast response

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

while True:    
    conn, addr = s.accept()

    print 'Connection address:', addr

    while True:
        data = conn.recv(BUFFER_SIZE)
        if not data: break
        print "received data:", data
        conn.send(data)

        #subprocess.Popen(["npm", "start --prefix ~/endgame/file-explorer &"])
        os.system("npm start --prefix %s &"%data)
        print "started"
        #os.system("gnome-terminal --window-with-profile=testprofile -e 'npm start --prefix ~/endgame/chattool/chat_client/' &");
        #lxterminal --title="MyScriptWindow" -e "bash -c ./somescript.sh;bash"\

    conn.close()



print 'Argument List:', str(sys.argv)

