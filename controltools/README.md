####install pssh

sudo apt-get update; sudo apt-get install pip; sudo pip install pssh;

then: make a pssh-hosts file with all ssh clients like this

192.168.0.201:22
192.168.0.202:22
...


####cssh config

install cssh: 

[https://github.com/brockgr/csshx](https://github.com/brockgr/csshx) (Mac)

1) get file csshXrc.sh from git
2) execute `sh ccshXrc.sh`
3) enjoy your puppet army!

or

`sudo apt-get install ` (Linux)

put file csshrc into /etc/ (Linux)

1) start cssh
2) choose option "Add Hosts or Clusters"
3) pick endgameAll
4) login and be a puppet master
