####install pssh

`sudo apt-get install pip; sudo pip install pssh`

####reboot all hosts in pssh_hosts:

`pssh -h pssh_hosts -l root -A "reboot"`

---------------

####close all hosts:

`pssh -h pssh_hosts -l root -A "shutdown -h now"`

---------------

####do whatever and get back the stdout:

`pssh -h pssh_hosts -l <username> -A -i "<bash command of your choice>"`

---------------

Remember though: lots of commands need root as user.

because of this the PermitRootLogin flag in /etc/ssh/sshd_config has to be checked to 'yes' (if password shall be used) or at least to 'without-password' (if keyaccess)