const r = require('rethinkdb');
const fs = require('fs');
const dialog = require('electron').remote.dialog;
const app = require('electron').remote.app;

var connection = null;
var HOST = 'localhost';
var PORT = 28015;

var tempPath = null;

function setPORT(value){
  PORT = value;
}

function setHOST(value){
  HOST = value;
}

function connect(){
  // close existing connections
  disconnect();

  // open a connection. run queries via the varibale connection.
  r.connect( {host: HOST, port: PORT}, function(err, conn) {
    if (err) throw err;
    connection = conn;
    console.log(connection);
    console.log("connection open");
    listDB();

    var connectBtns = document.getElementsByClassName("connect");
    for (var i = 0; i < connectBtns.length; i++) {
      connectBtns[i].disabled = true;
    }
    document.getElementById("import").disabled = false;
    document.getElementById("path").disabled = false;
    document.getElementById("export").disabled = false;
  });
}

function disconnect(){
  if (connection){
    connection.close(function(err) {
      if (err) throw err;
      console.log(connection);
      console.log("connection closed");
      removeOptions('databases');
      removeOptions('tables');
      var connectBtns = document.getElementsByClassName("connect");
      for (var i = 0; i < connectBtns.length; i++) {
        connectBtns[i].disabled = false;
      }
      document.getElementById("export").disabled = true;
      document.getElementById("import").disabled = true;
      document.getElementById("path").disabled = true;
    });
  }
}

function exportTable(){
  r.db(document.getElementById('databasesEXP').value).table(document.getElementById('tablesEXP').value).run(connection, function(err, table) {
    if (err) throw err;
    table.toArray(function(err, results) {
      if (err) throw err;
      saveFile(JSON.stringify(results, null, '\t'));
    });
  });
}

function importTable(){
  var targetTable = document.getElementById('tableIM').value;
  var targetDB = document.getElementById('databasesIM').value;
  if (targetTable && targetDB && tempPath) {
    r.db(targetDB).tableDrop(targetTable).run(connection, function(err, msg){
      r.db(targetDB).tableCreate(targetTable).run(connection, function(err, msg){
        if (err) throw err;
        r.db(targetDB).table(targetTable).insert(JSON.parse(fs.readFileSync(tempPath, "utf8"))).run(connection, function(err, msg){
          if (!err) {
            dialog.showMessageBox({ message: "This table has been imported!",
            buttons: ["OK"] });
          } else {
            dialog.showErrorBox("File Import Error", err.message);
          }
        });
      });
    });
  }
}

function listDB(){
  r.dbList().run(connection, function(err, dbList) {
    if (err) throw err;
    populateSelection('databases', dbList);
  });
}

function listTable(DB){
  removeOptions('tables');
  r.db(DB).tableList().run(connection, function(err, tableList) {
    if (err) throw err;
    populateSelection('tables', tableList);
  });
}

function populateSelection(elementID, options){
  var selectboxes = document.getElementsByClassName(elementID);
  for (var i = 0; i < selectboxes.length; i++) {
    for (var k = 0; k < options.length; k++) {
      var option = document.createElement('option');
      option.text = option.value = options[k];
      selectboxes[i].add(option, 0);
    }
  }
}

function removeOptions(elementID){
  var selectboxes = document.getElementsByClassName(elementID);
  for (var i = 0; i < selectboxes.length; i++) {
    for(var k = selectboxes[i].options.length - 1 ; k >= 0 ; k--){
      selectboxes[i].remove(k);
    }
  }
}

function saveFile(json) {
  dialog.showSaveDialog({title: 'Save Table as JSON',defaultPath:app.getAppPath(),
  filters: [{ name: 'json', extensions: ['json']}]}, function (fileName) {
    if (fileName === undefined) return;
    fs.writeFile(fileName, json, function (err) {
      if (!err) {
        dialog.showMessageBox({ message: "This table has been saved!",
        buttons: ["OK"] });
      } else {
        dialog.showErrorBox("File Save Error", err.message);
      }
    });
  });
}

function openFile() {
  dialog.showOpenDialog({title: 'Save Table as JSON',defaultPath:app.getAppPath(),
  filters: [{name: 'json', extensions: ['json']}]}, function (fileNames) {
    if (fileNames === undefined) return;
    var fileName = fileNames[0];
    document.getElementById('path').innerHTML = ""+fileName;
    tempPath = ""+fileName;
  });
}
