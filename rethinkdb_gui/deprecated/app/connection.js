// http://jeremydorn.com/json-editor/
// https://github.com/jdorn/json-editor

const r = require('rethinkdb');
const fs = require('fs');
const je = require('json-editor');

var connection = null;
const HOST = 'localhost';
const PORT = 28015;

var editor;
var default_schema = JSON.parse(fs.readFileSync("./schema/schema_array.json", "utf8"));

// open a connection. run queries via the varibale connection.
r.connect( {host: HOST, port: PORT}, function(err, conn) {
  if (err) throw err;
  connection = conn;
  initEditor();
  getTable();
});

/* create test database with entrys:
r.db('test').tableCreate('authors').run(connection, function(err, result) {
    if (err) throw err;
})


r.table('authors').insert([
    { name: "William Adama", tv_show: "Battlestar Galactica",
      posts: [
        {title: "Decommissioning speech", content: "The Cylon War is long over..."},
        {title: "We are at war", content: "Moments ago, this ship received word..."},
        {title: "The new Earth", content: "The discoveries of the past few days..."}
      ]
    },
    { name: "Laura Roslin", tv_show: "Battlestar Galactica",
      posts: [
        {title: "The oath of office", content: "I, Laura Roslin, ..."},
        {title: "They look like us", content: "The Cylons have the ability..."}
      ]
    },
    { name: "Jean-Luc Picard", tv_show: "Star Trek TNG",
      posts: [
        {title: "Civil rights", content: "There are some words I've known since..."}
      ]
    }
]).run(connection, function(err, result) {
    if (err) throw err;
})
*/


function getTable(){
  r.table('authors').run(connection, function(err, cursor) {
    if (err) throw err
    cursor.toArray(function(err, result) {
      if (err) throw err;
      console.log(JSON.stringify(result, null, 2));
      editor.setValue(result);
    });
  });
}

// Initialize the editor with a JSON schema
function initEditor() {
  editor = new JSONEditor(document.getElementById('editor_holder'),default_schema);
}
