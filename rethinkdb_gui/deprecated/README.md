Visualisation and Interaction with RethinkDB
=========

Test GUI für RethinkDB

----------

TODO
-------------
1. ...

Funktionen
-------------

1. **Anzeigen von Datenbankeinträgen**
2. **Ändern und Hinzufügen von Datenbankeinträgen**
3. **Tabellen exportieren
'rethinkdb export -e databasename.tablename -d export_directory'
4.**Tabellen importieren
'rethinkdb import -f filename.json --table databasename.tablename --force'

Anleitung
-------------

1. RethinkDB starten "rethingdb --bind all"

Ordnerstruktur
------------

```
RethinkDB_GUI
├── app
│   └── connection.js
├── main.html
├── main.js
├── node_modules
│   ├── bluebird
│       └── ...
│   ├── json-editor
│       └── ...
│   └── rethinkdb
│       └── ...
├── package.json
├── README.md
└── schema
    └── schema_default.json


```
