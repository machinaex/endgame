Visualisation and Interaction with RethinkDB
=========

Test GUI für RethinkDB

----------

TODO
-------------
1. ...

Funktionen
-------------

1. **Anzeigen von Datenbankeinträgen**
mit der Software ReQLPro können einzelne Datenbankeinträge, oder Tabellen angezeigt werden
2. **Ändern und Hinzufügen von Datenbankeinträge**
mit der Software ReQLPro können einzelne Datenbankeinträge, oder Tabellen angezeigt werden
3. **Tabellen exportieren**
4.**Tabellen importieren**
mit der Software ReQLPro können Tabellen importiert werden
'rethinkdb import -f filename.json --table databasename.tablename --force'
mit der Electron App können einzelne Tabellen exportiert werden
'rethinkdb export -e databasename.tablename -d export_directory'

Anleitung
-------------

1. RethinkDB starten "rethinkdb --bind all"

Ordnerstruktur
------------

```
RethinkDB_GUI
├── app
│   └── rethinkdb.js
├── main.html
├── main.js
├── node_modules
│   ├── bluebird
│       └── ...
│   └── rethinkdb
│       └── ...
├── package.json
├── README.md


```
