const fs = require('fs');
let dirty = JSON.parse(fs.readFileSync("./input/TWINE.json", "utf8"));

/* 1. Schritt: cleans TWISON json
removeKey(dirty, ['position', 'pid']);
renameKey(dirty, 'links', 'decisions');
textToMsg(dirty);
removeKey(dirty, ['text']);
cleanLinks(dirty);
fs.writeFile('./output/clean_twine.json',JSON.stringify(dirty,null,2),'utf8',
  function(){
    console.log('SUCCESSFULLY WRITE NEW JSON!');
  }
);
*/

/* 1. Schritt: cleans twine json */
removeKey(dirty, ['position', 'pid', 'tags', 'content']);
renameKey(dirty, 'childrenNames', 'decisions');
cleanUp(dirty, 'decisions');
//parseTags(dirty);
//cleanLinks(dirty);
fs.writeFile('./output/clean_twine.json',JSON.stringify(dirty,null,2),'utf8',
  function(){
    console.log('SUCCESSFULLY WRITE NEW JSON!');
  }
);

/* 2. Schritt: alle Values einmal in jeweils eine Zeile drucken für die Übersetzung
let liste = [];
jsonToList(dirty, '');
fs.writeFile('./output/liste.txt',liste.join("\n"),'utf8',
  function(){
    console.log('SUCCESSFULLY WRITE NEW LISTE!');
  }
);
*/

/* 3. Schritt ersetzte die original values mit denen aus einer json von englischen texten
let structure = JSON.parse(fs.readFileSync('./output/clean_twine.json', "utf8"));
listeToJSON(structure, '')
fs.writeFile('./output/yeah_clean_twine.json',JSON.stringify(structure,null,2),'utf8',
  function(){
    console.log('SUCCESSFULLY WRITE NEW JSON!');
  }
);
*/

function jsonToList(json, path){
  for (var item in json) {
      if (typeof(json[item]) == 'object'){
        jsonToList(json[item], path + (path ? '.' : '') + item);
      }
      else{
        if (!(liste.indexOf(json[item]) > -1)) {
          liste.push(json[item]);
        }
      }
  }
}

function listeToJSON(json, path){
  for (var item in json) {
      if (typeof(json[item]) == 'object'){
        listeToJSON(json[item], path + (path ? '.' : '') + item);
      }
      else{
        json[item] = "Yeah! " + json[item]; // hier käme dann der englische text aus der liste rein
      }
  }
}

///////////////////////////////////////////////////////////////////////////////
// HELPER FUNCTIONS AND DIRTY HARDCODED HACKS. SO SAD
///////////////////////////////////////////////////////////////////////////////

function removeKey(json, keys){
  for (let k = 0; k < keys.length; k++) {
    json.forEach(function(item) {
      delete item[keys[k]]
    });
  }
}

function renameKey(json, oldkey, newkey){
  json.forEach(function(item) {
    if (oldkey in item) {
      item[newkey] = item[oldkey];
      delete item[oldkey];
    }
  });
}

function parseTags(json){
  json.forEach(function(item) {
    if('msg' in item){
      for (var i = 0; i < item.msg.length; i++) {
        item.msg[i] = hackedydo(item.msg[i]);
      }
    }
  });
}

function cleanLinks(json){
  json.forEach(function(item) {
    console.log(item);
    if ('decisions' in item) {
      let liste = [];
      for (var i = 0; i < item.decisions.length; i++) {
        liste.push(item.decisions[i].link);
      }
      item.decisions = liste;
    }
  });
}

var messages = [];

function textToMsg(json){
  for (var i = 0; i < json.length; i++) {
    messages = [];
    getMsg(json[i].text);
    if (messages.length > 0) {
      json[i].msg = messages;
    }
    //console.log(messages);
  }
}

// gets tag/value in string {{tag}}value{{/tag}} for 2
function hackedydo(content){
  let ptrn = /\{\{((\s|\S)+?)\}\}((\s|\S)+?)\{\{\/\1\}\}/gm;
  let match = ptrn.exec(content);
  let obj = {}
  obj['text'] = match['input'].replace(match[0], '');
  obj['sender'] = match[3];
  messages.push(obj);
}

function getMsg(content, msg){
  let ptrn = /\{\{((\s|\S)+?)\}\}((\s|\S)+?)\{\{\/\1\}\}/gm;
  let match = ptrn.exec(content);
  if (match != null) {
    let obj = match[3];
    hackedydo(obj);
    let rest = match['input'].replace(match[0], '');
    if (hasMsg(rest)) {
      getMsg(rest);
    }
  }
}

function hasMsg(content){
  if(content.indexOf("{{msg}}") !== -1){
    return true;
  } else {
    return false;
  }
}

function cleanUp(json, key){
    json.forEach(function(item) {
      for (var i = 0; i < item[key].length; i++) {
        item[key][i] = cleanupDecisions(item[key][i]);
      }
    });
}

function cleanupDecisions(item){
    return item.replace('[[','').replace(']]','');
}
