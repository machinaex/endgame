const Mustache = require("mustache");
const fs = require('fs');
const $ = require('jquery');
const swal = require('sweetalert');
require('howler');

const CONSTANTS = {
  contentPath: '../../CONTENT/level2/profil/',
  soundTyping: 'ASSETS/typing.mp3',
  htmlTemplate: 'ASSETS/TEMPLATE.html',
  jsonContent: '/CONTENT_', //DE.json
  jsonEntrys: '/ENTRYS_', //DE.json
  globalAssets: '/ASSETS',
  contentContainer: 'pagecontent',
  scoreStep: 10,
};
let PATH;
let PERSON;
let SCORED;
let WEBSITE = false; // is Website open?
let CHAT = false; // "
let PHONE = false; // "

let TYPING;
let ENTRYS = [];
let ALLENTRYS = [];
let STASH = [];
let CONTENT = {};
let lock = false;

main.init = function(myappdata, language){
  let TEMPLATE;
  // set paths to content folder and load files
  try {
    language = language.toUpperCase();
    PATH = CONSTANTS.contentPath;
    PERSON = myappdata.person.substring(3);
    TEMPLATE = fs.readFileSync(PATH+CONSTANTS.htmlTemplate, "utf8");
    CONTENT = JSON.parse(fs.readFileSync(PATH+PERSON+CONSTANTS.jsonContent+language+'.json', "utf8"));
    CONTENT.path = CONSTANTS.contentPath+PERSON+CONSTANTS.globalAssets;
    ALLENTRYS = JSON.parse(fs.readFileSync(PATH+PERSON+CONSTANTS.jsonEntrys+language+'.json', "utf8"));
  } catch (e) {
    console.log('Error on load files in main init with: ' + e);
    player.setAppdata({"load_files":"ERROR!"});
  } finally {
    // set Path Variable for HTML Assets, render and append to app
    renderTemplate(TEMPLATE, CONTENT);
    player.setAppdata({"load_files":"CHECK!"});
    WEBSITE = false;
    CHAT = false;
    PHONE = false;

    // init phone and website
    let message = {"website_app":{"init":myappdata, "language": language}};
    main.send(message);
    // init phone and website
    let message2 = {"phone_app":{"init":myappdata, "language": language}};
    main.send(message2);
  }
}

main.receive = function(mydata){
  if('trigger' in mydata && !lock){
    let index = indexByValue(ALLENTRYS, 'id', mydata.trigger.id);

    if (mydata.trigger.id == 'ID0') {
      document.getElementById("profilimage").src= PATH+PERSON+"/fakebook.png";
      setQuestbook({"command":"set", "tags":"bild", "status":"checked"});
      player.score(CONSTANTS.scoreStep*3);
    }

    if(index>-1 && isNew(ALLENTRYS[index])){
      let obj = ALLENTRYS[index];
      ENTRYS.push(obj);

      if (mydata.trigger.id == 'ID13') {
        setQuestbook({"command":"set", "tags":"phone", "status":"checked"});
        player.score(CONSTANTS.scoreStep*5);
      }
      else if (mydata.trigger.id == 'ID5') {
        setQuestbook({"command":"set", "tags":"chat", "status":"checked"});
        player.score(CONSTANTS.scoreStep*3);
      }
      else {
        player.setAppdata({"entry":ENTRYS});
        player.score(CONSTANTS.scoreStep);
      }

      switch (ENTRYS.length) {
        case 3:
        setQuestbook({"command":"set", "tags":"3infos", "status":"checked"});
        break;
        case 6:
        setQuestbook({"command":"set", "tags":"6infos", "status":"checked"});
        break;
        case 12:
        setQuestbook({"command":"set", "tags":"12infos", "status":"checked"});
        break;
        default:

      }

      STASH.push(obj);
      if (STASH.length == 1) {
        addInfo(obj, 0, 40);
        TYPING = createAudio(PATH+CONSTANTS.soundTyping, { volume: .3 });
      }
    }
  }

  if ('active' in mydata) {
    if (mydata.active) {
      let message = {
        "command":"checkbox",
        "tags":["level2 bild", "level2 3infos", "level2 6infos", "level2 chat", "level2 phone", "level2 12infos"],
        "content":["Finde ein Bild der Zielperson", "Finde 3 Informationen", "Finde 6 Informationen", "Chate mit der Zielperson", "Rufe die Zielperson an", "Finde alle Informationen"]
      };
      setQuestbook(message);
    }
    else {
      let message = {
        "command":"remove",
        "tags":["level2"]
      };
      setQuestbook(message);
    }
  }

  if ('lock' in mydata) {
    if (mydata.lock) {
      document.getElementById('phone').style.display = "none";
      document.getElementById('fakebook').style.display = "none";
      lock = true;
    } else {
      document.getElementById('phone').style.display = "inline-block";
      document.getElementById('fakebook').style.display = "inline-block";
      lock = false;
    }
  }

}


// renders Template with selected language and appends to index.html
renderTemplate = function(_template, _content){
  // clean up html element container
  let container = document.getElementById(CONSTANTS.contentContainer);
  while (container.firstChild) {
    container.removeChild(container.firstChild);
  }
  // append newly renderd content
  let site = Mustache.to_html(_template, _content);
  container.innerHTML += site;

  /*
  $("#website").click(function () {
    if ($('#tag3').text() == "") {
      swal({
        title: "URL missing!",
        text: "There is no Link. Look for it!",
        icon: "error"
      });
    } else {
      swal({
        title: "Website wird geöffnet!",
        text: "Du kannst Informationen durch Mausklicks zum Profil hinzufügen!",
        icon: "info",
      }).then((value) => {
        let message = {"website_app":{"WINDOW": ["focus()","show()"], "render":"website"}};
        if (value) {
          main.send(message);
        }

        if (!WEBSITE) {
          // QUESTBOOK
          setQuestbook({"command":"set", "tags":"website", "status":"checked"});
          WEBSITE = true;
        }

      });
    }
  });
  */
  $("#fakebook").click(function () {
    swal({
      title: "Fakebook Profil wird geöffnet!",
      text: "Du kannst Informationen durch Mausklicks zum Profil hinzufügen!",
      icon: "info",
    }).then((value) => {
      let message = {"website_app":{"WINDOW": ["focus()","show()"], "render":"fakebook"}};
      if (value) {
        main.send(message);
      }
    });
  });

  /*
  $("#websitechat").click(function () {
    if ($('#tag4').text() == "") {
      swal({
        title: "Chat-Handle missing!",
        text: "We don't have a name to open chat with target. Look for it!",
        icon: "error"
      });
    } else {
      let message = {"website_app":{"WINDOW": ["focus()","show()"], "chat": true, "render":"website"}};
      main.send(message);

      if (!CHAT) {
        // QUESTBOOK
        setQuestbook({"command":"set", "tags":"chat", "status":"checked"});
        player.score(CONSTANTS.scoreStep);
        CHAT = true;
      }

    }
  });
  */
  $("#phone").click(function () {
    if ($('#tag5').text() == "") {
      swal({
        title: "Number missing!",
        text: "We don't have a phone number to call target. Look for it!",
        icon: "error"
      });
    } else {

      swal({
        title: "Vorsicht, Direktkontakt!",
        text: "Jetzt wirds ernst: Im direkten Kontakt mit der Zielperson darfst Du dir keine Fehler erlauben.",
        icon: "info",
      }).then((value) => {
        let message = {"phone_app":{"WINDOW": ["focus()","show()"], "start": true}};
        if (value) {
          main.send(message);
        }
      });

      if (!PHONE) {
        // QUESTBOOK
        player.score(CONSTANTS.scoreStep);
        PHONE = true;
      }

    }
  });

  // add eventlistener to sidebar
  $("a").click(function () {
    $("a").css("color", "rgba(255, 255, 255, 0.35)");
    $(this).css("color", "rgba(255, 255, 255, 0.95)");
  });
  $('a').hover(function(){$(this).toggleClass('nav_hover');});
  // select first entry on sidebar
  $('a').first().click();

  $('body').on('drop', function(event) { event.preventDefault(); });
  $('body').on('dragstart', function(event) { event.preventDefault(); });
  $('body').on('selectstart', false);
}

addInfo = function(message, index, interval) {
  //console.log(message);
  let formularfeld = document.getElementById(message.target);
  formularfeld.style.animationName = "typing";


  let section = $('#'+message.target).closest("section").prop("id");
  location.href = "#";
  location.href = "#"+section;
  section = 'a[href=\"#'+section+'\"]';
  $(section).click();


  if (index < message.text.length) {
    formularfeld.innerHTML += (message.text[index++]);
    setTimeout(function () { addInfo(message, index, interval); }, interval);
  } else {
    formularfeld.innerHTML += " ";
    formularfeld.style.animationName = "none";
    TYPING.stop();
    STASH.shift();

    if (STASH.length > 0) {
      addInfo(STASH[0], 0, 40);
      TYPING = createAudio(PATH+CONSTANTS.soundTyping, {  volume: .3});
    }

  }
}


// function um audio objekte zu erzeugen
createAudio = function(_file, _options){
  let defaults = {
    src: [_file],
    autoplay: true,
    loop: true,
    volume: .2,
    onend: function() {
    },
    onplay: function() {
    },
    onload: function(){
    }
  };
  return new Howl(Object.assign({}, defaults, _options));
}

isNew = function(obj){
  for(var i=0; i<ENTRYS.length; i++){
    if(JSON.stringify(ENTRYS[i]) === JSON.stringify(obj) ) {
      swal({
        title: "duplicate data",
        text: "This information is already saved!",
        icon: "warning",
        buttons: false,
        timer: 3000
      });
      return false;
    }
  }
  return true;
}

// gets index based on key value in one flat json
indexByValue = function(_json, _key, _value) {
  for(var i = 0; i < _json.length; i += 1) {
    if(_json[i][_key] === _value) {
      console.log("corresponding passage index: " + i);
      return i;
    }
  }
  console.log("no corresponding passage");
  return -1;
}

setQuestbook = function(msg){
  let message = {"questbook": msg};
  main.send(message);
}
