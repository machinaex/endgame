$ = require('jquery');

require("howler");

//var bamboo = new Howl({src: ['./bamboo_short.mp3']});
//var chlp = new Howl({src: ['./chlp.mp3']});
var open = new Howl({src: ['./questbook_open.wav']});
var close = new Howl({src: ['./questbook_close.wav']});
var itemchecked = new Howl({src: ['./questbook_itemchecked.wav']});
var newitem = new Howl({src: ['./questbook_newitem.wav']});

let appdata = {"listitems":[]};

document.addEventListener('auxclick', callback, false);
  function callback(e) {
    console.log('not left mouse click');
    e.preventDefault();
    return
  }


main.init = function(myappdata, language)
{
	appdata = myappdata;
	console.log("received appdata:");
	console.log(appdata);

	if ("listitems" in appdata)
	{
		if (appdata.listitems.length > 0)
		{
			// there are items i the list already. so we do not have a fresh start.
			// then: let's first clear the database, so we do not double all the data:
		//	let copy = JSON.parse(JSON.stringify(appdata));
		//	appdata.listitems = [];
			/*
			// and then initiate all objects that were provided through appdata.listitems:
			copy.listitems.forEach(function(item){
				item = JSON.parse(JSON.stringify(item));
				console.log(item);
				checkCommand(item);

			});
 		*/
		}
	}


}

//console.log(appdata);

console.log(main.mainWindow);


main.receive = function(mydata)
{
	console.log(mydata);
	if (mydata.WINRESPONSE != undefined)
	{
		console.log(mydata);
		if (mydata.WINRESPONSE == true)
		{
			main.send({"WINDOW":["hide()"]})
			close.play();
		}
		else
		{
			main.send({"WINDOW":["show()"]})
			open.play();
		}
	}

	if (mydata.command != undefined)
	{
			let command = mydata;

			checkCommand(command);
	}

}



function isMatch(element)
{
	//console.log(element);//console.log(this);//console.log(element[this[0]] == this[1]);
	return element[this[0]] == this[1];
}

console.log("neue version2");
function checkCommand(cmd)
{
	console.log(cmd);
	console.log(cmd.command == "toggleQuestbook");
	if (cmd.command == "toggleQuestbook")
	{
		//console.log("asking if isVisible");
		main.send({"WINDOW":["isVisible()"]});
	}

	if (!Array.isArray(cmd.content))
	{
		cmd.content = [cmd.content];
	}
	if (!Array.isArray(cmd.tags))
	{
		cmd.tags = [cmd.tags];
	}
	//console.log(cmd.content);

		//console.log(cmd);
		if (cmd.status == undefined){cmd.status = "unchecked"}

		if (cmd.command == "checkbox")
		{
			// show Questbook for 4 seconds than hide it
			main.send({"WINDOW":["show()"],"statusbalken":{"highlight":true}});//
			setTimeout(function(){main.send({"WINDOW":["hide()"]});},4000);

			newitem.play();

			cmd.content.forEach(function(c,i){

				console.log("adding checkbox: "+c)

				let classes = cmd.tags[i];

				let checkbox = "<div class=\""+classes+" chckdiv\"><input type=\"checkbox\" class=\"regular-checkbox\" disabled=\"disabled\"></input><span class=\"label\">"+c+"</span></div>"
				//console.log(classes);

				$("#pagecontent").prepend(checkbox);


				checkCommand({"command":"set", "tags":classes, "status":cmd.status, "content":c});
				/*if (cmd.status == "checked"){state = "checked"}
				else if (cmd.status == "x"){state = "indeterminate"}
				else {state = ""}
				*/
				c = {"command":cmd.command,"tags":classes,"content":c, "status":cmd.status};
				console.log(c);
				appdata.listitems.push(c);

			});

			console.log(appdata);
			player.setAppdata(appdata);

		}

		if (cmd.command == "html")
		{
			main.send({"WINDOW":["show()"]});//
			setTimeout(function(){main.send({"WINDOW":["hide()"]});},4000);

			newitem.play();
			cmd.content.forEach(function(c,i){

				let classes = cmd.tags[i];
				let checkbox = "<div class=\""+classes+" chckdiv\">"+c+"</div>"
				console.log(classes);

				$("#pagecontent").prepend(checkbox);
				c = {"command":cmd.command,"tags":classes,"content":c};
				console.log(c);
				appdata.listitems.push(c);
			});

			console.log(appdata);
			player.setAppdata(appdata);
		}

		if (cmd.command == "remove")
		{
			if (Array.isArray(cmd.tags)) {cmd.tags = cmd.tags[0]};
			toremove = cmd.tags;
			console.log(toremove);
			$( "."+toremove ).remove(); // removes all of this class from dom

			// get all items that do NOT match the remove class
			let filteredArray = appdata.listitems.filter(function(obj){
				//console.log(obj);
				console.log(obj.tags.indexOf("beispielklasse") > -1);
				return (obj.tags.indexOf(cmd.tags) == -1)
			});
			console.log(filteredArray);
			appdata.listitems = filteredArray
			console.log(appdata.listitems);

			player.setAppdata(appdata);
		}

		if (cmd.command == "set")
		{
			// show Questbook for 2 seconds then hide again.
			main.send({"WINDOW":["show()"],"statusbalken":{"highlight":true}});//
			setTimeout(function(){main.send({"WINDOW":["hide()"]});},2000);

			itemchecked.play();
			if (Array.isArray(cmd.tags)) {cmd.tags = cmd.tags[0]};
			if (Array.isArray(cmd.content)) {cmd.content = cmd.content[0]};
			if (Array.isArray(cmd.status)) {cmd.content = cmd.status[0]};

			toedit = cmd.tags;
			tosetto = cmd.status;
			//console.log(toedit);console.log(tosetto);console.log(Array.from($("."+toedit)));

			Array.from($("."+toedit)).forEach(function(div)
			{

				//console.log(div.firstChild) // every checkbox
				let chckbx = div.firstChild;
				//chckbx.checked = true;

				console.log(chckbx.type == "checkbox");

				// if divs are of class input (therefore checkbox)...
	 			if (chckbx.type == "checkbox")
	 			{

	 				if (tosetto == "checked")
	 				{
	 					//console.log($("."+toedit).children()[0]);
	 					chckbx.checked = true;
	 					chckbx.indeterminate = false;
	 				}
	 				else if (tosetto == "x")
	 				{
	 					//console.log($("."+toedit).children()[0]);
	 					chckbx.indeterminate = true;
	 				}
	 				else
	 				{
	 					//console.log($("."+toedit).children()[0]);
	 					chckbx.checked = false;
	 					chckbx.indeterminate = false;
	 				}

	 			}
				else if (cmd.content != undefined) // if not checkbox (and therefore html inside:)
				{
					div.innerHTML = cmd.content;
				}

			});


		// overwrite content in every item that matches class:
		appdata.listitems.forEach(function(obj)
		{
			if (obj.tags.indexOf(cmd.tags) > -1)
			{
				//overwrite:
				obj.status = cmd.status;
			}

		});

		console.log(appdata);
		player.setAppdata(appdata);


		}


}
