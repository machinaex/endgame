*QUESTBOOK*
=============

Basis Code für alle machina ENDGAME Electron Apps.

[MISSIONBOX API](#markdown-header-aufsetzen)

--------------------------------------------------------------

MISSIONBOX API
==========

versteht input jsons im folgenden Format:

`{
	"command" : < "checkbox" / "html" / "remove" / "set" >,
	"tags" : [< appnames / classnames >],
	"content" : [< mission texts (OPTIONAL!)>],
	"status" : <"checked" / "unchecked" / "x" (OPTIONAL! ONLY FOR CHECKBOXES!)>
}`

where commands can be
- 'remove' in order to remove all items with the provided classname(s)
- 'checkbox' to add checkbox items on the top of the list (number of text entries decided how many)
- 'set' sets all html with the tags provided to the text provided OR all checkboxes with the tags provided to either "checked", "x" (indeterminate) or "unchecked"

ATTENTION: 
- 'content' and 'tags' can be arrays for commands 'checkbox' and 'html' (creation of several list items at once) NOT for 'remove' or 'set' (modifying of list items by class)!!!
- 'content can be provided' for 'remove' but will be ignored because.... it makes no sense. removals are done by tags not by content comparison. duh!

examples:
`{
	"command" : "checkbox",
	"tags" : ["level1","freeprince"],
	"content" : ["Free the prince from the tower!"]
}`

will create a NEW checkbox with the classes level1 and freeprince to the list of missions with a label saying "Free the prince from the tower!"

`{
		"command" : "set"
		"tags" : "freeprince"
		"status" : "checked"
}`

will set the existing checkbox with the (hopefully unique) tag "freeprince" to checked.
