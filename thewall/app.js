$ = require("jquery");
require("howler");
const fs = require('fs');

const presentationfolder = './presentation/';
let presentation = [];


let recentSlide = 0;

fs.readdir(presentationfolder, (err, files) => {
  files.forEach(file => {
    //console.log(file);
    if (file.indexOf(".png") > -1)
    {
        presentation.push(presentationfolder + file);
    }

  });
  console.log(presentation);
  $("#presentation").attr("src",presentation[0]);
});


var ping = new Howl({
  src: ['./ping.mp3']
});


var counter = 0;
var coin = new Howl({   src: ['./coin.mp3'],   rate:1,
volume: 0.4,   loop: true,   onend: function() {     counter++;
//console.log("Finished iteration " + counter);
if (counter === 13 - 1) {
console.log("Last time!"); counter = 0;this.stop();     }
} });


var win = new Howl({
  src: ['./win.mp3']
});

var ProgressBar = require('progressbar.js')

var colors = ["rgba(0,185,185,1)","rgba(255,0,255,1)","rgba(50,155,55,1)"];

let posTotal = $("#gesamtscroll").offset().top;
let posTasks = $("#taskscroll").offset().top;
let posHighscore = $("#bestscroll").offset().top;
let posPresentation = $("#presentationscroll").offset().top;

// global vars for playerdata:

let teamlisa = [];
let teamtom = [];
let teampaulie = [];
let lisascore = 0;let tomscore = 0;let pauliescore = 0;
let lisataskscore = 0;let tomtaskscore = 0;let paulietaskscore = 0;
let lisataskhighscore = [0,""];let tomtaskhighscore = [0,""];let paulietaskhighscore = [0,""];


main.init = function(myappdata, language)
{

}

main.receive = function(mydata)
{
	if (mydata.prepare != undefined)
    {
      console.log(mydata);
        initialize(mydata.prepare, console.log);

    }
    else if (mydata.scroll != undefined)
    {
        if (mydata.scroll == "task")
        {
            $('body,html').animate({scrollTop: posTasks}, 1800);
        }
        else if (mydata.scroll == "highscore")
        {
            $('body,html').animate({scrollTop: posHighscore}, 1800);
            setTimeout(function(){win.play();}, 1800);
        }
        else if (mydata.scroll == "total")
        {
            $('body,html').animate({scrollTop: posTotal}, 1800);
        }
        else if (mydata.scroll == "presentation")
        {
            $('body,html').animate({scrollTop: posPresentation}, 1800);
        }

    }
    else if (mydata.task != undefined)
    {
        // here the animations???
        // here the animations get triggered:
        let totalscore = (lisataskscore+tomtaskscore+paulietaskscore);
        let num = 0;
        let max = Math.max(lisataskscore, tomtaskscore, paulietaskscore);
        if (max == lisataskscore){num = 1}else if(max == tomtaskscore){num=2}else{num=3}

        coin.play();
        bar[0].animate(lisataskscore/totalscore, function(){winnerbar(colors[num-1],"#balken"+num)});  // Number from 0.0 to 1.0
        bar[1].animate(tomtaskscore/totalscore);  // Number from 0.0 to 1.0
        bar[2].animate(paulietaskscore/totalscore);

    }

    else if (mydata.total != undefined)
    {
        if (mydata.total == true)
        {
            circleAuswertung();
        }
        if (mydata.total.time != undefined )
        {
            setTimeout(circleAuswertung, mydata.total.time);
        }

    }
    else if (mydata.slide != undefined)
    {
        if (mydata.slide == "next")
        {
            nextSlide();
        }
        else if (mydata.slide == "prev")
        {
            prevSlide();
        }
        else
        {
            gotoSlide(mydata.slide);
        }
    }
}


////////////////////////////////
//// testing receives: /////////
/*
setTimeout(function(){
    main.receive({prepare:"level0"});
    main.receive({scroll:"highscore"});
},1000);

setTimeout(function(){
    main.receive({scroll:"total"});
    main.receive({total:{time:1800}})
},5000);

setTimeout(function(){
    main.receive({scroll:"task"});
    main.receive({task:{time:1800}})
},15000);

setTimeout(function(){
    main.receive({scroll:"presentation"});
    main.receive({task:{time:1800}})
},20000);

setTimeout(function(){
    main.receive({slide:5});
    main.receive({task:{time:1800}})
},22000);
*/
////////////////////////////////

function getAllPlayers(callback)
{
        player.connect( function(connection)
       {
         r.db("status").table("player").orderBy({index: 'ip'}).run(connection, function(err, cursor){
             if (err) throw err;
            cursor.toArray(function(err, result) {
              if (err) throw err;
              //console.log(result);
              callback(result);
            });
         }
         );
       }
    );
}


$("body").keydown(function(e) {
    //console.log(e.key);
    if(e.key == "ArrowRight"){nextSlide()}
    if(e.key == "ArrowLeft"){prevSlide()}
    if(e.key == "0"){main.receive({prepare:"level0"});}
    if(e.key == "1"){main.receive({prepare:"level1"});}
    if(e.key == "2"){main.receive({prepare:"profil_app"});}
    if(e.key == "3"){main.receive({prepare:"inventory"});}
    if(e.key == "*"){main.receive({scroll:"presentation"});}
    if(e.key == "-"){main.receive({scroll:"task"});}
    if(e.key == "+"){main.receive({scroll:"highscore"});}
    if(e.key == "Enter"){main.receive({scroll:"total"});}
    if(e.key == "q"){main.receive({task:{time:0}});}
    if(e.key == "w"){main.receive({total:{time:0}});}
});

function nextSlide()
{
    $("#presentation").attr("src",presentation[recentSlide+1]);
    if(recentSlide < presentation.length-2)
    {
         recentSlide += 1;
    }
    else
    {
        recentSlide = presentation.length-1;
    }
    console.log(recentSlide);
    console.log(presentation.length-1);


}

function prevSlide()
{
    $("#presentation").attr("src",presentation[recentSlide-1]);
    recentSlide -= 1;
    if(recentSlide > 0)
    {
         recentSlide -= 1;
    }
    else
    {
        recentSlide = 0;
    }
    console.log(recentSlide);
    console.log(presentation.length-1);
}

function gotoSlide(num)
{
    if( num < presentation.length && num > -1 )
    {
        $("#presentation").attr("src",presentation[num]);
        recentSlide = num;
    }

}

function initialize(task, callback)
{
      $("*").removeClass("A_bounce");
      $("*").removeClass("winnerbalken");

    //////////////////////////////////////////////
    // get all Playerdata & filter for team  and then filter for highest "score" value by team:
    teamlisa = [];
    teamtom = [];
    teampaulie = [];
    lisascore = 0;tomscore = 0;pauliescore = 0;
    lisataskscore = 0;tomtaskscore = 0;paulietaskscore = 0;
    lisataskhighscore = [0,""];tomtaskhighscore = [0,""];paulietaskhighscore = [0,""];

    getAllPlayers(function(res)
    {

            //console.log(res);
            // filter all players by group:
            res.forEach(function(item, i){
                if (item.team.toLowerCase() == "paulie")
                { teampaulie.push(item); }
                else if (item.team.toLowerCase() == "tom")
                { teamtom.push(item); }
                else if (item.team.toLowerCase() == "matt")
                { teamlisa.push(item); }
            });
            //console.log(teamlisa);console.log(teamtom);console.log(teampaulie);
            // get scores per team for total and task specific:
            teamlisa.forEach(function(item, i){
                lisascore += item.score;
                //console.log("lisascore "+lisascore);
                try{

                    if (item[task].score != undefined)
                    {

                        if (isNumeric(item[task].score))
                        {
                            lisataskscore += item[task].score;
                            //console.log("lisaTASKscore "+lisataskscore);
                            if (lisataskhighscore[0] <= item[task].score)
                            {
                                lisataskhighscore[0] = item[task].score;

                                lisataskhighscore[1] = item.name.replace(" ","_");

                            }
                        }
                    }

                }catch(e){console.log(e)}

            })
            teamtom.forEach(function(item, i){
                tomscore += item.score;
                //console.log("tomscore "+tomscore);
                try{

                  if (item[task].score != undefined)
                  {

                      if (isNumeric(item[task].score))
                      {
                          tomtaskscore += item[task].score;
                          if (tomtaskhighscore[0] <= item[task].score)
                          {
                              tomtaskhighscore[0] = item[task].score;
                              tomtaskhighscore[1] = item.name.replace(" ","_");
                          }
                      }

                  }
                }catch(e){console.log(e)}
            })
            teampaulie.forEach(function(item, i){
                pauliescore += item.score;
                try{

                    if (item[task].score != undefined)
                    {
                        if (isNumeric(item[task].score))
                        {
                            paulietaskscore += item[task].score;
                            //console.log("pauliescore "+pauliescore);
                            if (paulietaskhighscore[0] <= item[task].score)
                            {
                                paulietaskhighscore[0] = item[task].score;
                                paulietaskhighscore[1] = item.name.replace(" ","_");
                            }
                        }
                    }

                }catch(e){console.log(e)}
            })
            //console.log(lisascore + " " + tomscore + " " + pauliescore);
            console.log(lisataskscore + " " + tomtaskscore + " " + paulietaskscore);
            //console.log(lisataskhighscore + ";" + tomtaskhighscore + ";" + paulietaskhighscore);

            // INIT ANIMATION OBJECTS:
            bar = [];
            initBar(0,"#balken1","rgba(0,185,185,1)","MATT");
            initBar(1,"#balken2","rgba(255,0,255,1)","TOM");
            initBar(2,"#balken3","rgba(50,155,55,1)","PAULIE");

            initStreberinnen([lisataskhighscore,tomtaskhighscore,paulietaskhighscore]);

            circle = [];
            initCircle(0,"#kreis1","rgba(0,185,185,1)","team MATT");
            initCircle(1,"#kreis2","rgba(255,0,255,1)","team TOM");
            initCircle(2,"#kreis3","rgba(50,155,55,1)","team PAULIE");

            callback();
    });

    /////////////////////////////////////////////

}


function isNumeric(n)
{
  return !isNaN(parseFloat(n)) && isFinite(n);
}

var bar = []; // Array of objects for bar animations
function initBar(num, id, endcolor, teamname)
{
    $(id).html("");
    // define animations for stats:
    bar[num] = new ProgressBar.Line(id, {
        strokeWidth: 4,
        easing: 'easeInOut',
        duration: 5000,
        color: '#FFEA82',
        trailColor: 'rgba(30,0,0,0.2)',
        trailWidth: 4,
        svgStyle: {width: '100%', height: '100%'},
        from: {color: endcolor},
        to: {color: endcolor},
        text: {
        style: {
          color: 'white',
          position: 'relative',
          left: "40%",
          top: '-6vh',
          padding: 0,
          margin: 0,
          transform: null,
        },
        autoStyleContainer: false,
        },
        step: (state, bar) => {
        bar.path.setAttribute('stroke', state.color);
        var totalscore = (lisataskscore+tomtaskscore+paulietaskscore);
        //console.log(paulietaskscore);
        //console.log(bar.value());
        //console.log(totalscore);
        var value = Math.max(Math.round((bar.value() - 0.01) * totalscore),1);
        bar.setText("TEAM  "+teamname +" "+ value + ' ***');

        }
    });
    bar[num].text.style.fontSize = "5vh";

}



var circle = []; // Array of objects for bar animations
function initCircle(num, id, endcolor, teamname)
{
    $(id).html("");
	// define animations for stats:
	  circle[num] = new ProgressBar.Circle(id, {
	  color: 'white',
	  // This has to be the same size as the maximum width to
	  // prevent clipping
	  strokeWidth: 3,
	  trailWidth: 1,
	  easing: 'easeInOut',
	  duration: 6000,
	  text: {
	  	style: {
	      color: 'white',
	      position: 'relative',
	      //left: "33%",
	      top: "-16vh",
	      padding: 10,
	      margin: 0,
	      transform: null,
	    },
	    autoStyleContainer: false
	  },
	  from: { color: endcolor, width: 1 },
	  to: { color: endcolor, width: 3 },
	  // Set default step function for all animate calls
	  step: function(state, circle) {
	    circle.path.setAttribute('stroke', state.color);
	    circle.path.setAttribute('stroke-width', state.width);

        let totalscore = (lisascore+tomscore+pauliescore);
	    var value = Math.max(Math.round((circle.value() - 0.01) * totalscore),1);
	    if (value === 0) {
	      circle.setText('');
	    } else {
	      circle.setText(teamname+" "+value);
	    }

		  }
		});
		circle[num].text.style.fontFamily = 'SignPainter';
		circle[num].text.style.fontSize = "5vh";
		circle[num].text.style.textAlign = "center";

}

function initStreberinnen(winnerArray)
{
    console.log(winnerArray);
    let theBEST = [0,"",0];

    winnerArray.forEach(function(item, i){
        console.log(item);
        console.log(i);
        let basefile = "<img src=\"/Volumes/Async/Temp/"+item[1]+".png\" class=\"profilepicIMG\"/>";
        $("#best"+(i+1)).html(basefile);
        $("#best"+(i+1)+"score").html(item[0]+"<br><span id=\"winnername\">"+item[1].replace("_"," ")+"</span>");
        if (item[0] >= theBEST[0]) {theBEST = [item[0],item[1],i+1]};
    });

      $("#best"+theBEST[2]).addClass("A_bounce");
}


/*
// INIT DATA by pulling it from DB:
// then by callback: start animations!
initialize("level0", function(){

    // here the animations get triggered:
    let num = 0;
    let max = Math.max(lisataskscore, tomtaskscore, paulietaskscore);
    if (max == lisataskscore){num = 1}else if(max == tomtaskscore){num=2}else{num=3}

    coin.play();
    bar[0].animate(lisataskscore/100, function(){winnerbar(colors[num-1],"#balken"+num)});  // Number from 0.0 to 1.0
    bar[1].animate(tomtaskscore/100);  // Number from 0.0 to 1.0
    bar[2].animate(paulietaskscore/100);

});
//*/
function winnerbar(winnercolor, winnerid){
	$(winnerid).addClass('winnerbalken');
	$("#canvas").prependTo(winnerid);
	particleColors.colorOptions =[winnercolor];
	RestartConfetti();
	win.play();
	$(winnerid).parent().addClass('A_bounce');
}

function winnerCircle(winnercolor, winnerid){
	//$(winnerid).addClass('winnerbalken');
	$("#canvas").prependTo($("#gesamtscroll"));
	particleColors.colorOptions =[winnercolor];
	RestartConfetti();
	win.play();
	$(winnerid).parent().addClass('A_bounce');
}

function winnerHighscore(winnercolor, winnerid){
    //$(winnerid).addClass('winnerbalken');
    $("#canvas").prependTo($("#best"));
    particleColors.colorOptions =[winnercolor];
    RestartConfetti();
    win.play();
    //$(winnerid).parent().addClass('A_bounce');
}

function circleAuswertung()
{
    let num = 0;
    let max = Math.max(lisascore, tomscore, pauliescore);
    console.log(lisascore +" "+ tomscore +" "+pauliescore);
    if (max == lisascore){num = 1}else if(max == tomscore){num=2}else{num=3}

	setTimeout(function(){
        let totalscore = (lisascore+tomscore+pauliescore);
		circle[0].animate(lisascore/totalscore+0.01);  // Number from 0.0 to 1.0
		circle[1].animate(tomscore/totalscore+0.01, function(){winnerCircle(colors[num-1],"#kreis"+num)});  // Number from 0.0 to 1.0
		circle[2].animate(pauliescore/totalscore+0.01);

		setTimeout(function(){
			coin._loop = true;
			coin.play();
		},1000)
	},300);
}










//////////////////////////////////////
/// CONFETTI >>>> ////////////////////

    // globals
    var canvas;
    var ctx;
    var W;
    var H;
    var mp = 150; //max particles
    var particles = [];
    var angle = 0;
    var tiltAngle = 0;
    var confettiActive = true;
    var animationComplete = true;
    var deactivationTimerHandler;
    var reactivationTimerHandler;
    var animationHandler;

    // objects

    var particleColors = {
        colorOptions: ["magenta"],
        colorIndex: 0,
        colorIncrementer: 0,
        colorThreshold: 10,
        getColor: function () {
            if (this.colorIncrementer >= 10) {
                this.colorIncrementer = 0;
                this.colorIndex++;
                if (this.colorIndex >= this.colorOptions.length) {
                    this.colorIndex = 0;
                }
            }
            this.colorIncrementer++;
            return this.colorOptions[this.colorIndex];
        }
    }

    function confettiParticle(color) {
        this.x = Math.random() * W; // x-coordinate
        this.y = (Math.random() * H) - H; //y-coordinate
        this.r = RandomFromTo(10, 30); //radius;
        this.d = (Math.random() * mp) + 10; //density;
        this.color = color;
        this.tilt = Math.floor(Math.random() * 10) - 10;
        this.tiltAngleIncremental = (Math.random() * 0.07) + .05;
        this.tiltAngle = 0;

        this.draw = function () {
            ctx.beginPath();
            ctx.lineWidth = this.r / 2;
            ctx.strokeStyle = this.color;
            ctx.moveTo(this.x + this.tilt + (this.r / 4), this.y);
            ctx.lineTo(this.x + this.tilt, this.y + this.tilt + (this.r / 4));
            return ctx.stroke();
        }
    }

    $(document).ready(function () {
        SetGlobals();
        InitializeButton();
        InitializeConfetti();

        $(window).resize(function () {
            W = window.innerWidth;
            H = window.innerHeight;
            canvas.width = W;
            canvas.height = H;
        });

    });

    function InitializeButton() {
        $('#stopButton').click(DeactivateConfetti);
        $('#startButton').click(RestartConfetti);
    }

    function SetGlobals() {
        canvas = document.getElementById("canvas");
        ctx = canvas.getContext("2d");
        W = window.innerWidth;
        H = window.innerHeight;
        canvas.width = W;
        canvas.height = H;
    }

    function InitializeConfetti() {
        particles = [];
        animationComplete = false;
        for (var i = 0; i < mp; i++) {
            var particleColor = particleColors.getColor();
            particles.push(new confettiParticle(particleColor));
        }
        StartConfetti();
    }

    function Draw() {
        ctx.clearRect(0, 0, W, H);
        var results = [];
        for (var i = 0; i < mp; i++) {
            (function (j) {
                results.push(particles[j].draw());
            })(i);
        }
        Update();

        return results;
    }

    function RandomFromTo(from, to) {
        return Math.floor(Math.random() * (to - from + 1) + from);
    }


    function Update() {
        var remainingFlakes = 0;
        var particle;
        angle += 0.01;
        tiltAngle += 0.1;

        for (var i = 0; i < mp; i++) {
            particle = particles[i];
            if (animationComplete) return;

            if (!confettiActive && particle.y < -15) {
                particle.y = H + 100;
                continue;
            }

            stepParticle(particle, i);

            if (particle.y <= H) {
                remainingFlakes++;
            }
            CheckForReposition(particle, i);
        }

        if (remainingFlakes === 0) {
            StopConfetti();
        }
    }

    function CheckForReposition(particle, index) {
        if ((particle.x > W + 20 || particle.x < -20 || particle.y > H) && confettiActive) {
            if (index % 5 > 0 || index % 2 == 0) //66.67% of the flakes
            {
                repositionParticle(particle, Math.random() * W, -10, Math.floor(Math.random() * 10) - 20);
            } else {
                if (Math.sin(angle) > 0) {
                    //Enter from the left
                    repositionParticle(particle, -20, Math.random() * H, Math.floor(Math.random() * 10) - 20);
                } else {
                    //Enter from the right
                    repositionParticle(particle, W + 20, Math.random() * H, Math.floor(Math.random() * 10) - 20);
                }
            }
        }
    }
    function stepParticle(particle, particleIndex) {
        particle.tiltAngle += particle.tiltAngleIncremental;
        particle.y += (Math.cos(angle + particle.d) + 3 + particle.r / 2) / 2;
        particle.x += Math.sin(angle);
        particle.tilt = (Math.sin(particle.tiltAngle - (particleIndex / 3))) * 15;
    }

    function repositionParticle(particle, xCoordinate, yCoordinate, tilt) {
        particle.x = xCoordinate;
        particle.y = yCoordinate;
        particle.tilt = tilt;
    }

    function StartConfetti() {
        W = window.innerWidth;
        H = window.innerHeight;
        canvas.width = W;
        canvas.height = H;
        (function animloop() {
            if (animationComplete) return null;
            animationHandler = requestAnimFrame(animloop);
            return Draw();
        })();
    }

    function ClearTimers() {
        clearTimeout(reactivationTimerHandler);
        clearTimeout(animationHandler);
    }

    function DeactivateConfetti() {
        confettiActive = false;
        ClearTimers();
    }

    function StopConfetti() {
        animationComplete = true;
        if (ctx == undefined) return;
        ctx.clearRect(0, 0, W, H);
    }

    function RestartConfetti() {
        ClearTimers();
        StopConfetti();
        reactivationTimerHandler = setTimeout(function () {
            confettiActive = true;
            animationComplete = false;
            InitializeConfetti();
        }, 100);

    }

    window.requestAnimFrame = (function () {
        return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function (callback) {
            return window.setTimeout(callback, 1000 / 60);
        };
    })();


DeactivateConfetti();



//////////////////////////////////////
/// <<<< CONFETTI ////////////////////
