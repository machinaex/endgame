#!/usr/bin/python
# -*- coding: utf-8 -*-

# File: cue.py
# cue Object

import rethinkdb as r

# Class: Cue
# fetches a cue from the cuetable in storage DB and makes the Parameters accessible.
class Cue(object):
	# Function: __init__
	# - pluggs Actions and Listeners from cue and makes them accessible
	# - makes Changes in Player Status Table
	#
	# Variables:
	# - name: cue name
	# - actions: list of actions to forward to app_handler
	# - changes: list of changes that are done in Status Player Table
	# - listeners: list of listeners to install as change feeds in player status table
	# - close_calls: list of change feed listeners that have to be closed
	def __init__(self, pointer, db_ip, db_port):
		self.name = pointer["Name"]

		if "Source" in pointer:
			self.source = pointer["Source"]
		else:
			self.source = None
		
		self.actions = []
		self.changes = []
		self.listeners = []
		self.control_messages = []
		self.close_calls = []
		self.db_ip = db_ip
		self.db_port = db_port
		self.properties = None

		try:
			conn = r.connect(self.db_ip, self.db_port)
		except r.ReqlTimeoutError as e:
			print("rethinkdb ERROR in Cue %s. %s" % (self.name, e))


		# get cue entry by name
		db_cue = r.db('storage').table('cues').get(self.name).run(conn)

		conn.close()

		if db_cue is not None:
			print("++++++ %s ++++++" % self.name)
			self.properties = db_cue

			# Fetch Action Entries from Cue Object
			# try getting list of targetet Players (their IP) from Target Entry
			if 'Action' in db_cue:

				if type( db_cue['Action'] ) is list:
					self.actions = db_cue['Action']

					for i, action in enumerate(self.actions):
						action['targets'] = self.get_targets(action.pop('Target'), 'ip')
						if len(action['targets']) == 0:
							print('could not assign any Target to Action No.%d in Cue %s' % (i, self.name))
						else:
							print "Action No.%d in Cue %s:\n%s." %(i, self.name, db_cue['Action'])

				elif type( db_cue['Action'] ) is dict:
					self.actions.append( db_cue['Action'] )

					self.actions[0]['targets'] = self.get_targets(self.actions[0].pop('Target'), 'ip')

					if len(self.actions[0]['targets']) == 0:
						print('could not assign any Target to Action in Cue %s' % self.name)
					else:
						print "Action No.%d in Cue %s:\n%s." %(0, self.name, db_cue['Action'])

				else:
					print("DB Format ERROR: 'Action' Property in Cue '%s' is neither an object nor a list." % self.name)

				print("\n")

			# Fetch Changes to be made in Status Database
			# Then Execute changes
			# Note that change Target behaves slightly different from action Target
			if 'Change' in db_cue:
				
				if type( db_cue['Change'] ) is list:
					self.changes = db_cue['Change']
					for i, change in enumerate(self.changes):
						change = self.status_change(change, i)
				elif type( db_cue['Change'] ) is dict:
					self.changes.append( db_cue['Change'] ) 
					self.changes[0] = self.status_change(self.changes[0], 0)
				else:
					print("DB Format ERROR: 'Change' Object in Cue '%s' is neither an object nor a list." % self.name)
				print("\n")

			# Fetch Entries in Listener Parameter
			# Store it in a globally accessible list
			if 'Listen' in db_cue:
				if type( db_cue['Listen'] ) is list:
					self.listeners = db_cue['Listen']
				elif type( db_cue['Listen'] ) is dict:
					self.listeners.append( db_cue['Listen'] )
				else:
					print("DB Format ERROR: 'Listen' Object in Cue '%s' is neither an object nor a list." % self.name)
				#print("\n")

			# Get Entries in Control parameter
			# store it so it can be triggered by main
			if 'Control' in db_cue:
				if type( db_cue['Control'] ) is list:
					self.control_messages = db_cue['Control']

					for i, control_msg in enumerate(self.control_messages):
						control_msg['targets'] = self.get_targets(control_msg.pop('Target'), 'ip')
						if len(control_msg['targets']) == 0:
							print('could not assign any Target to Control No.%d in Cue %s' % (i, self.name))
						else:
							print "Control No.%d in Cue %s:\n%s." %(i, self.name, db_cue['Control'])

				elif type( db_cue['Control'] ) is dict:
					self.control_messages.append( db_cue['Control'] )

					self.control_messages[0]['targets'] = self.get_targets(self.control_messages[0].pop('Target'), 'ip')

					if len(self.control_messages[0]['targets']) == 0:
						print('could not assign any Target to Control in Cue %s' % self.name)
					else:
						print "Control No.%d in Cue %s:\n%s." %(0, self.name, db_cue['Control'])

				else:
					print("DB Format ERROR: 'Control' Property in Cue '%s' is neither an object nor a list." % self.name)

				print("\n")

			# Fetch Names of Listeners that shall be closed
			if 'Close' in db_cue:
				self.close_calls = db_cue['Close']
				#print ("close calls: %s\n" % self.close_calls)

		else:
			print ("cue %s is not in Database" % self.name)

		conn.close()


	# Function: get_targets
	# Passes Target Query to exec() function and evaluates output
	def get_targets(self, t_query, param):

		conn = r.connect(self.db_ip, self.db_port)

		targets = []

		if  t_query[:4] == 'r.db':
			query_str = 'target_obj = %s' % t_query
		else:
			query_str = 'target_obj = r.db("status").table("player").filter(%s)' % t_query

		print ("query: %s" % query_str)

		try:
			query = compile(query_str, 'query', 'exec')
		except SyntaxError as e:
			print("Could not compile Target '%s'. '%s'" % (t_query, e))

		try:
			exec(query)

			for row in target_obj.run(conn):
				targets.append(row[param])

		except r.ReqlServerCompileError as e:
			print "Could not resolve Target '%s'. %s" % (t_query, e)
		except ReqlDriverCompileError as e:
			print "Could not resolve Target '%s'. %s" % (t_query, e)
		except NameError as e:
			print "Could not resolve Target '%s'. %s" % (t_query, e)

		conn.close()
		return targets

	# Function: status_change
	# make immediate changes in Player Status Table
	def status_change(self, change, index):

		conn = r.connect(self.db_ip, self.db_port)
		
		if 'Update' in change:
			if 'Target' in change:
				query_str = 'result = r.db("status").table("player").filter(%s).update(%s).run(conn)' % (change['Target'], change['Update'])
				print "Cue %s Change No.%d Query: %s" % (self.name, index, query_str)

				try:
					query = compile(query_str, 'query', 'exec')
					exec(query)
				except AttributeError as e:
					print(e)
				except SyntaxError as e:
					print(e)
				except TypeError as e:
					print(e)

			else:
				print("found 'update' in 'Change' without Target. Cue %s Change No.%d" % (self.name, index))

		elif 'Query' in change:
			query_str = 'result = %s.run(conn)' % change['Query']
			print "Cue %s Change No.%s Query: %s" %(self.name, index, str(query_str))
			query = compile(query_str, 'query', 'exec')

			try:
				exec(query)
			except r.ReqlOpFailedError as e:
				print("ERROR in %s Change Query: %s" % (self.name, e))

			#print "Result: %s" % result

		else:
			print("Change Property No.%d in Cue %s has no valid Content" % (index, self.name))

		conn.close()
		return change

		