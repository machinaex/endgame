Framework
=========

Python Skript, dass den Zugriff auf die zentralen Datenbanken verwaltet.
*Framework* werted cue Objekte aus und führt die darin enthaltenen Anweisungen aus.





Funktionen
----------

1. **Cues aus Datenbank holen und auswerten.** Anschließend werden die entsprechenden Teile des Cues an etwaige Clients weitergeleitet (z.B. app-handler, MAX oder direkter Zugriff auf Datenbank)
2. **Change Feeds (Listener) in Status Datenbank anlegen und ggf. cue auslösen.** listener werden, wenn sie eintreffen, wieder entfernt. Können auch manuell gestoppt werden.
3. **Zu Spielbeginn Anfangswerte aus Storage Spieler Tabelle in Status Spieler Table übertragen** Dabei wird die Status Spieler Tabelle überschrieben nachdem eine Kopie Archiviert wurde.
4. **Ereignishistorie anlegen.** Im besonderen die Listener sollten geloggt werden und z.B. per Hand unterbrochen werden können.


Setup
------------

####Dependencies

* **python 2.7** mit rethinkdb library
* rethinkdb Datenbank

####Getting startet

rethink DB mit default Einstellungen in Terminal Session starten: 
`$ rethinkdb`

framework_server in neuer Terminal Session starten: 
`$ cd ~/endgame/framework_server` 
`$ python framework.py`

Datenbank Basisstruktur erstellen: 
`create`

Datenbank Zugrifftool starten (z.B. [Chateau](https://github.com/neumino/chateau) oder [ReQLPro](http://reqlpro.com/) und mit laufendem rethink server verbinden.

Player und Cues Table in Storage Datenbank mit Objekten aus player_example.json und cue_example.json füllen.

Spiel initialisieren (framework.py Terminal Session): 
`init`

Cues auslösen mit: 
`cue <cuename>`



Eingaben
------------
wenn das Programm gestartet ist können jederzeit Eingaben gemacht werden

* `create` erstellt für endgame benötigten Datenbankaufbau (ohne Einträge hinzuzufügen)  
* `init` initialisiert Spiel. Archiviert Einträge in Status DB und ersetzt sie mit Starteigenschaften  
* `cue <CUENAME>` löst cue mit namen CUENAME aus  
* `listener` listet alle aktiven Change Feed Listener auf
* `close <listener_name>` schließt etwaigen Listener der mit CUENAME installiert wurde. 'all' schliesst alle aktiven listener
* `close <list of listeners>` schliesst reihe von listenern
* `start <appname> <laptop id (list)>` öffnet app auf Laptops mit ip `192.168.0.<laptop id>`. 'all' öffnet app auf allen laptops (201-230)
* `control <command> <value> <laptop id (list)>` führt system command auf Laptops mit ip `192.168.0.<laptop id>` aus. 'all' öffnet app auf allen laptops (201-230).
* `export <dbname.tablename> <exportfolder>` exportiert Datanbank Tabellen (oder, wenn angegeben nur eine Tabelle) in `<exportfolder>`. Export Ordner darf noch nicht existieren.
* `import cues <fromfolder>` cues.json aus `<fromfolder>` in aktuelle storage.cues Tabelle importieren. Erstellt automatisch Backup in Archive DB.


Datenbank
-----------
Endgame findet sich in zwei Datenbanken wieder:

**`Status`** beinhaltet alle Eigenschaften, die sich im Laufe des Spiels verändern. Wesentlicher Datensatz ist die `Player` Tabelle.

**`Storage`** beinhaltet alle Eigenschaften, die im Entwicklungsprozess festgelegt werden und während des Spiels nur abgerufen und nicht verändert werden. Tabellen sind:
 - `Cues`
 - `Player`
 - `Content` **(noch nicht implementiert)**

###Player

Liste von Spielern, die jeweils einen Spieler Laptop repräsentieren.
In der `Storage` Datenbank ist eine Grundform der Spielerliste vorhanden in der zunächst nur `level` und `score` (mit default Wert) und `ip` eingetragen sind.
Die Player Tabelle in der `Status` Datenbank ist bei Spiel Initialisierung eine Kopie der Storage.player Datenbank. Weitere Parameter und Eigenschaften werden im Spielverlauf hinzugefügt.

>Eine liste von funktionierenden Beispiel Player Objekten findet sich in [player_example.json](https://bitbucket.org/machinaex/endgame/src/347b48ee95680069b6b8984169610085ad8dd2ca/framework_server/player_example.json?at=master&fileviewer=file-view-default)

####Parameter:

- **`ip`** (primary key)
IP des Laptops der dem Player zugeordnet ist. Im Format: `"192.168.1.xxx"`
- **`name`**
Name des Spielers. Kann Dynamisch erstellt werden oder in Storage schon festgelegt sein. Mit dem Namen könnten bestimmte (Charakter) Eigenschaften (Vorzüge, Nachteile, Minigames) verbunden sein.
- **`language`**
Bestimmt auf welche Inhalte die Apps zugreifen (Text, Audio, Video. Default: `'de'` alternative: `'en'`
- **`score`**
Punktestand des Spielers. Default = 0.
- **`level`**
Meilensteine im Vorankommen des Spielers.
- **`team`**
Einer von drei Gruppennamen der der Spieler zugeordnet wird. (z.B. `"blue"`)
- **`attributes`**
Liste von EIgenschaften, die dem Spieler im Laufe des Spiels gegeben werden. Gut nutzbar um bestimmte relevante Ereignisse die der Spieler in Apps erspielt zu dokumentieren und ggf. in anderen Spielzusammenhängen darauf zu reagieren.
- **`<app_status>`**
Name der jeweiligen App. Enthält spezifische Informationen zum Spielstatus des Spielers in dieser App.


###Cues
Liste von Anweisungsobjekten, die per `Name` aufgerufen werden können.
>Eine Liste von funktionierenden Beispielcues findet sich in [cue_example.json](https://bitbucket.org/machinaex/endgame/src/347b48ee95680069b6b8984169610085ad8dd2ca/framework_server/cue_example.json?at=master&fileviewer=file-view-default)

####Parameter:

- **`Name:<CUENAME>`** (primary key)
Der Name/ID des Cues. Entspricht dem Eventnamen in Max. 

- **`Action: [] oder {}`** 
Eine, oder mehrere Aktionen die an die app-handler, die in `Target` definiert sind weitergegeben werden. Einzelne Aktion kann als Objekt `{}` eingetragen werden. Mehrere als Liste `[]`  
**Eigenschaften:**  
`Target` (obligatorisch) definiert das Ziel von `Action`. Details siehe unten  
`all` Appdaten, die an alle Apps weitergeleitet werden sollen. Wenn die *all* Eigenschaft genutzt wird, wird sie ggf. mit der Object.assign methode mit den einzelnen appdaten ín ein js objekt gelegt. Wenn *all* in Kombination mit einzelnen appdaten genutzt wird sollten aswohl *all* als auch die app daten als js Ojekt definiert werden, da sonst nicht korrekt gemerged werden kann.  
`<appname>` standins für appspezifische daten/befehle für Apps. appname wäre hier der tatsächliche Name der App. Kann Sowohl Objekt `{}` als auch Liste `[]` enthalten. Hierzu die API der jeweiligen APP konsultieren!  
 
- **`Change: [] oder {}`**  Mehr oder weniger direkter Zugriff auf die Spieler Status Datenbank. Betroffene Spieler werden über `Target` adressiert. EInzelner Change kann als Objekt `{}` eingetragen werden. Mehrere als Liste `[]`  
**Eigenschaften:**  
`Query`Direkter Zugriff auf rethink DB.   
`Update` Parameter in Status DB der geändert werden soll. Erfordert `Target`.  
`Target` Definiert Betroffene Spieler für `Update`.  
  
- **`Listen: [] oder {}`** Installiert Change Feed in Spieler Status DB. Wenn der angegebene Fall eintritt wird `Followcue` dispatched. Einzelner Listener kann als Objekt `{}` angelegt werden. Mehrere als Liste `[]`  
**Eigenschaften:**  
`Name` Listener ID. Benötigt um listener wieder zu schliessen. Wenn nichts angegeben wird, wird dem Listener der ihn auslösende Cue zugeordnet. Ist 'Listen' eine Liste MUSS für alle 'Listen' Objekte 'Name' Eigenschaft vergeben werden.  
`Target` Definiert auf welche Player der Change Feed Listener abonniert wird  
`Condition` Bedingung, die bei change im feed abgefragt wird. Ist die Bedingung `True` wird `Followcue` ausgelöst. 
`Followcue` Name des Cues, der ausgelöst werden soll wenn `Condition` zutrifft.  
`Query` Eigenhändig formulierte change feed query. Muss `.changes()` enthalten um einen validen feed zu erstellen. (Wird zurzeit noch nicht überprüft) 
  
- **`Close: []`** Liste von Listener `Name` Eigenschaften. Schließt entsprechende Listener und beendet deren Change Feeds. 

- **`Execute`** **(noch nicht implementiert)** Liste an System Befehlen die ausgeführt werden sollen. Etwa um externes Skript oder Programm auszuführen. Beispiel: `"Execute":["scriptname.py -p param1 --pp param2", "dosomething.js"]` 

#### Besondere Eigenschaften

- `Target` Filtert die Status Player Tabelle. `Target` wird in ReQL [.filter()](https://www.rethinkdb.com/api/python/filter/) Funktion ausgeführtund bezieht sich stets auf die Player Tabelle in der Status Datenbank. Wenn `"Target":"''"` werden ALLE Player als Target definiert.
*Ausnahme:* Wird `Target` mit `"r.db"` begonnen, kann Eigenhändig eine Auswahl formuliert werden. Dann müssen sowohl datenbank als auch tabelle definiert werden. 
Achtung: Alles ist dann möglich. Auch Tabellen und Datenbanken zu löschen!

- `Query` Vollständige Query in [Python ReQL](https://www.rethinkdb.com/api/python/) Format. Sowohl rethink variable sowie Datenbank als auch Tabelle muessen angegeben werden: `"r.db('dbname').table('tablename')..."`. Die Query wird allerdings immer ausgeführt, das heißt `.run()` wird angehängt.
Achtung: Alles ist möglich. Auch Tabellen und Datenbanken zu löschen! 

- `Condition` ist eine Python formatierte Anweisung, die an eine Variable übergeben wird. In der Anweisung kann auf folgende Variablen zugegriffen werden: 
*change["new_val"]* python dict mit aktuellem Tabelleneintrag des Objekts, dass den Change Feed durch eine Änderung ausgelöst hat.
*change["old_val"]* python dict des Change Feed auslösendem Objekt mit Tabellenintrag vor der Veränderung. 

#### Rezepte

hier oder vielleicht bald an anderer Stelle (Wiki?) wird eine Liste erstellt werden, die verfahren Dokumentiert, wie welche Art der cue logik im oben beschreibenen System erreicht werden kann.

##### Change

###### Eine Eigenschaft überschreiben/ändern

`"Update":"{'level':'engineer'}"`

###### Einen Listeneintrag hinzufügen

Beispiel:  
`"Update":"{'attributes': r.row['attributes'].set_insert('charming')}"`


###### Einen Listeneintrag entfernen

Beispiel als vollständige Query:
`"Query":"r.db('status').table('player').filter(r.row['score'] < 10).update(lambda doc: {'attributes': doc['attributes'].difference(['clever'])})"`  
entfernt das Attribut 'clever' wenn 'score' kleiner ist als 10.
Als Target:  
`"Update":"lambda doc: {'attributes': doc['attributes'].difference(['clever'])}"`  


##### Target

###### Spieler mit Attribut x

Um alle Spieler auszuwählen, die ein bestimmtes Keyword in ihrer `attributes` Liste haben lässt sich folgende `Target` verwenden:
`"Target":"r.row['attributes'].contains('<keyword>')"`

###### Property enthält Zeichenfolge

Property muss  vom typ 'string' sein.
`lambda doc: doc['<property>'].match('<wortteil>')`

Beispiel:  
`lambda doc: doc['name'].match('Char')`  

###### Liste enthält x Einträge

Beispiel:Anzahl der Liste 'friends' in chat app ist größer als 0
`lambda player: player['chat']['friends'].count() > 0`

##### Listener

###### Eigenschaft trifft auf alle Spieler zu

Beispiel: Alle Spieler haben einen validen Eintrag in 'language':  
`list(r.db('status').table('player').pluck('language').run(self.conn)) == list(r.db('status').table('player').filter(r.row['language'] != "none").pluck('language').run(self.conn))`  

Beispiel: Alle Spieler des Teams 'blue' besitzen das Attribut 'clever'  
``` json
"Condition":"list(r.db('status').table('player').filter(lambda p: (p['team'] == 'blue') & (p['attributes'].contains('clever'))).run(self.conn)) == list(r.db('status').table('player').filter(lambda p: p['team'] == 'blue').run(self.conn))"
```

###### Auslösen wenn Spieler bestimmtes Attribut hat

Beispiel:  
`"Condition":"'streberin_1' in change['new_val']['attributes']"`



###### Cue auslöser Adressieren

um den Spieler mit dem Cue zu Ereichen, den er zuvor ausgelöst hat, kann folgender Aufbau verwendet werden: 
Die Veränderung an der Spieler DB, die nötig war um den Followcue des Listeners auszulösen, muss auch maßgeblich für das `Target` des Followcue sein.
Es stehen noch zwei möglichkeiten zur Debatte Spieler auszusortieren, die den entsprechenden Cue bereits erhalten haben um doppelungen zu vermeiden:

1. In einer `cues` Property wird gespeichert welcher Spieler welchen cue bisher erhalten hat. Darüber lässt sich im Target definieren, dass diese Spieler nicht betroffen sein sollen.

2. Der Listener wird nach jedem match neu erstellt, wobei alle Spieler, auf die die gesuchte Eigenschaft zum Erstellzeitpunkt bereits zutrifft in `Target` ausgeschlossen werden. 