# File: db_structure
# Python Module to configure Basic Database Setup

import rethinkdb as r
import os
from datetime import datetime

# Function: init
# - moves Init Player Table to Live Player Table
# - moves (if existing) recent Live Player Table to Archive. Uses <backup>
# - renames existing temp folder and creates new empty one
def init(rethink_db, tempfolder):

	feedback = {}

	os.system("mv /Volumes/Async/Temp %s%s" % (tempfolder, datetime.now().strftime("%Y_%m_%d-%H_%M_%S")))
	os.system("mkdir %s" % tempfolder)

	backup('status', 'player', 'ip', rethink_db)

	conn = r.connect(rethink_db['ip'], rethink_db['port'])

	try:
		conn.use('status')

		# copy and replace status.livehack table
		r.table_drop('livehack').run(conn)
		r.table_create('livehack', primary_key='team').run(conn)
		r.table('livehack').insert(r.db('storage').table('livehack')).run(conn)

		r.table_drop('player').run(conn)
		r.table_create('player', primary_key='ip').run(conn)

		# Create a secondary index on the name attribute
		r.table("player").index_create("name").run(conn)
		# Wait for the index to be ready to use
		r.table("player").index_wait("name").run(conn)

		r.table('player').insert(r.db('storage').table('player')).run(conn)

		feedback["success"] = "reset status.livehack table. reset status.player table. created backup."

	except r.ReqlRuntimeError as e:
		print e
		feedback["failed"] = e

	conn.close()

	return feedback

# Function: backup
# creates unique copy of table in archive
#
# Parameters: 
# table - table you want backed up
# db - Database the table is in that you want backed up
# pk - Primary Key of the original Table
def backup(db, table, pk, rethink_db):

	conn = r.connect(rethink_db['ip'], rethink_db['port'])
	try:
		# Variable: bckp
		# str that combines table name with timestamp to be used as name for archived table
		bckp = "%s_%s" % (table, datetime.now().strftime("%Y_%m_%d_%H_%M_%S") )

		r.db('archive').table_create(bckp, primary_key=pk).run(conn)

		r.db('archive').table(bckp).insert(r.db(db).table(table)).run(conn)

		print 'created backup in archive: %s' % bckp
	except r.ReqlRuntimeError as e:
		print e

	conn.close()

# Function: create
# Creates empty necessary Databases and tables for ENDGAME 
# if they don't exist on the running rethinkDB server.
def create(rethink_db):

	conn = r.connect(rethink_db['ip'], rethink_db['port'])
	try:
		r.db_create('storage').run(conn)
		print "create storage DB"
	except r.ReqlRuntimeError as e:
		print e

	try:
		r.db('storage').table_create('cues', primary_key='Name').run(conn)
		print "create storage.cues table"
	except r.ReqlRuntimeError as e:
		print e

	try:
		r.db('storage').table_create('player', primary_key='ip').run(conn)
		print "create storage.player table"
	except r.ReqlRuntimeError as e:
		print e

	try:
		r.db('storage').table_create('content', primary_key='id').run(conn)
		print "create storage.content table"
	except r.ReqlRuntimeError as e:
		print e

	try:
		r.db('storage').table_create('livehack', primary_key='team').run(conn)
		print "create storage.livehack table"
	except r.ReqlRuntimeError as e:
		print e

	try:
		r.db_create('archive').run(conn)
		print "create archive DB"
	except r.ReqlRuntimeError as e:
		print e

	try:
		r.db_create('status').run(conn)
		print "create status DB"
	except r.ReqlRuntimeError as e:
		print e

	try:
		r.db('status').table_create('player', primary_key='ip').run(conn)
		print "create status.player table"
	except r.ReqlRuntimeError as e:
		print e

	try:
		r.db('status').table_create('livehack', primary_key='team').run(conn)
		print "create status.livehack table"
	except r.ReqlRuntimeError as e:
		print e


	conn.close()

def export(rethink_db, item, export_dir):
	#os.system("rm -r %s" % export_dir)
	os.system("rethinkdb export -c %s:%s -e %s -d %s" % (rethink_db['ip'], rethink_db['port'], item, export_dir))

def import_cues(rethink_db, import_dir):
	backup("storage", "cues", "Name", rethink_db)
	os.system("rethinkdb import -f %s/cues.json --table storage.cues --force" % import_dir )
