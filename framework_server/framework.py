#!/usr/bin/python
# -*- coding: utf-8 -*-

# Title: framework.py
# Main script for script Modules that organize the ENDGAME cue system.

# External modules
import rethinkdb as r
import threading
from time import sleep
import signal
import sys
import json

# Local modules
import cue, listener, db_structure, network

# Variable: cue_bucket
# Global Variable. Every String you append will be dispatched as a cue
cue_bucket = []

# Variable: listener_stack
# holds all active change feed listeners
listener_stack = {}

# Variable: rethink
# holds connection values to rethinkdb from config.json
rethink = {}

# Variable: control_clients
# Network objects
# connection info about clients that get send each cue
control_clients = {}

# Variable: app_handler
# Network Objects
# connection info about app_handler clients
app_handler = None

# Variable: device_control
# connection to shell script on client PCs
device_ctrl = None

# Variable: running
# keeps cuebucket thread open until its set to False
running = True

# Variable: tempfolder
# Path to temporary files folder on shared drive
tempfolder = "/Temp";

# Function: signal_handler
# global function to register a quit operation
def signal_handler(signal, frame):
    quit()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

# Function: work_cues
# check the cue_bucket for new entries and dispatch if
# uses time.sleep to prevent massive performance usage
def work_cues():
	while running:
		if cue_bucket:
			# remove listeners that closed because there was a match
			for listen_name, listen_obj in listener_stack.items():
				if not listen_obj.open:
					listener_stack.pop(listen_name)

			dispatch(cue_bucket.pop(0))

		else:
			sleep(0.1)

# Function: dispatch
# - creates cue object
# - forwards cue object to control clients (e.g. MAX/MSP)
# - forwards action instrucions
# - looks for listeners to install
# - looks for listeners to close
def dispatch(cue_pointer):
	new_cue = cue.Cue(cue_pointer, rethink['ip'], rethink['port'])

	if cue_pointer["Name"] == "RESET":
		close_listener('all')
		feedback = db_structure.init(rethink, tempfolder)
		send_feedback(feedback)
	elif cue_pointer["Name"] == "CREATE":
		db_structure.create(rethink)
	
	forward(new_cue)

	if new_cue.properties is not None:
		# Forward Actions to Targetet App Handlers
		for action in new_cue.actions:
			threads = []
			for target in action['targets']:
				app_handler.send_json(action, host=target, timeout=5.0)
				threads.append(app_handler.sending)
			for thread in threads:
				thread.join()
			send_feedback(app_handler.report())

		# install Listeners and store them in a stack. Ignore listeners that are already running
		for listener_object in new_cue.listeners:
			new_listener = None;
			if 'Name' in listener_object:
				if listener_object['Name'] not in listener_stack:
					new_listener = listener.Listener(listener_object, cue_pointer["Name"], cue_bucket, rethink["ip"], rethink['port'])
				else:
					print("Can not install listener. A listener with the name %s is already active." % listener_object['Name'])
			else:
				if cue_pointer['Name'] not in listener_stack:
					new_listener = listener.Listener(listener_object, cue_pointer["Name"], cue_bucket, rethink["ip"], rethink['port'])
				else:
					print("Can not install listener. A listener with the name %s is already active." % cue_pointer['Name'])
			if new_listener is not None:
				if new_listener.open:
					listener_stack[new_listener.name] = new_listener
				

		# send control messages to client system script
		for control_msg in new_cue.control_messages:
			send_control(control_msg["targets"], control_msg)


		# close listeners
		close_listener(new_cue.close_calls)

	send_feedback("%s done" % new_cue.name)

# send dict formatted feedback msg to control clients
#
#
def send_feedback(feedback):
	message = {}
	message["feedback"] = feedback
	for client in control_clients:
				control_clients[client].send_json(message)
# 
# Build a new minimal cue from the Database cue to forward to Controlling and monitoring instances
#
def forward(cue):
	
	forward_cue = {}
	forward_cue["Name"] = cue.name

	if cue.properties is not None:
		if "Comment" in cue.properties:
			forward_cue["Comment"] = cue.properties["Comment"]
		elif "Page" in cue.properties:
			forward_cue["Page"] = cue.properties["Page"]
		elif "Index" in cue.properties:
			forward_cue["Index"] = cue.properties["Index"]
		elif "Style" in cue.properties:
			forward_cue["Style"] = cue.properties["Style"]

	# filter the cues origin else forward.
	for client in control_clients:
		if cue.source is not None:
			if cue.source == client:
				pass # dont forward to where it comes from
			else:
				control_clients[client].send_json(forward_cue)
				print("forwarding %s to %s" % (cue.name, client))
		else:
			control_clients[client].send_json(forward_cue)
			print("forwarding %s to %s" % (cue.name, client))


# Funciton: close_listeners
# close one, 'all' or a list of listeners.
# remove them from the stack
def close_listener(close_calls):
	# close all
	if close_calls == "all":
		for listener_name, listener_value in listener_stack.items():
			stop_listen(listener_name)
	# close single listener
	elif type(close_calls) is str or type(close_calls) is unicode:
		stop_listen(close_calls)
	# close list of listeners
	elif type(close_calls) is list:
		for close_call in close_calls:
			if close_call == "all":
				for listener_name, listener_value in listener_stack.items():
					stop_listen(listener_name)
			else:
				stop_listen(close_call)

def stop_listen(listener_name):
	if listener_name in listener_stack:
		print("Close Listener: %s" % listener_name)
		listener_stack[listener_name].close()
		listener_stack.pop(listener_name)
	else:
		print("%s can not be closed. Its not an active listener" % listener_name)

#
#
#
def control_device(targets, cmd, val):
	msg = {}

	if val == "true":
		msg[cmd] = True
	elif val == "false":
		msg[cmd] = False
	else:
		msg[cmd] = val
	
	hosts = []
	if targets[0] == 'all':
		del targets[0]
		for i in range(201, 231):
			hosts.append("192.168.0.%d" % i)
	else:
		for target in targets:
			hosts.append("192.168.0.%s" % target)

	send_control(hosts, msg)
	

def send_control(hosts, msg):
	print("forwarding control message")

	threads = []
	for ip in hosts:
		device_ctrl.send_json(msg, ip)
		threads.append(device_ctrl.sending)
	for thread in threads:
		thread.join()
	send_feedback(device_ctrl.report())

# Function: checkInput
# checking for command line user input.
def checkinput():
	
	newline = raw_input("\n")

	in_list = newline.split(" ")

	# print('keyboard input: %s' % in_list)

	if len(in_list) == 1:
		# quit the program
		if in_list[0] == "quit":
			quit()
		# create basic table structure
		elif in_list[0] == "create":
			db_structure.create(rethink)
		# initialize for a new game
		elif in_list[0] == "init":
			db_structure.init(rethink, tempfolder)
		# show currently active listeners
		elif in_list[0] == "listener":
			for l_name, l_obj in listener_stack.items():
				print("- Name: '%s'. Followcue: '%s'." % (l_name, l_obj.followcue) )
				#print(l_name, l_obj.properties)
		else:
			print('no valid input "%s". try again:' % in_list[0])

	if len(in_list) == 2:
		# dispatch a cue
		if in_list[0] == "cue":
			cue_bucket.append({"Name":in_list[1]})
		# close listener by name
		elif in_list[0] == "close":
			close_listener(in_list[1])
		else:
			print('no valid input "%s %s". try again:' % (in_list[0], in_list[1]))

	if len(in_list) > 2:
		# export table
		if in_list[0] == "export":
			db_structure.export(rethink, in_list[1], in_list[2])
		# import in cue table
		elif in_list[0] == "import":
			if in_list[1] == "cues":
				db_structure.import_cues(rethink, in_list[2])
		# start app on PC by IP
		elif in_list[0] == "start":
			del in_list[0]
			app_path = "/home/endgame/APPS/%s/" % in_list.pop(0)
			control_device(in_list, "npm", app_path)
		# control PC by IP
		elif in_list[0] == "control":
			if len(in_list) > 3:
				del in_list[0]
				cmd = in_list.pop(0)
				val = in_list.pop(0)
				control_device(in_list, cmd, val)
			else:
				print('no valid input %s. try again:' % in_list)
		# close list of listeners
		elif in_list[0] == "close":
			in_list.pop(0)
			close_listener(in_list)
		else:
			print('no valid input %s. try again:' % in_list)


# Function: quit
# join all open threads to close the program gracefully
def quit():
	# close cue Queue
	global running
	running = False
	
	if cue_bucket_thread.isAlive():
		cue_bucket_thread.join(2.0)
		
		if cue_bucket_thread.isAlive():
			print('could not join thread: %s' % cue_bucket_thread.name)

	# close network connections to control devices
	for client in control_clients:
		control_clients[client].quit()

	# close connection to app handler
	app_handler.quit()

	# close all open listeners
	close_listener("all")

# Function: main
# open channels for incomming cues and check for commandline input
if __name__ == "__main__":

	# fetch config options
	try:
		with open('config.json') as json_data:
			config = json.load(json_data)
			if 'rethink' in config:
				rethink = config['rethink']
			else:
				print "rethink DB connection values not found."
				sys.exit(0)

			if 'control' in config:
				for client in config['control']:

					ip = config['control'][client]['ip']
					port = config['control'][client]['port']

					control_clients[client] = network.UDP_connection(ip, port, client)

					if 'server' in config['control'][client]:
						server = config['control'][client]['server']
						control_clients[client].await_cue(server, cue_bucket)

			else:
				print "no control clients found in config.json"

			if 'app_handler' in config:
				# Leave host None on initial. It will be resolved with each cue.
				app_handler = network.TCP_connection(None, config['app_handler']['port'])
				#app_handler = network.UDP_connection(None, config['app_handler']['port'])
			else:
				print "no app_handler info found in config.json"

			if 'device_control' in config:
				device_ctrl = network.TCP_connection(None, config['device_control']['port'], "device_control")
				#app_handler = network.UDP_connection(None, config['app_handler']['port'])
			else:
				print "no device_control info found in config.json"

			if 'tempfolder' in config:
				tempfolder = config['tempfolder']

	except IOError as e:
		print e
		sys.exit(0)

	# check for cues to dispatch
	cue_bucket_thread = threading.Thread(name = 'cue_bucket_worker', target = work_cues, args = ())
	cue_bucket_thread.start()

	while True:
		#open std input for testsetupand interventions
		checkinput()

	print __name__, ' quit'
