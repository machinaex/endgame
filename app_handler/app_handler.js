main.init = function(appdata, language)
{
	console.log("APP_HANDLER INIT: " + JSON.stringify(appdata) + " lan " + language);
}

main.receive = function(message)
{
	
	/* Forward Appspecific data to each app.
	*  Append Data with 'all' property so every app gets it
	*/
  	main.config.apps.forEach(
	  	function(targetApp)
	  	{
	  		if (message[targetApp.name] != undefined && message.all != undefined)
	  		{
	  			// Be aware that merging only works fine with proper Objects! Not strings and lists
	  			let appdata = Object.assign({},message.all,message[targetApp.name]);
	  			sendToApp(targetApp, appdata);
	  		}
		    else if (message[targetApp.name] != undefined)
		    {
		      sendToApp(targetApp, message[targetApp.name]);
		    }
		    else if (message.all != undefined)
			{
				sendToApp(targetApp, message.all);
			}
	  	}
  	)
}

/* Function: sendToApp
*	 Forward Data to App.
*/
function sendToApp(targetApp, appdata)
{
	if (targetApp.name == "apphandler")
	{
		electron.ipcRenderer.send("handling", appdata);

		console.log(JSON.stringify(appdata) + " to apphandler");
		/*
		*  Filter headphone jack status
		*/
		if (appdata.audiojack != undefined)
		{
			player.setProperty("hardware", appdata);
		}
		if (appdata.speaker != undefined)
		{
			main.speaker(appdata.speaker);
		}
		if (appdata.headphones != undefined)
		{
			main.headphones(appdata.headphones);
		}
	} else {
		main.print("forwarding " + JSON.stringify(appdata) + " to " + targetApp.name + " port " + targetApp.udp);
 		main.sendUDP("0.0.0.0", targetApp.udp, JSON.stringify(appdata));
	}
	
}