const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

app.on('ready', () => {
  mainWindow = new BrowserWindow({
    height: 500,
    width: 900
  })

 let url = require('url').format({
    protocol: 'file',
    slashes: true,
    pathname: require('path').join(__dirname, 'index.html')
  })  
  mainWindow.loadURL(url)
  mainWindow.webContents.openDevTools()


	////// COMMUNICATION WITH BROWSER WINDOW ////////


 	mainWindow.webContents.on('did-finish-load', () => {

  	// this sends a message to the BrowserWindow
    mainWindow.webContents.send('ping', 'whoooooooh!');
	
	})

	/// add event listener for "pong":

	electron.ipcMain.on('pong', (event, arg) => {
  		
  		console.log(arg)  // prints "ping"
  		
  		//event.sender.send('ping', 'ping') // this is for asynch

  		event.returnValue = "pongSynch"; // this is only for responding to incoming synch messages.
  		// without it a synch message will break the system! 
  		// this seems to be the only way to work synchronous from 
  		// the main process.

  		// example reaction to message from the Renderer Process (window.js)
  		if (arg == "badabummbumm!")
  		{
  			mainWindow.setBounds({x:30,y:30,height:500, width:900})
  		}



		////////////// Buttons //////////

  		if (arg == "DevTools")
  		{
  			if(mainWindow.isDevToolsOpened())
  			{
  				mainWindow.closeDevTools()
  			}
  			else
  			{
  				mainWindow.openDevTools()
  			}
  			
  		}
  		else if (arg == "Move")
  		{
  			winBounds = mainWindow.getBounds();
  			if (winBounds.x != 510)
  			{
  				mainWindow.setPosition(510,328);
  			}
  			else
  			{
  				mainWindow.setPosition(110,28);
  			}
  		}
  		else if (arg == "pingSync")
  		{
  			console.log("pongSynch happened before. see index.js for how")
  		}
  		else if (arg == "pingAsync")
  		{
  			mainWindow.webContents.send('ping', 'pongAsync')
  		}
	})

  // see: https://electron.atom.io/docs/api/ipc-main/



})


// Make a method that's externaly visible
exports.pong = arg => {  
    //Print 6
    console.log("main got: "+ arg);
    return "pongMethod"
}
