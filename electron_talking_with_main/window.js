const electron = require('electron');


//  listener for message/event "ping" from main (index.js)
electron.ipcRenderer.on('ping', (event, message) => {

	console.log(message);  // Prints whatever comes  in 
	// with Async ping events.

	if (message == "whoooooooh!")
	{
		electron.ipcRenderer.send("pong","badabummbumm!");
	}

})



//sends sync message and receives it's response
//console.log(electron.ipcRenderer.sendSync("pong","pongSync"))




document.getElementById("DevTools").onclick = function() 
{
	electron.ipcRenderer.send("pong","DevTools");
};

document.getElementById("Move").onclick = function() 
{
	electron.ipcRenderer.send("pong","Move");
};

document.getElementById("sendSYNC").onclick = function() 
{
	// this is a synch event. It has to have a return value assigned
	// on the other side (the listener) in order to work and not break
	// the whole system
	console.log(electron.ipcRenderer.sendSync("pong","pingSync"));
};

document.getElementById("sendASYNC").onclick = function() 
{
	electron.ipcRenderer.send("pong","pingAsync");
}



var main = electron.remote.require("./index.js");

document.getElementById("UseMethod").onclick = function() 
{
	console.log(main.pong("pingMethod"));
}
