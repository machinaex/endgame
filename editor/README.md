Endgame Editor
==============

rethinkDB Editor basierend auf [jsoneditor](https://github.com/jdorn/json-editor).

Ermöglicht das Laden und Bearbeiten der storage.cue und storage.player Tabellen.

Löschen von Player und Cue Objekten ist wegen Konflikten und fatalen Problemen derzeit nicht möglich. Hierfür bitte eine eigene reql auf dem DB Server formulieren oder ReQL Pro benutzen.