var default_player = {
	"name": "",
	"ip": "192.168.0.",
	"language":"none",
	"score": 0,
	"team": undefined,
	"attributes":[],
	"hardware": {
		"speaker":true,
		"headphones":false,
		"audiojack":false
	},
	"login": {
		"password":""
	},
	"level0" : {
		"score":1
	},
	"website_app": {
		"chat_open":false,
		"found_first":false,
		"passage":"start",
		"path":"./data/",
		"person":"Arne_Brauer_A"
	},
	"profil_app": {
		"entry":[],
		"path":"./data/",
		"person":"Arne_Brauer_A",
		"score":1
	},
	"phone_app": {
		"fails": 0,
		"passage": "start",
		"path": "./data/",
		"play": false,
		"win": "",
		"person":"Arne_Brauer_A"
	},
	"questbook": {
		"listitems": []
	},
	"level1" : {
		"startbild": "1_2_3_A_bus.png",
		"score":1
	},
	"inventory": {
		"level":"TROLL"
	},
	"fileexplorer": {
		"path":"../../CONTENT/level3_fileexplorer"
	}
			
}


var plyr = {
	"title": "Player",
	"headerTemplate": "{{ i1 }} {{ self.name }} ({{ self.team }})",
	"type":"object",
	"properties": {
		"name": {
			"type":"string",
			"required":true,
			"propertyOrder": 2
		},
		"ip": {
			"propertyOrder": 1,
			"type":"string",
			"required":true
		},
		"language": {
			"type":"string",
			"required":true,
			"editable": false,
			"enum":["none", "de", "en"]
		},
		"score": {
			"type":"number",
			"required":true
		},
		"team": {
			"propertyOrder": 3,
			"type":"string",
			"required": true,
			"enum":[undefined, "paulie","tom","matt"]
		},
		"partner" : {
			"type": "string",
			"propertyOrder": 4,
			"required": true,
			"enum": ["Adalbert Anders","Brigitte Behrens","Carlos Casa","Dimos Dimitrios","Elfride Eisenbach","Fridolin Frei","Gabi Gretchen","Hermann Harenberg","Irmgard Inn","Jelena Jorgens","Karlo Krumbiegel","Lena Lausenbach","Marlies Mesebusch","Norbert Naechstenliebe","Otto Ohnsorg","Paula Petersen","Quaro Quentana","Ralf Robinson","Sabine Scheier","Tobi Tromsoe","Ursula Ulmen","Victor Vollmers","Werner Wolters","Xavier Xanto","Yosemite Yeng","Zacharias Zion","Aulana Aurenfeld","Eugen Eulershaven","Oezguer Oezdemir","Szlawa Szydlak"]
		},
		"attributes": {
			"type":"array",
			"format":"table",
			"required": true,
			"items": {
				"type":"string",
				"title":"attributes"
			}
		},
		"hardware": {
			"type":"object",
			"properties": {
				"speaker": {
					"type":"boolean",
					"format":"checkbox"
				},
				"headphones": {
					"type":"boolean",
					"format":"checkbox"
				},
				"audiojack": {
					"type":"boolean",
					"format":"checkbox"
				}
			}
		},
		"login": {
			"required": true,
			"type": "object",
			"properties": {
				"password": {
					"type": "string",
					"required": true
				}
			}
		},
		"level0" : {
			"type": "object",
			"required": true,
			"properties": {
				"correctinput": {
					"required" : true,
					"type": "array",
					"items": {
						"type": "string"
					}

				},
				"incorrectinput" : {
					"required": true,
					"type": "array",
					"items": {
						"type": "string"
					}
				},
				"score":{
					"type":"number", 
					"required":true
				}
			}
		},
		"level1" : {
			"type": "object",
			"required" :true,
			"properties":{
				"startbild": {
					"type": "string",
					"enum":  ["1_2_3_A_bus.png", "A_4_5_6_bus.png","7_8_9_B_bus.png","B_10_11_12_bus.png","13_14_15_C_bus.png","C_16_17_18_bus.png","19_20_21_D_bus.png","D_22_23_24_bus.png","25_26_27_E_bus.png","E_28_29_30_bus.png"]
				},
				"score":{
					"type":"number", 
					"required":true
				}
			}
		},
		"profil_app": {
			"type":"object",
			"required":true,
			"properties": {
				"entry": {
					"type":"array"
				},
				"path": {
					"type":"string"
				},
				"person": {
					"required":true,
					"type":"string",
					"enum":[
						"3A_Conny_Mühsal",
						"3B_Conny_Mühsal",
						"1A_Simon_Schildner",
						"1B_Simon_Schildner",
						"3A_Elena_Manfred",
						"3B_Elena_Manfred",
						"2A_Thilo_Sand",
						"2B_Thilo_Sand",
						"3A_Martin_Gutfried",
						"3B_Martin_Gutfried",
						"1A_Julian_Birk",
						"1B_Julian_Birk",
						"2A_Annemarie_Schlitzer",
						"2B_Annemarie_Schlitzer",
						"3A_Philipp_Koslowski",
						"3B_Philipp_Koslowski",
						"1A_Charlotte_Specht",
						"1B_Charlotte_Specht",
						"2A_Natascha_Markwart",
						"2B_Natascha_Markwart",
						"2A_Marco_Knochenwirt",
						"2B_Marco_Knochenwirt",
						"1A_Cornelius_Werner",
						"1B_Cornelius_Werner",
						"3A_Denise_Schicker",
						"3B_Denise_Schicker",
						"2A_Arne_Brauer",
						"2B_Arne_Brauer",
						"1A_Melanie_Niedermeyer",
						"1B_Melanie_Niedermeyer"
						]
				},
				"score":{
					"type":"number", 
					"required":true
				}
			}
		},
		"website_app": {
			"type":"object",
			"required":true,
			"properties": {
				"chat_open": {
					"type":"boolean",
					"format":"checkbox"
				},
				"found_first": {
					"type":"boolean",
					"format":"checkbox"
				},
				"passage": {
					"type":"string"
				},
				"path": {
					"type":"string"
				},
				"person": {
					"required":true,
					"type":"string",
					"enum":["Arne_Brauer_A", "Arne_Brauer_B"]
				}
			}
		},
		"phone_app": {
			"type":"object",
			"required":true,
			"properties": {
				"fails": {
					"type":"integer"
				},
				"passage": {
					"type":"string"
				},
				"path": {
					"type":"string"
				},
				"play": {
					"type":"boolean",
					"format":"checkbox"
				},
				"win": {
					"type":"string"
				},
				"person": {
					"required":true,
					"type":"string",
					"enum":["Arne_Brauer_A", "Arne_Brauer_B"]
				}
			}
		},
		"questbook": {
			"type" : "object",
			"required" : true,
			"properties": {
				"listitems": {
					"type":"array",
					"required": true
				}
			}
		},
		"inventory": {
			"type":"object",
			"properties" : {
				"style": {
					"type":"string",
					"required":true,
					"enum": ["playerA", "playerB", "playerC", "playerD", "playerE"]
				},
				"level": {
					"type":"string",
					"required":true,
					"enum":["TROLL", "FIN", "BEFEHL"]
				}
			}
		},
		"playground": {
			"type":"object",
			"properties": {
				"task": {
					"type":"string",
					"required":true,
					"enum": ["client", "server"]
				},
				"default": "client"
			}
		},
		"fileexplorer": {
			"type":"object",
			"properties":
			{
				"path": {
					"type:":"string",
					"required":true
				},
				"person": {
					"type":"string",
					"required":true,
					"enum":[
						"Conny Mühsal",
						"Simon Schildner",
						"Elena Manfred",
						"Thilo Sand",
						"Martin Gutfried",
						"Julian Birk",
						"Annemarie Schlitzer",
						"Philipp Koslowski",
						"Charlotte Specht",
						"Natascha Markwart",
						"Marco Knochenwirt",
						"Cornelius Werner",
						"Denise Schicker",
						"Arne Brauer",
						"Melanie Niedermeyer"
					]
				}
			}
		},
		"chat": {
			"type":"object"
		}
	},
	"default": default_player
}