/*
* Puzzle Piece of information that can be positioned on the grid along the cells.
*/

//////////////////////////////////////////////////////////////////////////////
// quickfix um player startfelder auf playground anzuzeigen
// startrow/startcol muss HIER und(!) im CSS geändert werden!!!!!
//////////////////////////////////////////////////////////////////////////////
let PlayerStyles = [
	{
		player: 'playerA',
		background_color: 'rgba(0,0,255,1)',
		startrow:1,
		startcol:0,
	},
	{
		player: 'playerB',
		background_color: 'rgba(0,128,0,1)',
		startrow:3,
		startcol:7,
	},
	{
		player: 'playerC',
		background_color: 'rgba(0,255,255,1)',
		startrow:6,
		startcol:0,
	},
	{
		player: 'playerD',
		background_color: 'rgba(255,0,0,1)',
		startrow:9,
		startcol:7,
	},
	{
		player: 'playerE',
		background_color: 'rgba(128,0,128,1)',
		startrow:11,
		startcol:0,
	}
];
//////////////////////////////////////////////////////////////////////////////
// ende quickfix
//////////////////////////////////////////////////////////////////////////////

function Tile(properties)
{
	this.name = properties.name;
	this.level = properties.level;
	this.nodes = [];
	this.player = properties.player;
	this.endpos = properties.endpos;
	this.status = "pending";
	this.match = properties.match;

	this.div = document.createElement('tile');
	this.div.id = this.name;
	this.div.className = this.player.style;
	console.log("new Tile: " + this.name + " " + this.player.style);
	var self = this;

	this.properties = properties; // save bare object to forward it to rethinkdb

	document.addEventListener('auxclick', callback, false);
	  function callback(e) {
	    console.log('not left mouse click');
	    e.preventDefault();
	    return
	  }

	for(node of properties.nodes)
	{
		new_node = new Node(node);
		this.div.append(new_node.div);
		this.nodes.push(new_node);
	}

	this.div.style.width = this.nodes.length * grid.cell_size;
	this.div.style.height = grid.cell_size;

	jq(this.div).click(function(evt) { self.infobox() });

	this.infobox = function()
	{
		console.log("Infotext comming up " + self.name);
	}

	this.apply = function()
	{
		// Inform Players inventory about applied Tile if you are the server playground
		if (task == "server")
		{
			let sendObject = {"inventory": {}};
			sendObject["inventory"]["applied"] = this.name;
			main.sendTCP(this.player.ip, 8002,sendObject);
			console.log('??????????????????????applied??????????????????????????');
			console.log(sendObject);
			console.log(this.player.ip);
		}

		this.moveTo(this.endpos.x, this.endpos.y, self.isApplied);
		this.status = "applied";
	}

	/*
	*  wird aufgerufen wenn apply bewegung beendet
	*/
	this.isApplied = function()
	{
		console.log(self.name + " applied");
		jq(self.div).addClass("applied");
		// HIER IST DIE BEWEGUNG BEENDET
	}

	this.show = function(container)
	{
		container.append(this.div); // Append before move so css properties are available
		this.moveTo(parseInt(jq(this.div).css("--startrow")), parseInt(jq(this.div).css("--startcol")));

	}

	this.hide = function(container)
	{
		console.log("hiding " + this.name)
		container.removeChild(this.div);
	}

	/*
	* Position tile on inventory grid and remember position
	*/
	this.enqueue = function(row, col, container)
	{
		container.append(this.div);
		this.queuePos = {"row":row, "col":col};
		this.moveTo(row, col);
	}

	this.moveTo = function(row, col, callback=undefined)
	{
		this.row = row;
		this.col = col;
    	jq(this.div).animate({left: grid.cells[row][col].pos.x, top: grid.cells[row][col].pos.y}, {duration:800, complete:callback });
	}
}

function Node(properties)
{
	//console.log(JSON.stringify(properties.text));
	this.name = properties.name;
	this.div = document.createElement('tile');
	this.div.id = this.name;

	this.div.className = "node"

	//this.div.style.width = grid.cell_size;
	//this.div.style.height = grid.cell_size;
	this.div.innerHTML = properties.text;
}


/* The Orientation grid to place tiles
*
*/
function Grid(rows, cols, canvas, showStartField)
{
	var ctx = canvas.getContext('2d');				// context element of board canvas
	this.cells = new Array(rows);

	this.rows = rows;
	this.cols = cols;

	// calculate maximum width of cell
	if (board.height > board.width)
	{
		this.cell_size = board.width / rows;
	} else {
		this.cell_size = board.height / cols;
	}


	for (let r = 0; r <= rows; r++)
	{
		this.cells[r]  = new Array(cols);
		for (let c = 0; c < cols; c++) {
			this.cells[r][c] = new Cell(r*this.cell_size, c*this.cell_size, this.cell_size, ctx);
			for (var i = 0; i < PlayerStyles.length; i++)
			{
				if (r >= PlayerStyles[i].startrow && r <= PlayerStyles[i].startrow+1 &&
					c >= PlayerStyles[i].startcol && c <= PlayerStyles[i].startcol && showStartField) {
					this.cells[r][c].drawRect(PlayerStyles[i].background_color);
					break;
				}
				else
				{
					this.cells[r][c].draw('rgba(50,50,50,0.5)');
				}
			}
		}
  }
}

/* One Grid element with x and y coordinates
*
*/
function Cell(x, y, size, ctx)
{
	//console.log(x + " / " + y + " / " + size);

	this.pos = {};
	this.pos.x = x;
	this.pos.y = y;
	this.size = size;

	this.draw = function(color)
	{
		ctx.strokeStyle = color;
		ctx.strokeRect(this.pos.x ,this.pos.y , size, size);
	}

	this.drawRect = function(color)
	{
		/* */
		ctx.beginPath();
		ctx.rect(this.pos.x ,this.pos.y , size, size);
		ctx.fillStyle = color;
		ctx.fill();

		ctx.strokeStyle = color;
		ctx.strokeRect(this.pos.x ,this.pos.y , size, size);

	}
}
