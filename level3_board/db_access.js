function updateLivehack(data)
{
  player.connect( function(conn)
        {
          r.db("status").table("livehack").get(team).update(data).run(conn, function(err, result){
            if (err) throw err;
            console.log(JSON.stringify(result, null, 2));
           })
         }
    );
}

function shareTile(team, tile)
{
  player.connect( function(connection) 
       {
         r.db("status").table("livehack").get(team).update({ tiles:r.row("tiles").setInsert(tile)}).run(connection, function(err, result){
           if (err) throw err;
          console.log(JSON.stringify(result, null, 2));
           })
         }
      );
}

function unshareTile(team, tile)
{
  player.connect( function(connection) 
       {
         r.db("status").table("livehack").get(team).update({ tiles:r.row("tiles").difference([tile])}).run(connection, function(err, result){
           if (err) throw err;
            console.log(JSON.stringify(result, null, 2));
           })
         }
      );
}

function getSetup(team, callback)
{
	player.connect(function(conn) {
    r.db("status").table("livehack").get(team).run(conn, function(err, result) {
      if (err) throw err;
	    //console.log(JSON.stringify(result, null, 2));
	    callback(result);
    })
  })
}

function listen(team, callback)
{
  player.connect(function(conn) {
    r.db("status").table("livehack").get(team).changes().run(conn, function(err, cursor) {
      if (err) throw err;
      cursor.each(function(err, row) {
        if (err) throw err;
        callback(row);
      });
    })
  })
}

// Obsolete
/*
function connect(queryfunction)
  {
    return r.connect( {host: player.host, port: player.port}, function(err, conn) {
            if (err) 
            {
              console.log("Could not connect to Database: " + err);
            } else {
              queryfunction(conn)
            }
      	}
      )
  }
*/

function getForeignPlayerData(name, callback)
{
  player.connect(function(conn) {
    r.db("status").table("player").getAll(name, {index:"name"}).run(conn, function(err, result) {
      if (err) throw err;
      //console.log(JSON.stringify(result, null, 2));
      callback(result);
    })
  })
}