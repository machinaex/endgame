function load_table(db,table,orderby, callback)
{
	player.connect( function(connection) 
		{
		 r.db(db).table(table).orderBy(orderby).run(connection, function(err, cursor){
		 		if (err) throw err;
				cursor.toArray(function(err, result) {
		    	if (err) throw err;
		    	// console.log(JSON.stringify(result, null, 2));
		    	callback(result);
				});
		 }
		 );
		}
	);
}

function get_cues(cues, callback)
{
	player.connect( function(connection) 
		{
		 r.db("storage").table("cues").getAll(r.args(cues)).run(connection, function(err, cursor){
			 		if (err) throw err;
					cursor.toArray(function(err, result) {
			    	if (err) throw err;
			    	//console.log(JSON.stringify(result, null, 2));
			    	callback(result, cues);
					});
			 }
		 );
		}
	);
}

