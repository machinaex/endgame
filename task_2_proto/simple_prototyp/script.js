let net = require('net'); // for tcp
let dgram = require('dgram'); // for udp
const electron = require('electron');
const {remote} = require('electron');


////////////////////////////////////////////////
//// COMMUNICATION WITH MAIN > /////////////


//  listener for message/event "ping" from main (main.js)
electron.ipcRenderer.on('fromMain', (event, message) => {

  console.log(message); // Prints whatever comes  in

  if(message.ALLOWSCROLLING != undefined)
  {
    let body = document.getElementsByTagName("BODY")[0];
    if(message.ALLOWSCROLLING)
    {
      body.style.overflow = "auto";
    }
    else
    {
      body.style.overflow = "hidden";
    }

  }
  if (message.SCREENSAVER == true)
  {screensaver(true);}
  if(message.SCREENSAVER == false)
  {screensaver(false);}
  if(typeof message.SCREENSAVER === 'object')
  {
    if (isEmpty(message.SCREENSAVER))
    {
      screensaver(false);
    } else
    {
      screensaver(true,message.SCREENSAVER.image,message.SCREENSAVER.opacity,message.SCREENSAVER.html)
    }

  }

  ///////////////////////////////////////////////
  /// ADD FILTERS for specific messages here ////
  ///////////////////////////////////////////////

})

function sendToMain(input){
  // in case we need to do sth that goes outside of this browser window:
  electron.ipcRenderer.send("fromWindow",input);
}


//// < COMMUNICATION WITH MAIN  ///////////
///////////////////////////////////////////////


function sendTCP(url,port,message){
  let client = new net.Socket();
  client.connect(port, url, function() {

    console.log('sending TCP to: ' + url + ':' + port);
    // Write a message to the socket as soon as the client is connected, the server will receive it as message from the client
    client.write(message);
    client.destroy(); // if no response is to be awaited else: comment this line out!

  });
}


function sendUDP(url,port,message){
  let message = new Buffer(message);

  let client = dgram.createSocket('udp4');
  client.send(message, 0, message.length, port, url, function(err, bytes) {
    if (err) throw err;
    console.log('UDP message sent to ' + port +':'+ url);
    client.close();
  });

}

function screensaver(on,image=undefined,opacity=1,html=undefined ){
  if(on)
  {
    if(image != undefined)
    {
      image = "url("+image+")";
    }
    else
    {
      image = "none";
    }
    let HTML = '<div id="screensaver" style="margin-left:0px;margin-top:0px;position:absolute;min-width:100%;min-height:100%;opacity:'+opacity+';z-index:100;background:#000; z-index:100;background-image:'+image+'"></div>';

    document.body.innerHTML += HTML;

    if (html != undefined)
    {
      let element = document.getElementById("screensaver")
      element.innerHTML = html;
      //console.log(element);
    }

  }
  else
  {
    try{
      let element = document.getElementById("screensaver");
      element.parentNode.removeChild(element);
    }
    catch(e){
      console.log("got screensaver false but there is no screensaver on.")
    }
  }

}

function isEmpty(obj) {
  for(let key in obj) {
    if(obj.hasOwnProperty(key))
    return false;
  }
  return true;
}


////////////////////////////////////////////////////////////////
//  HERE GOES THE APP CODE >>>>>>
/////////////////////////////////////////////////////

// macht aus allen divs in der WEBSITE draggables
// let draggables = document.querySelectorAll('.mover');
// [].forEach.call(draggables, function(item) {
//   item.addEventListener("dragstart", function(event) {
//     event.dataTransfer.setData("Text", event.target.textContent);
//     let id = "mover"+ new Date().getTime().toString();
//     item.setAttribute("id",id)
//     event.dataTransfer.setData("ID", id);
//   }, false);
// });
updateDraggables('.mover');

function updateDraggables(selector){
  let draggables = document.querySelectorAll(selector);
  [].forEach.call(draggables, function(item) {
    item.addEventListener("dragstart", function(event) {
      event.dataTransfer.setData("Text", item.getAttribute("profiltext"));
      event.dataTransfer.setData("Target", item.getAttribute("profiltarget"));
      if (item.getAttribute("id") === null) {
        let id = "mover"+ new Date().getTime().toString();
        item.setAttribute("id",id)}
      event.dataTransfer.setData("ID", item.getAttribute("id"));
    }, false);
  });
}

// Animation um die DROPZONE hervorzuheben
document.getElementById("profil").addEventListener('dragover', function (event) {
  event.preventDefault();
  event.target.style.borderWidth = "3px";
}, false);

// kümmert sich um das DROP EVENT
document.getElementById("profil").addEventListener('drop', function (event) {
  console.log(event);
  event.preventDefault();
  event.target.style.borderWidth = "0px";
  let draggable = document.getElementById(event.dataTransfer.getData("ID"));
  draggable.setAttribute("draggable", "false");

  console.log(event.dataTransfer.getData("Text"));
  console.log(event.dataTransfer.getData("Target"))
  let formularfeld = document.getElementById(event.dataTransfer.getData("Target"));
  formularfeld.innerHTML += "\n "+event.dataTransfer.getData("Text");
  //event.target.innerHTML += event.dataTransfer.getData("Text");
}, false);

// öffnet und schließt den CHAT auf Mausclick
document.getElementById("chat").style.display = "none";
document.getElementById("chatopener").addEventListener("click", function(){
  let chat = document.getElementById("chat");
  //console.log(chat.style.display === "none");
  chat.style.display = chat.style.display === "none" ? "table-cell" : "none";
});

// lädt CHAT TWINE JSON and populates Chat
let twine_chat = require('./task2_chatjson_test_2.json');
let current_node = twine_chat[0];
let current_index = 0;
let afterDecision = false;
let conversation = document.getElementById("conversation");
let input = document.getElementById("input");

function showNext(){
  if (current_node.msg.length>current_index) {
    let current_message = convertJsonToMessage(current_node["msg"][current_index]);
    switch (current_message.sender) {
      case "system":
      appendMessage(current_message);
      setTimeout(showNext, 3000);
      break;
      case "send":
      if(afterDecision){
        appendMessage(current_message);
        afterDecision = false;
        setTimeout(showNext, 3000);
      }
      else appendInput([current_message.text]);
      break;
      case "recieve":
      appendMessage(current_message);
      setTimeout(showNext, 1000);
      break;
      default:
      console.log("empty");
    }
    current_index++;
  } else {
    console.log("no more new messages, trying to create input buttons");
    appendDecisions(current_node["childrenNames"]);
  }
}

function appendMessage(obj){
  let msg = document.createElement("li");
  msg.innerHTML = obj.text;
  msg.setAttribute("class", obj.sender);
  conversation.appendChild(msg);

  updateDraggables('.chatmover');
}

function appendInput(text){
  //console.log(text);
  input.innerHTML = '';
  let option = document.createElement("button");
  option.setAttribute("class", "answer");
  option.setAttribute("type", "button");
  option.addEventListener('click', function (event) {
    appendMessage({"text":text,"sender":"self"});
    input.innerHTML = '';
    setTimeout(showNext, 3000);});
    option.innerHTML = text;
    input.appendChild(option);
  }

  function appendDecisions(options){
    //console.log(options);
    input.innerHTML = '';
    for (var i = 0; i < options.length; i++) {
      let text = cleanupDecisions(options[i]);
      let option = document.createElement("button");
      option.setAttribute("class", "answer");
      option.setAttribute("type", "button");
      option.addEventListener('click', function (event) {
        //appendMessage({"text":text,"sender":"self"});
        afterDecision = true;
        input.innerHTML = '';
        current_node = twine_chat[indexByValue(text)];
        current_index = 0;
        setTimeout(showNext, 1);});
      option.innerHTML = text;
      input.appendChild(option);
    }
  }

// cleans and gets tag/value in string {{tag}}value{{/tag}}
  function convertJsonToMessage(content){
    var ptrn = /\{\{((\s|\S)+?)\}\}((\s|\S)+?)\{\{\/\1\}\}/gm;
    var match;
    var obj = {};

    if((match = ptrn.exec(content)) !== null){
      var tag = match[1];
      // Gets the actual value inside the '{{tag}}value{{/tag}}' tags
      var value = match[3].split(/(\r\n|\n|\r)/gm);
      obj["sender"] = value.join('\r\n');
      // TODO: schnellen hack sauber lösen
      obj["text"] = content.substring(content.lastIndexOf("}}")+2);
    }
    return(obj);
  }

  function cleanupDecisions(text){
    return text.replace('[[','').replace(']]','');
  }

// gets index based on key value in one flat json
  function indexByValue(value) {
    for(var i = 0; i < twine_chat.length; i += 1) {
        if(twine_chat[i]["name"] === value) {
          console.log("corresponding passage index:");
          console.log(i);
            return i;
        }
    }
    console.log("no corresponding passage");
    return -1;
}

  showNext();

  /////////////////////////////////////////////////////
  //  <<<<<< HERE GOES THE APP CODE
  ////////////////////////////////////////////////////////////////
  /*
  function parseString(input){
  console.log("Befor: \n");
  console.log(input);
  var firstvariable = "{{system}}";
  var secondvariable = "{{/system}}";
  var regExString = new RegExp("(?:"+firstvariable+")(.*?)(?:"+secondvariable+")", "ig");
  var testRE = regExString.exec(input);
  console.log("After: \n");
  console.log(testRE);
}
*/
