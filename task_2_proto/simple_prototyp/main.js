var net = require('net'); // for tcp
var dgram = require('dgram'); // for udp

try{
	var config = require('./config.json'); //with path
}
catch(e){
	console.log("\n#####ERROR#####\nYour config file config.json is broken\n");
}

const {app,BrowserWindow,Menu,globalShortcut} = require('electron');
const path = require('path');
const electron = require('electron');
//const app = electron.app;
//const BrowserWindow = electron.BrowserWindow;


////////////////////////////////////////////////
//// COMMUNICATION WITH RENDERER > /////////////

// 	Listen for messages from BrowserWindow (Renderer) //

electron.ipcMain.on('fromWindow', (event, arg) => {

      console.log("main received from Window: "+arg)  // prints "ping"

	  ///////////////////////////////////////////////
	  /// ADD FILTERS for specific messages here ////
	  ///////////////////////////////////////////////

	  // best to implement JSON parser here and filter
	  // then for specific.

	  // example: if (arg == "stichwort"){dowhatever}
})


function sendToRenderer(message)
{
	mainWindow.webContents.send('fromMain', message);
}


//// < COMMUNICATION WITH RENDERER  ///////////
///////////////////////////////////////////////



app.on('ready', () => {

	startUDPServer();
	startTCPServer();

	globalShortcut.register('CommandOrControl+Alt+I', () => {
		/// COMMENT OUT AFTER DEVELOPMENT >>> ////
    	mainWindow.webContents.openDevTools();
    })
    	/// <<< COMMENT OUT AFTER DEVELOPMENT ////

	let windowconf = {height: 800,width: 800}
	if (config.WINDOW != undefined){windowconf = config.WINDOW;}
	mainWindow = new BrowserWindow(windowconf)
	//mainWindow.setMenu(null);

	let url = require('url').format({
	protocol: 'file',
	slashes: true,
	pathname: require('path').join(__dirname, 'index.html')
	})

	mainWindow.loadURL(url);
	//setMainMenu();

	app.on('browser-window-created',function(e,window) {
      window.setMenu(null);
  	});

})


function setMainMenu() {
  const template = [
    {
      //label: 'Filter',submenu: [{label: 'Hello',accelerator: 'Shift+CmdOrCtrl+H',click() {console.log('Oh, hi there!')}}]
    }
  ];
  Menu.setApplicationMenu(Menu.buildFromTemplate(template));
}


///////////////////////////////////////
/// Network Receive Servers to receive messages from the AppHandler:

function startUDPServer()
{
	var port = config.udpPort;
	var host = '127.0.0.1';
	var server = dgram.createSocket('udp4');
	server.on('listening', function(){
		var address = server.address();
		console.log("UDP server listening on "+ address.address +":"+port)
	});
	server.on("message", function (message, remote) {
		console.log("UDP: " + remote.address + ":" + remote.port + " " + message);
		message = String(message);
		message = filterNetworkIn(message);

		mainWindow.webContents.send('fromMain', message);
	});
	server.bind(port, host);
}



function startTCPServer()
{
	var server = net.createServer();
	server.on('connection', handleConnection);

	server.listen(config.tcpPort, function() {
	  console.log('TCP server listening to %j', server.address());
	});

	function handleConnection(conn) {
	  var remoteAddress = conn.remoteAddress + ':' + conn.remotePort;
	  console.log('new TCP client connection from %s', remoteAddress);

	  conn.setEncoding('utf8'); // important

	  conn.on('data', onConnData);
	  conn.once('close', onConnClose);
	  conn.on('error', onConnError);

	  function onConnData(message) {
	    console.log('TCP: %s: %j', remoteAddress, message);

	    message = filterNetworkIn(message);
	    // dispatch netReceive event for other parts of the program to know about it
	    mainWindow.webContents.send('fromMain', message);

	    //conn.write(d); // respond (not needed here)
	  }


	  function onConnClose() {
	    //console.log('connection TCP from %s closed', remoteAddress);
	  }

	  function onConnError(err) {
	    //console.log('Connection TCP %s error: %s', remoteAddress, err.message);
	  }
	}
}

function filterNetworkIn(json)
{
	try{
		json = JSON.parse(json);

		if (json.WINDOW != undefined)
		{
			console.log("setting BrowserWindow");
			json.WINDOW.forEach(function(command){

				console.log(command);
				if (command.startsWith("allowScrolling"))
				{
					sendToRenderer(command);
				}
				else
				{
					try{
						eval("mainWindow."+command);
					}
					catch(e){
						console.log(e);
					}
				}

			})
			delete json.WINDOW; // should we?
		}

	}
	catch(e){
		console.log("\n#####ERROR#####\nThis input is not a valid json and will be ignored: \n");
		json = "";
	}

	return json

}


function sendTCP(url,port,message)
{
	var client = new net.Socket();
	client.connect(port, url, function() {

		console.log('sending TCP to: ' + url + ':' + port);
		// Write a message to the socket as soon as the client is connected, the server will receive it as message from the client
		client.write(message);
		client.destroy(); // if no response is to be awaited else: comment this line out!

	});
}


function sendUDP(url,port,message)
{
	var message = new Buffer(message);

	var client = dgram.createSocket('udp4');
	client.send(message, 0, message.length, port, url, function(err, bytes) {
	    if (err) throw err;
	    console.log('UDP message sent to ' + port +':'+ url);
	    client.close();
	});

}
