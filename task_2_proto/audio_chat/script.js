var net = require('net'); // for tcp
var dgram = require('dgram'); // for udp
const electron = require('electron');
const {remote} = require('electron');


////////////////////////////////////////////////
//// COMMUNICATION WITH MAIN > /////////////


//  listener for message/event "ping" from main (main.js)
electron.ipcRenderer.on('fromMain', (event, message) => {

  console.log(message); // Prints whatever comes  in



  if(message.ALLOWSCROLLING != undefined)
  {
    let body = document.getElementsByTagName("BODY")[0];
    if(message.ALLOWSCROLLING)
    {
      body.style.overflow = "auto";
    }
    else
    {
      body.style.overflow = "hidden";
    }

  }
  if (message.SCREENSAVER == true)
  {screensaver(true);}
  if(message.SCREENSAVER == false)
  {screensaver(false);}
  if(typeof message.SCREENSAVER === 'object')
  {
    if (isEmpty(message.SCREENSAVER))
    {
      screensaver(false);
    } else
    {
      screensaver(true,message.SCREENSAVER.image,message.SCREENSAVER.opacity,message.SCREENSAVER.html)
    }

  }

  ///////////////////////////////////////////////
  /// ADD FILTERS for specific messages here ////
  ///////////////////////////////////////////////

})

function sendToMain(input)
{
  // in case we need to do sth that goes outside of this browser window:
  electron.ipcRenderer.send("fromWindow",input);
}


//// < COMMUNICATION WITH MAIN  ///////////
///////////////////////////////////////////////


function sendTCP(url,port,message)
{
  var client = new net.Socket();
  client.connect(port, url, function() {

    console.log('sending TCP to: ' + url + ':' + port);
    // Write a message to the socket as soon as the client is connected, the server will receive it as message from the client
    client.write(message);
    client.destroy(); // if no response is to be awaited else: comment this line out!

  });
}


function sendUDP(url,port,message)
{
  var message = new Buffer(message);

  var client = dgram.createSocket('udp4');
  client.send(message, 0, message.length, port, url, function(err, bytes) {
    if (err) throw err;
    console.log('UDP message sent to ' + port +':'+ url);
    client.close();
  });

}

function screensaver(on,image=undefined,opacity=1,html=undefined )
{
  if(on)
  {
    if(image != undefined)
    {
      image = "url("+image+")";
    }
    else
    {
      image = "none";
    }
    var HTML = '<div id="screensaver" style="margin-left:0px;margin-top:0px;position:absolute;min-width:100%;min-height:100%;opacity:'+opacity+';z-index:100;background:#000; z-index:100;background-image:'+image+'"></div>';

    document.body.innerHTML += HTML;

    if (html != undefined)
    {
      var element = document.getElementById("screensaver")
      element.innerHTML = html;
      //console.log(element);
    }

  }
  else
  {
    try{
      var element = document.getElementById("screensaver");
      element.parentNode.removeChild(element);
    }
    catch(e){
      console.log("got screensaver false but there is no screensaver on.")
    }
  }

}

function isEmpty(obj) {
  for(var key in obj) {
    if(obj.hasOwnProperty(key))
    return false;
  }
  return true;
}


////////////////////////////////////////////////////////////////////////////////
//  HERE GOES THE APP CODE >>>>>>
////////////////////////////////////////////////////////////////////////////////
const fs = require('fs');
const mustache = require('mustache');
const howler = require('./node_modules/howler/dist/howler.js')

const html_template = fs.readFileSync("audio_chat_template.html", "utf8");
const content_json = JSON.parse(fs.readFileSync("audio_chat_template.json", "utf8"));
const language = 'de';

let timeStart = Date.now();

const structure_json = require('./audio_chat_content.json');
const AUDIOPATH = './audio/'
let current_passage = structure_json[0];
let current_index = 0;
let fail_index = 0;
let fail_audio = [];
let fail_max = 3;
let playlist = {};
let answer_timer;
let wordcloud_timer = [];
let max_answer_time = 15000;


//////////////////////////////////////////
// program start
/////////////////////////////////////////

// prints ms in programmstart
console.log((Date.now()-timeStart)+ 'ms Init ');

// renders template with contents in selected language and appends to index.html
initHTML(html_template, content_json, language);

// load fail sounds
fail_audio = structure_json[getIndex(structure_json, 'name', 'fail')].audio;
fail_max = fail_audio.length-1;

// start ringing
let ringring = newAudio(AUDIOPATH, current_passage.audio[0], {loop:true, autoplay:true});

// stop klingeln.mp3 and hide button
document.getElementById('chatopener').addEventListener('click', function(event){
  ringring.stop();
  event.target.style.display = "none";
  document.getElementById("circle").style.display = "block";

  let index = getIndex(structure_json, 'name', cleanDecisionString(current_passage.childrenNames[0]))
  current_passage = structure_json[index];
  playNext();
});

// GAME LOOP
function playNext(){
  // geht durch die passagen des content_json
  if ('audio' in current_passage) {
    // solange noch audio files vorhanden werden diese nacheinander abgespielt
    if (current_passage.audio.length>current_index) {
      console.log('audio tag: check, current_index: '+current_index);
      // play next audio file newAudio(path, filename, options)
      newAudio(AUDIOPATH, current_passage.audio[current_index], {
          autoplay:true,onend: function(){current_index++;
          circle.style.animationName = "silent";
          playNext();}});

        if ('wordcloud' in current_passage) {
          for (var i = 0; i < current_passage.wordcloud.length; i++) {
            let word = current_passage.wordcloud[i]["word"];
            let time = current_passage.wordcloud[i]["time"]*1000;
            wordcloud_timer.push(setTimeout(function(){
              let wordcloud = document.createElement("DIV");
              wordcloud.setAttribute('class', 'wordcloud');
              wordcloud.innerHTML = word;
              document.getElementById('wordcloudcontainer').appendChild(wordcloud);
            },time))
          }
        }
      }
      // wenn es mehrere entscheidungen gibt werden diese der spielerin angezeigt
      else if(current_passage.childrenNames.length>1){
        console.log('audio tag: check, current_index: to big, appendDecisions childrenName');
        appendDecisions(current_passage.childrenNames);
      }
      // wenn es nur eine verzweigung gibt wird diese direkt gegangen
      else if(current_passage.childrenNames.length==1){
        console.log('audio tag: check, current_index: to big, only one childrenName');
        let index = getIndex(structure_json,'name',cleanDecisionString(current_passage.childrenNames[0]));
        current_passage = structure_json[index];
        current_index = 0;
        playNext();
      }
      else {
        console.log("keine audio files und decisions mehr");
    }
  }
  // falls es sich um eine sackgasse handelt wird ein fail sound abgespielt
  else if ('fail' in current_passage && fail_index<fail_max) {
    newAudio(AUDIOPATH, fail_audio[fail_index], {
      autoplay:true, onend: function(){
        let index = getIndex(structure_json,'name',cleanDecisionString(current_passage.childrenNames[0]));
        current_passage = structure_json[index];
        current_index = 0;
        playNext();}});
      fail_index++;
      console.log("FAIL "+fail_index);
    }
    else if (fail_index>=fail_max) {
      lost();
      }
    else {
      console.log("no audio tag and not fail tag in passage");
    }
}

//////////////////////////////////////////
// helper functions
/////////////////////////////////////////

// build html with selected language
function initHTML(_html, _content, _language){
  // for every item in content json add the one variable name with corresponding language
  let translatedContent = {};
  for (var i = 0; i < _content.length; i++) {
    translatedContent[_content[i]['variable']] = _content[i][_language];
  }
  // add title bar text
  document.title = translatedContent['TITLE'];
  // renders HTML in selected language and appends to HTML Body
  var rendered_HTML = mustache.to_html(_html, translatedContent);
  document.getElementById("pagecontent").innerHTML = rendered_HTML;
}

// function um audio objekte zu erzeugen
function newAudio(_path, _file, _options){
  let circle = document.getElementById('circle');
  let defaults = {
    src: [_path+_file],
    autoplay: false,
    loop: false,
    volume: 1,
    onend: function() {
      circle.style.animationName = "silent";
      //console.log((Date.now()-timeStart)+'ms Finished: '+_file);
    },
    onplay: function() {
      circle.style.animationName = "speak";
      //console.log((Date.now()-timeStart)+'ms Playing: '+_file);
    },
    onload: function(){
      //console.log((Date.now()-timeStart)+'ms Loaded: '+_file);
    }
  };
  // ersetzt defaults mit _options (wenn sie gesetzt wurden)
  return new Howl(Object.assign({}, defaults, _options));
}

function removeAudio(_key, _playlist){
  _playlist[_key].stop();
  _playlist[_key].unload();
  delete _playlist[_key];
}

// gets index based on key value in one flat json
function getIndex(json, key, value) {
  for(var i = 0; i < json.length; i += 1) {
    if(json[i][key] === value) {
      //console.log("corresponding passage index: "+i);
      return i;
    }
  }
  console.log("no corresponding passage");
  return -1;
}

// removes [[ and ]] from decisions text
function cleanDecisionString(text){
  return text.replace('[[','').replace(']]','');
}

// adds buttons to html
function appendDecisions(options){
  document.getElementById('wordcloudcontainer').innerHTML = "";
  for (var i = 0; i < options.length; i++) {
    let text = cleanDecisionString(options[i]);

    let option = document.createElement("button");
    option.setAttribute("class", "answer");
    option.setAttribute("type", "button");
    option.addEventListener('click', function (event) {
      clearTimeout(answer_timer);
      document.getElementById("decisions").innerHTML = '';
      current_passage = structure_json[getIndex(structure_json, 'name', text)];
      current_index = 0;
      playNext();});
    option.innerHTML = text;

    let container = document.createElement("div");
    container.setAttribute("class", "answercontainer");
    let background = document.createElement("div");
    background.setAttribute("class", "answerbackground");

    container.appendChild(option);
    container.appendChild(background);
    document.getElementById("decisions").appendChild(container);
  }

  // add timer to fail if ther
  answer_timer = setTimeout(function() {
    document.getElementById("decisions").innerHTML = '';
    if (fail_index<fail_max) {
      newAudio(AUDIOPATH, fail_audio[fail_index], {
        autoplay:true, onend: function(){
          current_index = 0;
          playNext();}});
          fail_index++;
          console.log("FAIL "+fail_index);
        } else {
          lost();
        }

      }, max_answer_time);
    }

function lost(){
  newAudio(AUDIOPATH, fail_audio[fail_max], {
    autoplay:true, onend: function(){
      }});
  console.log('lost');
}

/////////////////////////////////////////////////////
//  <<<<<< HERE GOES THE APP CODE
////////////////////////////////////////////////////////////////
