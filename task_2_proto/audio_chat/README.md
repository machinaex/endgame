Boilerplate
=========

Boilerplate (= Basis Code) für alle machina ENDGAME Electron Apps.


Funktionen
----------

1. **UDP senden** sendet Strings aus Main als auch Renderer (BrowserWindow) mit Hilfe der Funktion `sendUDP(url,port,message)`
2. **TCP senden** sendet Strings aus Main als auch Renderer (BrowserWindow) mit Hilfe der Funktion `sendTCP(url,port,message)`
3. **Kommunikation zwischen Main Process und Renderer Processes** aus der Main können Nachrichten an den Renderer geschickt werden mit Hilfe der funktion `sendToRenderer(string)` und vom Renderer an den Main Process mit Hilfe von `sendToMain(string)`
4. **UDP und TCP empfangen** Jede App beinhaltet eine config.json in der die Empfangsports für den UDP und den TCP Server eingestellt werden. Erreicht eine Nachricht die Server (die im Main Process laufen) senden diese den Inhalt der Nachricht als String via sendToRenderer(). So kommen alle Nachrichten von außen im Renderer über dasselbe Event "fromMain" an und können dort zentral gefiltert und geparsed werden.
5. **BrowserWindow verändern**: via json-API kann das App Fenster interaktiv verschoben, versteckt und anders verändert werden.


config
----------

im file **config.json** muss definiert werden:
`udp: <port>
tcp: <port>
`
optional geht:
`WINDOW:{<initialisierungsparameter für das BrowserWindow>}`

wenn kein WINDOW in der config angegeben ist, wird das Window mittig mit Größe 800x800 initialisiert.


API
----------

standard JSON im Format wie es vom AppHandler an Apps weitergeleitet wird:
`{"WINDOW":[<listofchangecommandsforBrowserWindow],
 "<appdata>":<appdata>},
 {"SCREENSAVER":{<config>}}
 `

`WINDOW:[]` ist optional: Hier kann als String notiert jede Methode aus github.com/electron/electron/blob/master/docs/api/browser-window.md eingefügt werden um das Fenster zu manipulieren.

**Bsp:** 
`{"WINDOW":["setPosition(0,0)","focus()"]}` : Verschiebt das App Fenster an die Position 0,0 und fokusiert es.


`"SCREENSAVER":{}` ist optional und funktioniert entweder mit einfachen boolschen wie `"SCREENSAVER":true` (Schwarzer Screen über App) oder `"SCREENSAVER":false` (Screensaver aus) 
oder mit Parametern:
 `"SCREENSAVER":{"image":<linktoimage>, "opacity":<0. - 1.>, "html":<htmlstring>}`
 alle parameter sind optional. Anwesenheit eines Parameters wird als *true* Befehl zum Anschalten des Screensavers interpretiert. 
 Ein leeres Objekt `"SCREENSAVER:{}` wird interpretiert wie *false* (Screensaver aus)


 REZEPTE
----------

