## NaturalDocs in aller Kürze##

# Installieren #

[NaturalDocs herunterladen](http://www.naturaldocs.org/download.html)

In den gewünschten Ordner entpacken und (Linux) ausführbar machen: "chmod +x NaturalDocs"



# Dokumentieren #

Tip Top Einführung auf der [NaturalDocs Webseite](http://www.naturaldocs.org/documenting/walkthrough.html)


**Gut zu wissen**: 

* Wenn immer in irgendeinem Kommentar in irgendeinem Dokument des Projektes, das zu einer der [unterstützten Sprachen](http://www.naturaldocs.org/languages.html) gehört, ein Keyword auftaucht landet der Text in der Dokumentation.

* Nach dem Keyword kommt ein Doppelpunkt und alles was dahinter steht wird zur Überschrift des Doku Eintrages. Alles was dahinter, bis zum Ende des Kommentars oder zum nächsten Keyword kommt wird zum Text des Doku Eintrages.

* Der Eintrag wird je nach Keyword anders Formatiert

* alle Einträge einer Datei landen auf einer Unterseite in der Dokumentation.

* Wenn in einer .txt Datei ein Keyword auftaucht, wird die Datei Kompletto integriert.


[Liste aller gültigen Keywords](http://www.naturaldocs.org/keywords.html)



# Ausführen #

[Anleitung auf der Webseite](http://www.naturaldocs.org/running.html)

NaturalDocs braucht zwei Ordner für die Dokumentation die vorher (z.B. im Projekt selbst) angelegt werden müssen. Der Dokumentationordner und der Projektordner für NaturalDocs.
Beim Ausführen von NaturalDocs muss angegeben werden wo sich die Projektdateien befinden und wo sich der Dokumentationsordner und der Projektordner befinden.
  
Ich empfehle die Dokumentation direkt in dieses machina eX Bitbucket Repository zu schieben:  
[https://bitbucket.org/machinaex/machinaex.bitbucket.io]()
  
Es wird als webseite interpretiert und ist unter [https://machinaex.bitbucket.io/]() erreichbar. Am besten kommen die ENDGAME Dokus in den unterordner:  
docs/endgame/*projekttitel*  

**Execute Beispiel:** 

`$ ./NaturalDocs -i ~/Documents/endgame/framework_server -o HTML ~/Documents/machinaex.bitbucket.io/docs/endgame/framework_server -p ~/Documents/machinaex.bitbucket.io/docs/endgame/framework_server/proj`

wenn ihr etwas in den Dokumentationsrelevanten Kommentaren geändert habt sieht das im Terminal etwa so aus:  

```
Finding files and detecting changes...
Parsing 1 file...
Building 1 file...
Updating CSS file...
Done.
```
  
Die index.html der Dokumentation findet ihr im Dokumentationsordner.


# Volle Breitseite #

Für JavaScript gibt es eine [Extension für NaturalDocs](https://github.com/prantlf/NaturalDocs) die Full Language Support bietet.

Das Programm ist im Prinzip das selbe. Wenn ihr jedoch den [Full Language Support für JavaScript](https://github.com/prantlf/NaturalDocs#full-support) aktiviert sucht NaturalDocs automatisch alle Funktionen, Variablen, Parameter pipapo zusammen und packt sie automatisch in die Dokumentation, ob kommentiert oder nicht.