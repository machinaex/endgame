
// GAME LOOP
handleChat = function(){
  // geht durch die passagen des content_json
  if ('audio' in crrPassage) {
    // solange noch audio files vorhanden werden diese nacheinander abgespielt
    if (crrPassage.audio.length>crrIndex) {
      playing.push(createAudio(PATH+ASSETS+crrPassage.audio[crrIndex], {
        autoplay:true,
        onplay:function(){
          document.getElementById(CONSTANTS.calleeSymbol).style.animationName = "speak";
        },
        onend:function(){
          document.getElementById(CONSTANTS.calleeSymbol).style.animationName = "silent";
          triggerID(crrPassage);
          handleChat();}
        }));
        crrIndex++;
      }
      // wenn es mehrere entscheidungen gibt werden diese der spielerin angezeigt
      else if(crrPassage.decisions.length>0){
        appendDecisions(crrPassage.decisions);
      }
      else if ('win' in crrPassage) {
        win();
      }
      else if ('fail' in crrPassage) {
        fail();
      }
      else {
        console.log("keine audio files und decisions mehr");
      }
    }
    // falls kein AUDIO Tag in der Passage vorhanden ist:
    else {console.log("no audio tag, no win and no fail tag in passage ... nothing to do here"); }
  }

function win(){
  console.log('win');
  document.getElementById(CONSTANTS.calleeSymbol).style.display = "none";
  outputToSpeaker();
  player.setAppdata({"win":true});
  player.setAppdata({"play":false});
  player.setAppdata({"busy":false});
  setQuestbook({"command":"set", "tags":"phone", "status":"checked"});
  showGameEndScreen(true);
}

function lost(){
  console.log('lost');
	document.getElementById(CONSTANTS.calleeSymbol).style.display = "none";
  outputToSpeaker();
  player.setAppdata({"win":false});
  player.setAppdata({"play":false});
  player.setAppdata({"busy":false});
  setQuestbook({"command":"set", "tags":"phone", "status":"checked"});
  showGameEndScreen(false);
}

function fail(){
  lost();
}

function showGameEndScreen(_win){
  //let message = {"phone_app":{"WINDOW": ["maximize()"]}};
  //main.send(message);
  let message2 = {"profil_app":{"WINDOW": ["minimize()"], "lock":true}};
  main.send(message2);
  let message3 = {"website_app":{"WINDOW": ["minimize()"]}};
  main.send(message3);

    swal({
      title: _win ? "YOU ARE UEBER AWESOME!" : "DU BIST AUFGEFLOGEN!",
      text: _win ? "Du hast dir was aus der Kaffeeecke verdient!" : "Hoffentlich hat dein*e Kolleg*in mehr Erfolg..." ,
      icon: _win ? "success" : "error",
      button: false,
      closeModal: false,
      closeOnClickOutside: false,
      closeOnEsc: false
    }).then((value) => {
      let message5 = {"WINDOW": ["minimize()","unmaximize()"]};
      main.send(message5);
    });

    let message4 = {"statusbalken":{"WINDOW": ["show()"]}};
    main.send(message4);

}

////////////////////////////////////////////////////////////////////////////////
// HELPER FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

triggerID = function(obj){
  if ('ID' in obj) {
    let message = {"profil_app":{"trigger":{"id": "ID13"}}};
    main.send(message);
    console.log(message);
  }
}


// gets index based on key value in one flat json
indexByValue = function(_json, _key, _value) {
    for(var i = 0; i < _json.length; i += 1) {
        if(_json[i][_key] === _value) {
          console.log("corresponding passage index: " + i);
            return i;
        }
    }
    console.log("no corresponding passage");
    return -1;
}

setQuestbook = function(msg){
  let message = {"questbook": msg};
  main.send(message);
}

// function um audio objekte zu erzeugen
createAudio = function(_file, _options){
  let defaults = {
    src: [_file],
    autoplay: true,
    loop: false,
    volume: 1,
    onend: function() {
    },
    onplay: function() {
    },
    onload: function(){
    }
  };
  return new Howl(Object.assign({}, defaults, _options));
}
