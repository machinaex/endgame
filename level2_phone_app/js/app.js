const Mustache = require("mustache");
const fs = require('fs');
const $ = require('jquery');
const swal = require('sweetalert');
const exec = require('child_process').exec;
require('howler');

const CONSTANTS = {
  contentPath: '../../CONTENT/level2/phone/',
  htmlTemplate: 'ASSETS/TEMPLATE.html',
  jsonContent: '/CONTENT_', //DE.json
  globalAssets: '/ASSETS/',
  contentContainer: 'pagecontent',
	wordcloudContainer: 'words',
  wordDiv: 'word',
  calleeSymbol: 'circle',
  calleeContainer: 'caller',
  pickupBtn: 'pickup',
  decisionContainer: 'decisions',
	soundDecisions: 'answer_available.wav',
	maxTimeAnswer: 15000,
  timeoutSound:'fail2.wav'
};
let PATH;
let ASSETS;
let START = false;
let chat;
let crrPassage;
let crrIndex;
let playing = [];
let failIndex;
let soundsFail = [];
let soundsPlaying = [];
let timerAnswer;
let timerWordcloud = [];

main.init = function(myappdata, language){}

localInit = function(myappdata, language){
	let TEMPLATE, CONTENT, CHAT;
	// set paths to content folder and load files
	try {
		language = language.toUpperCase();
    let PERSON = myappdata.person.substring(3);
		PATH = CONSTANTS.contentPath+PERSON;
		TEMPLATE = fs.readFileSync(CONSTANTS.contentPath+CONSTANTS.htmlTemplate, "utf8");
		CONTENT = JSON.parse(fs.readFileSync(PATH+CONSTANTS.jsonContent+language+'.json', "utf8"));
    ASSETS = '/ASSETS/'+language+'/';
		CONTENT.path = CONSTANTS.contentPath+PERSON+ASSETS;
		CHAT = JSON.parse(fs.readFileSync(PATH+'/PHONE_'+language+'.json', "utf8"));
	} catch (e) {
		console.log('Error on load files in main init with: ' + e);
		player.setAppdata({"load_files":"ERROR!"});
	} finally {

		// load chat content and start on entry point
		chat = CHAT;
    crrPassage = chat[indexByValue(CHAT, 'name', 'start')]; // load start passage
    crrIndex = 0; // set Index to first message

		// set Path Variable for HTML Assets, render and append to app
		renderTemplate(TEMPLATE, CONTENT);
		player.setAppdata({"load_files":"CHECK!"});
    player.setAppdata({"busy":false});

    // middle mouse preventer
    document.addEventListener('auxclick', auxclick, false);
	}
}

main.receive = function(mydata){

  if ('init' in mydata) {
    localInit(mydata.init, mydata.language)
  }

	if('start' in mydata && !START){
    START = true;
		start();
		player.setAppdata({"play":true});
    outputToHeadphone();
	}
  else if ('stop' in mydata) {
		for (var i = 0; i < playing.length; i++) {
			playing[i].stop();
		}
		playing = [];
		player.setAppdata({"play":false});
    player.setAppdata({"busy":false});
    outputToSpeaker();
	}
  else if ('audiojack' in mydata) {
    if (!mydata.audiojack) {
      headphoneJackError();
    } else {
      swal.close();
    }
  }
}

////////////////////////////////////////////////////////////////////////////////
// HELPER FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
function auxclick(e) {
  console.log('not left mouse click');
  e.preventDefault();
  return
}

// renders Template with selected language and appends to index.html
renderTemplate = function(_template, _content){
  // clean up html element container
  let container = document.getElementById(CONSTANTS.contentContainer);
  while (container.firstChild) {
    container.removeChild(container.firstChild);
  }
  // append newly renderd content
  let site = Mustache.to_html(_template, _content);
  container.innerHTML += site;

  $('body').on('drop', function(event) { event.preventDefault(); });
  $('body').on('dragstart', function(event) { event.preventDefault(); });
  $('body').on('selectstart', false);
}


start = function(){
  player.get(["hardware"], function(data) {
    if (!data.hardware.audiojack) {
      headphoneJackSwal(false);
    } else {
      // TODO  swal: 'Kopfhörer aufsetzen jetzt!!'
      headphoneJackSwal(true);
      console.log(data);
    }
  });
	document.getElementById(CONSTANTS.pickupBtn).addEventListener('click', function(event){
    //player.get(["partner"], pickupCallback); // MUSS WIEDER REIN BUG
    handlePartner('Kein Partner. BUGFIX', {}); // BUG TODO: handle boilerplate exception!!!
	});
}

pickupCallback = function(data){
  console.log("Partner: " + data.partner);
  getForeignPlayerData(data.partner, "phone_app", handlePartner);
}

handlePartner = function(name, data){
  console.log("Partner busy?: " + data.busy);
  if ('busy' in data) { // TODO: Testen ob Bugfix für kein Partner BUG
    if (data.busy) {
      swal({
        title: "Dein*e Tischnachbar*in "+name+" telefoniert gerade mit dieser Nummer!",
        text: "Sprech dich mit "+name+" ab!",
        icon: "error",
        button: true
      });
    } else {
      player.setAppdata({"busy":true});
      document.getElementById(CONSTANTS.pickupBtn).style.display = "none";
      crrPassage = chat[indexByValue(chat, 'name', 'start')];
      handleChat();
    }
  } else { // TODO: Testen ob Bugfix für kein Partner BUG
    player.setAppdata({"busy":true});
    document.getElementById(CONSTANTS.pickupBtn).style.display = "none";
    crrPassage = chat[indexByValue(chat, 'name', 'start')];
    handleChat();
  }


}

getForeignPlayerData = function(name, property, callback){
  player.connect(function(conn) {
    r.db("status").table("player").getAll(name, {index:"name"}).pluck(property).run(conn, function(err, cursor) {

      if (err) throw err;
      cursor.toArray(function(err, result) {
        if (err) throw err;
        // console.log(JSON.stringify(result, null, 2));
        callback(name, result[0][property])
    });

    })
  })
}


// adds buttons to html
appendDecisions = function(options){
  // removes all old word from wordcloud
	document.getElementById(CONSTANTS.wordcloudContainer).innerHTML = "";

	for (var i = 0; i < options.length; i++) {
		let text = options[i]
		let option = document.createElement("button");
    option.setAttribute("class", "answer");
    option.setAttribute("type", "button");
    option.addEventListener('click', function (event) {
      clearTimeout(timerAnswer);
      document.getElementById(CONSTANTS.decisionContainer).innerHTML = '';
			crrPassage = chat[indexByValue(chat,'name',text)];
      crrIndex = 0;
      handleChat();});
    option.innerHTML = text;

    let container = document.createElement("div");
    container.setAttribute("class", "answercontainer");
    let background = document.createElement("div");
    background.setAttribute("class", "answerbackground");
    container.appendChild(option);
    container.appendChild(background);
    document.getElementById(CONSTANTS.decisionContainer).appendChild(container);
  }

	// play sound that decisions are available
	playing.push(createAudio(PATH+ASSETS+CONSTANTS.soundDecisions,
		{autoplay:true, volume: .5}));

  // add timer to fail if ther
	timerAnswer = setTimeout(function() {
		document.getElementById(CONSTANTS.decisionContainer).innerHTML = '';
    playing.push(createAudio(PATH+ASSETS+CONSTANTS.timeoutSound, {
      autoplay:true,
      onplay:function(){},
      onend:function(){lost();}
      }));
		}, CONSTANTS.maxTimeAnswer);
}

headphoneJackSwal = function(toggle) {
  swal({
    title: toggle ? "Kopfhörer aufsetzen!" : "Keine Kopfhörer eingesteckt!",
    text:  toggle ? false : "Please plugin your headphones!",
    icon: "info",
    button: toggle,
    closeModal: false,
    closeOnClickOutside: false,
    closeOnEsc: false
  });
}

execute = function(command, callback){
  exec(command, function(error, stdout, stderr){
    callback('stdout: '+stdout + 'stderr: '+stderr);
  });
}

outputToHeadphone = function(){
  // call the function
  execute("amixer set 'Speaker+LO' mute; amixer set 'Headphone' unmute", function(output) {
      console.log(output);
  });
}

outputToSpeaker = function(){
  // call the function
  execute("amixer set 'Speaker+LO' unmute; amixer set 'Headphone' mute", function(output) {
      console.log(output);
  });
}
