let msgSelected = false;

// main loop for chat
handleChat = function(){
  console.log('handleChat');
  if (crrPassage.msg.length>crrIndex) {
    console.log("next messages");
    switch (crrPassage.msg[crrIndex].sender) {
      case 'receive':
          let delayTime = getRandomInt(1500, 3000);
          console.log(delayTime);
          initTyping(delayTime);
          setTimeout(function(){
            appendMessage(crrPassage.msg[crrIndex]); crrIndex++;
          }, delayTime);
          setTimeout(handleChat, delayTime + 1000);
      break;
      case 'send':
          if (msgSelected) {
            appendMessage(crrPassage.msg[crrIndex]);
            setTimeout(handleChat, 2000);
            crrIndex++;
            msgSelected = false;
          } else {
            appendDecision(crrPassage.msg[crrIndex]);
          }
      break;
      default:
        console.log('in default');
        appendMessage(crrPassage.msg[crrIndex]);
        setTimeout(handleChat, 2000);
        crrIndex++;
    }

  }
  else if (!('decisions' in crrPassage)) {
    console.log("no more messages and no decisions, but WIN!!");
    // send to profil_app
    player.setAppdata({"chat_done":true});
  }
  else if (crrPassage.decisions.length > 0){
    console.log("no more messages, but decisions");
    appendDecisions(crrPassage.decisions);
  }
   else {
    console.log("SACKGASSE. WTF!? passage: " + crrPassage);
  }
}

initTyping = function(alive){
  let msg = document.createElement("li");
  msg.innerHTML = '...';
  msg.setAttribute("class", "typing");
  document.getElementById("conversation").appendChild(msg);
  setTimeout(function(){
    $( ".typing" ).remove();
  }, alive-100);

  // scroll to current message
  let container = document.getElementById("chatcontainer");
  container.scrollTop = container.scrollHeight;
}

appendMessage = function(obj){
  // append new message to conversation
  let msg = document.createElement("li");
  msg.innerHTML = obj.text;
  msg.setAttribute("class", obj.sender);
  let sender = document.createElement("span");
  sender.setAttribute("class", "name");
  sender.innerHTML = obj.sender == 'receive' ? NAME : '';
  msg.appendChild(sender);
  document.getElementById("conversation").appendChild(msg);

  // scroll to current message
  let container = document.getElementById("chatcontainer");
  container.scrollTop = container.scrollHeight;

  initMovers(msg, CONSTANTS.siteMover);
  createAudio(PATH+CONSTANTS.soundMsg, {  volume: .5 });
}

appendDecisions = function(options){
  let input = document.getElementById("input");
  input.innerHTML = '';

  for (var i = 0; i < options.length; i++) {
    let text = options[i].substring(0, options[i].indexOf("|"));
    let passage = options[i].substring(options[i].indexOf("|")+1);
    let option = document.createElement("button");
    option.setAttribute("class", "answer");
    option.setAttribute("type", "button");
    option.addEventListener('click', function (event) {
      input.innerHTML = '';
      //appendMessage({'text':text, 'sender':'send'});
      crrPassage = CHAT[indexByValue(CHAT, 'name', passage)];
      crrIndex = 0;
      msgSelected = true;
      setTimeout(handleChat, 10);
    });
      let inner = document.createElement('span');
      inner.innerHTML = text;
      option.appendChild(inner);
      //option.innerHTML = text;
      input.appendChild(option);
    }

    createAudio(PATH+CONSTANTS.soundDecision, { volume: .5});
}

appendDecision = function(obj){
  let input = document.getElementById("input");
  input.innerHTML = '';

    let text = obj.text
    let option = document.createElement("button");
    option.setAttribute("class", "answer");
    option.setAttribute("type", "button");
    option.addEventListener('click', function (event) {
      input.innerHTML = '';
      msgSelected = true;
      setTimeout(handleChat, 10);
    });
    let inner = document.createElement('span');
    inner.innerHTML = text;
    option.appendChild(inner);
    //option.innerHTML = text;
    input.appendChild(option);

    createAudio(PATH+CONSTANTS.soundDecision, {  volume: .5 });
}

// gets index based on key value in one flat json
indexByValue = function(_json, _key, _value) {
    for(var i = 0; i < _json.length; i += 1) {
        if(_json[i][_key] === _value) {
          console.log("corresponding passage index: " + i);
            return i;
        }
    }
    console.log("no corresponding passage for: " + _value);
    return -1;
}

// function um audio objekte zu erzeugen
createAudio = function(_file, _options){
  let defaults = {
    src: [_file],
    autoplay: true,
    loop: false,
    volume: 1,
    onend: function() {
    },
    onplay: function() {
    },
    onload: function(){
    }
  };
  return new Howl(Object.assign({}, defaults, _options));
}

getRandomInt = function(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
