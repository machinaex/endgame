const Mustache = require("mustache");
const fs = require('fs');
const $ = require('jquery');
require('howler');

const CONSTANTS = {
  contentPath: '../../CONTENT/level2/website/',
  soundMsg: 'ASSETS/msg_recieve.wav',
  soundDecision: 'ASSETS/answer_available.wav',
  contentContainer1: 'webframe1',
  contentContainer2: 'webframe2',
  htmlFakebook: '/FAKEBOOK_',
  htmlTemplate: '/TEMPLATE.html',
  siteContainer: 'webframe',
  siteMover: '.mover',
  chatContainer: 'chat',
  chatBtn: 'chatopener'
};
let PATH, CONTENT, CHAT, LASTSENT, NAME;

let chat;
let crrPassage;
let crrIndex;
main.init = function(myappdata, language){ }
localInit = function(myappdata, language){
  // set paths to content folder and load files
  try {
    language = language.toUpperCase();
    PATH = CONSTANTS.contentPath;
    let VERSION = myappdata.person.charAt(1);
    NAME = myappdata.person.substring(3);
    let LANGUAGE = language.toUpperCase();

    let CHATPATH = PATH+'CHAT/'+LANGUAGE+'/'+VERSION+'/'+NAME+'.json';
    console.log(CHATPATH);

    chat = fs.readFileSync(CHATPATH, "utf8");
    CHAT = JSON.parse(chat);
    chat = CHAT;
    crrPassage = chat[indexByValue(CHAT, 'name', 'start')]; // load start passage
    crrIndex = 0; // set Index to first message

    let FBPATH = PATH+'FAKEBOOK/'+LANGUAGE+'/'+VERSION+'/'+NAME+'_mini.html';
    let fakebook = fs.readFileSync(FBPATH, "utf8");
    renderTemplate(fakebook, {}, CONSTANTS.contentContainer1);

  } catch (e) {
    console.log('Error on load files in main init with: ' + e);
    player.setAppdata({"load_files":"ERROR!"});
  } finally {
    initChatBtn();
    handleChat();
    document.getElementById(CONSTANTS.contentContainer1).style.display = 'none';
    //document.getElementById(CONSTANTS.contentContainer2).style.display = 'none';
    player.setAppdata({"load_files":"CHECK!"});
  }
}

main.receive = function(mydata){
  if('chat' in mydata){
      document.getElementById(CONSTANTS.chatBtn).style.display = "table-cell";
      document.getElementById(CONSTANTS.chatContainer).style.display = "table-cell"
  }
  if('render' in mydata){
    if(mydata.render == 'website') {
      document.getElementById(CONSTANTS.contentContainer1).style.display = 'none';
      //document.getElementById(CONSTANTS.contentContainer2).style.display = 'block';
    }
    else if(mydata.render == 'fakebook') {
      document.getElementById(CONSTANTS.contentContainer1).style.display = 'block';
      //document.getElementById(CONSTANTS.contentContainer2).style.display = 'none';
    }
  }
  if ('init' in mydata) {
    localInit(mydata.init, mydata.language)
  }

}

// renders Template with selected language and appends to index.html
renderTemplate = function(_template, _content, _container){
  // clean up html element container
  let container = document.getElementById(_container);

  while (container.firstChild) {
    container.removeChild(container.firstChild);
  }

  // append newly renderd content
  let site = Mustache.to_html(_template, _content);
  container.innerHTML += site;
  initMovers(document, CONSTANTS.siteMover);

  // prevent img drag and text select
  $('body').on('drop', function(event) { event.preventDefault(); });
  $('body').on('dragstart', function(event) { event.preventDefault(); });
  $('body').on('selectstart', false);
}

// walks trough website and adds eventlistener to all collectables
initMovers = function(_element, selector){
  let movers = _element.querySelectorAll(selector);
  [].forEach.call(movers, function(item) {
    addMoverClick(item);
  });
}

addMoverClick = function(node){
  node.addEventListener("click", function(event){
    node.classList.remove("mover");
    node.style.display = 'inline-block';
    this.removeEventListener('click', arguments.callee);

    if(LASTSENT != node.getAttribute("trigger")){
    // update appdata
    player.setAppdata({"found_first":true});
    // send message to profil_app with target and mode
    let message = {"profil_app":{"trigger":{
      "id": node.getAttribute("trigger")}}};
    main.send(message);
    LASTSENT = node.getAttribute("trigger");
    console.log(message);
      // clean document and remove eventlistener
    //node.className = "found";
    }

  });
}

// öffnet und schließt den CHAT auf Mausclick
initChatBtn = function(){
  document.getElementById(CONSTANTS.chatContainer).style.display = "none";

  document.getElementById(CONSTANTS.chatBtn).addEventListener("click", function(){
  $(this).toggleClass('A_bounce');
  let chat = document.getElementById(CONSTANTS.chatContainer);
  chat.style.display = chat.style.display === "none" ? "table-cell" : "none";
  });

}
