jq = require("jquery");
swal = require("sweetalert")

require('howler');
const SOUNDS = {
  'komplett': '../../CONTENT/level3_sounds/playground/kette_komplett.wav',
	'button': '../../CONTENT/level3_sounds/playground/okbutton_push.wav',
	'erscheint': '../../CONTENT/level3_sounds/playground/tile_erscheint.wav',
	'match': '../../CONTENT/level3_sounds/playground/tile_match.wav'
}


var task;
var team;

main.init = function(myappdata, language)
{
	console.log("language " + language);
}
// Wird nur jedes x-te mal aufgerufen wenn innerhalb init
player.get(["team", "playground"], begin);

main.receive = function(mydata)
{
	console.log("Received " + mydata);

	if ("swal" in mydata)
	{
		//eval(mydata.eval);
		console.log(mydata);
		if (mydata.swal.level == "BEFEHL" || mydata.swal.level == "befehl") {
		  showSwalLevel3(mydata.swal.level);
		} else {
      showSwal(mydata.swal.level);
    }
		playAudio(SOUNDS.komplett, {volume: .5});
	}

  if("eval" in mydata){
    eval(mydata.eval);
  }

}

// zeigt Pop-Up wenn Beweiskette vollständig und trägt Entscheidung in DB ein
function showSwalLevel3(_level)
{
	swal({
		title: "Beweiskette vollständig!",
		buttons: {
      ok:{
        text: "Jetzt der Rede hinzufügen",
        value: "ok"
      },
      ne:{
        text: "Nicht der Rede hinzufügen",
        value: "ne"
      }
		},
		closeOnClickOutside: false,
		closeOnEsc: false,
		text: "Die manipulierten Daten ergeben eine sinnvolle Beweiskette!"
	}).then(function(res) {
    playAudio(SOUNDS.button, {volume: .5});
		if (res === "ok") {
			// Eintrag in DB, ob Beweiskette veröffentlicht
			let obj = {};
			obj[_level] = 'confirmed';
			player.setAppdata(obj);
			swal({
				button: false,
				text:"Eure Stimme wurde gezählt. Jetzt warten wir nur noch auf das Votum eurer Kolleg*innen!",
				closeOnClickOutside: false,
				closeOnEsc: false
			});
      console.log("Swal Close confirmed.");
		}
		else if (res === "ne"){
      let obj = {};
      obj[_level] = 'denied';
      player.setAppdata(obj);
      swal({
        button: false,
        text:"Eure Stimme wurde gezählt. Jetzt warten wir nur noch auf das Votum eurer Kolleg*innen!",
        closeOnClickOutside: false,
        closeOnEsc: false
      });
			console.log("Swal Close verneint.");
		}
    else {
      console.log("Swal Close extern.");
    }
	});
}

function showSwal(_level)
{
	swal({
		title: "Beweiskette vollständig!",
		buttons: {
      ok:{
        text: "Jetzt der Rede hinzufügen",
        value: "ok"
      }
		},
		closeOnClickOutside: false,
		closeOnEsc: false,
		text: "Perfekt! Die gesammelten Daten eures Teams ergeben eine sinnvolle Beweiskette!"
	}).then(function(res) {
		if (res === "ok") {
			// Eintrag in DB, ob Beweiskette veröffentlicht
			let obj = {};
			obj[_level] = 'confirmed';
			player.setAppdata(obj);
			playAudio(SOUNDS.button, {volume: .5});
			swal({
				button: false,
				text:"Alrighty. Jetzt warten wir nur noch auf die Bestätigung eurer Team Members!",
				closeOnClickOutside: false,
				closeOnEsc: false
			});
		}
		else {
			console.log("Swal Close extern.");
		}
	});
}

function begin(data)
{
	console.log("task: " + data.playground.task + ", team: " + data.team);
	task = data.playground.task;
	team = data.team;
	getSetup(data.team, setup);
	listen(data.team, update);
}

// Collect HTML Elements
var field = document.getElementById("field"); // practically the whole screen div
var board = document.getElementById("board"); // Background/Playground canvas
var board_ctx = board.getContext('2d');				// context element of board canvas

// Genearate Playground Board Grid using board_ctx context
var grid = new Grid(13, 8, board, true);

// List of all Tiles on Playground
var tiles = [];

// Some Test Tile Objects
var testtile = new Tile({"name":"container",
	"nodes":[{"name":"a", "place":[0, 0], "text":"Krisengespräch Schmidt & Schmidt"},{"name":"b", "place":[0,1], "text":"<key>12.10.</key> Container verschwunden"}],
	"player":"A",
	"endpos":{"x":3, "y":5},
	"match":["geschaefft"]
});

//testtile.show();
//testtile.apply();

var testtileB = new Tile({"name":"geschaefft",
	"nodes":[{"name":"b", "place":[0, 0], "text":"Ware ab <key>12.10.</key> lieferbar"},{"name":"b", "place":[0,1], "text":"Geschäfft mit <key>albanischer</key> Firma"}],
	"player":"B",
	"endpos":{"x":5, "y":5},
	"match":["container"]
});

//testtileB.show();
//testtileB.apply();

/*
* If there are Tiles in the DB from Beginning on. Display them
*/
function setup(data)
{
	buff = {};
	buff["old_val"] = {"tiles":[]};
	buff["new_val"] = data;
	update(buff);
}

/*
*  Listen for Changes in livehack status Table
*  If theres a new Tile or one went missing react accordingly
*/
function update(data)
{
	newtiles = compare(data["new_val"]["tiles"], data["old_val"]["tiles"]);
	removedtiles = compare(data["old_val"]["tiles"], data["new_val"]["tiles"])

	for (tile of newtiles)
	{
		// add and show newly added tile
		let newtile = new Tile(tile);
		newtile.show(field);
		playAudio(SOUNDS.erscheint, {volume: .5});

		tiles.push(newtile);
		checkMatches(newtile);

	}

	for (rem_tile of removedtiles)
	{
		for (tile of tiles)
		{
			if (rem_tile.name == tile.name)
			{
				// undisplay tile
				tile.hide(field);

				// remove tile from tiles
				let i = tiles.indexOf(tile);
				if(i != -1) {
					tiles.splice(i, 1);
				}
			}
		}
	}
}

/*
* returns items that are in array a and NOT in array b
*/
function compare(a, b)
{
	items = [];
	outerloop:
	for (var i = 0; i < a.length; i++)
	{
		for (var g = 0; g < b.length; g++)
		{
			if (a[i].name == b[g].name)
			{
				continue outerloop;
			}
		}
		items.push(a[i]);
	}

	return items;
}

/*
*  see if tile has a match with the existing tiles
*  if then apply it
*/
function checkMatches(newtile)
{
	let levelcount = {};
	for (tile of tiles)
	{
		if (levelcount[tile.level] == undefined)
		{
			levelcount[tile.level] = 0;
		}
		for(match of newtile.match)
		{
			if (match == tile.name)
			{
				newtile.apply();
				tile.apply();
				playAudio(SOUNDS.match, {volume: .5});
        player.score(Math.floor(500/16)); // TODO: mal ernsthaft über die Punktevergabe nachdenken
			}
		}
		if (tile.status == 'applied') {
			levelcount[tile.level] ++;
			//console.log(newtile.name + " "+ levelcount[tile.level]);
		}
	}

	if(task == "server")
	{
		console.log(levelcount);
		let status = {status:{}};
		status.status = levelcount
		updateLivehack(status);
	}

}

// function um audio objekte zu erzeugen
function playAudio(_file, _options){
  let defaults = {
    src: [_file],
    autoplay: true,
    loop: false,
    volume: 1,
    onend: function() {
    },
    onplay: function() {
    },
    onload: function(){
    }
  };
  return new Howl(Object.assign({}, defaults, _options));
}
