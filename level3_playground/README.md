Level 3 Board
=============

Gemeinsames Spielfeld für Level 3 Hacking Game.

Auf 5 Laptops synchronisiert pro Team. Ggf. noch eine Ansicht auf Projektor oder größerem Monitor.

ACHTUNG: TCP Port des Apphandlers ist undynamisch auf 8002 gesetzt.

Beschreibung
------------

Jedes Live_hack Game (eins pro team) hat change feed auf sein Tabellenobjekt in Livehack tabelle (status DB) aboniert.

Wird ein tile der Liste hinzugefügt, wird es nach den Properties des Tiles angelegt. Player spezifische Eigenschaften werden von inventar bei erstellen oder weiterleiten des Tiles von player Status Datenbank hinzugefügt.


###Inventar

Zugriff auf livehack tabelle:

putTile(tileJSobject)

removeTile(ip, callback)
- callback returns tileJSobject that has `player=="ip" && status=="pending"`

###Board

changefeed auf Objekt

##Cue

Im Cue wird über die jeweilige properts (inventory oder playground) property im Player Objekt ausgesucht, welcher laptop welche apps öffnet.

alle Spieler mit playground öffnen board app

alle spieler mit inventory öffnen inventory und file_explorer app

##Player

Eintrag in Player Datenbank:

```javascript
playground: {
	task:"server" || "client"
}
```

EIN Spieler MUSS als server gesetzt werden. Dieser darf natürlich nie aussortiert werden!