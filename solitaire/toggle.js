main.init = function(appdata, language)
{

}

let visible = false;

main.receive = function(msg)
{
    if ("toggle" in msg)
    {
      if (!visible)
      {
        visible = true;
        main.send(  {"WINDOW":["show()"]}   )
      }
      else
      {
        visible = false;
        main.send(  {"WINDOW":["minimize()"]}   )
      }
    }
}
