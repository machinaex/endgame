ENDGAME PROJECT
===============

Setup
-----

###DIMMER ANSCHALTEN

###Router anschalten

###NAS anschalten

###Cory anschalten

------ ANFANG AUTOMATION auf cory

####Rethink DB Server starten

`$ rethinkdb --bind all -d /Users/Cory/Documents/Endgame/Database/rethinkdb_data`

####Framework starten

`$ cd /Users/Cory/Documents/Endgame/Apps/framework_server/`
`$ python framework.py`

####Max Control starten

`$ open -a Max /Users/Cory/Documents/Endgame/Framework/endgame_control.maxpat`

Reload klicken

####Cue control starten

`$ cd /Users/Cory/Documents/Endgame/Apps/control/`  
`$ npm start`  

------ ENDE AUTOMATION

####D Pro 2U

show Control �ffnen

####Max Control

Reload klicken

###Ableton

Soundkarte starten
Soundkarte Input/Output �berpr�fen

###Projektor anschalten

###Ben

`cd Documents/endgame/thewall`  
`npm start debug`  

`cd Documents/endgame/ipadhack_v41`  
`sh startIpadHack.sh`  

Firefox auf Vollbildmodus

auf zwei Bildschirme verteilen (Pr�sentation links, ipadhack rechts) 

Auf Pr�sentation gehen

NAS-Mounting �berpr�fen


### Nas-Mounting der Laptops �berpr�fen

- csshx �ffnen (im git)
- im nas einloggen und zum test ordner in temp anlegen
- im csshx den tempordner lsen udn schauen, ob der testordner angezeigt wird.

### Handy von "Lisa" �berpr�fen
- Programm auf Handy �ffnen und rumklicken
- checken, obs im Control auf Cory ankommt


Reset
------

###Cory
- cue RESET
- warten auf positives Feedback
- RELOAD_ALL
- LOGIN_SHOW (CONTROL)
- alle laptops �berpr�fen ob Kamera funktioniert. Sonst reload (ctrl + r)
- dann wieder auf EINLASS (START)

###Ben
- �berpr�fen ob NAS gemountet
- thewall reload [cmd]+[r]


Karten rausnehmen bei nicht voller Spielerzahl
------

###ReQL
- unter "status -> player" Eintr�ge l�schen
- zuerst die 5er, dann die 7er, dann die 3er



Development
-----------

####Apps erstellen

Erstellen und aktualisieren von Apps siehe [boilerplate](https://bitbucket.org/machinaex/endgame/src/c61af28bed3cda8074b3e9f7bf3892608a94ed90/boilerplate/?at=master).


####Framework bearbeiten

Zum erstellen und bearbeiten von Cues und Spieler Objekten siehe [framework_server](https://bitbucket.org/machinaex/endgame/src/c61af28bed3cda8074b3e9f7bf3892608a94ed90/framework_server/?at=master).

Mit dem endgame [editor](https://bitbucket.org/machinaex/endgame/src/c61af28bed3cda8074b3e9f7bf3892608a94ed90/editor/?at=master) k�nnen Cues und Spieler Objekte bearbeitet werden.

####Betriebssystem aufsetzen

Zum aufsetzen und verwalten des Endgame OS siehe [betriebssystem](https://bitbucket.org/machinaex/endgame/src/c61af28bed3c/betriebssystem/?at=master).

####ssh control f�r multiple Rechner gleichzeitg

2 M�glichkeiten um mehrere Rechner gleichzeitig via ssh zu bedienen:
a) [pssh](https://linux.die.net/man/1/pssh) --> ein python based (und damit MacOS f�higer Wrapper f�r bash befehle die an mehrere Rechner gehen. Nachteil: funktioniert nur mit gut vorbereiteten befehlen und gibt nur sehr basic feedback. Vorteil: sehr �bersichtlich.

f�r pssh hier ein paar Rezepte:

reboot all hosts in pssh_hosts:

`pssh -h pssh_hosts -l root -A "reboot"`

close all hosts:

`pssh -h pssh_hosts -l root -A "shutdown -h now"`

do whatever and get back the stdout:

`pssh -h pssh_hosts -l <username> -A -i "<bash command of your choice>"`

Remember though: lots of commands need root as user.
because of this the PermitRootLogin flag in /etc/ssh/sshd_config has to be checked to 'yes' (if password shall be used) or at least to 'without-password' (if keyaccess)

b) [cssh](https://linux.die.net/man/1/cssh) --> linux bash app die mehrere fenster mit ssh verbindung �ffnet und durch eine gemeinsame texteingabe parallel bedient. Vorteil: Super m�chtig und gute �bersicht �ber feedback der einzelnen Rechner. Nachteil: nur linux und visuell einnehmend.

f�r cssh: file mit den anzusteuernden Ips (sogennantes cluster file): s. dazu [controltools README.md](https://bitbucket.org/machinaex/endgame/src/ed302c162e7ea08122a6e73b43b9a7134840f98f/controltools/?at=master)
