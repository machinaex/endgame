$ = require("jquery");

fs = require('fs');
sys = require('util');

swal = require("sweetalert");

const contentlink = "./content/";
const outputlink = "../../login_temp/"
const outputlinkNAS = "../../TEMP/"
var playerName;

var languageChoosen = "de";

const masterpw = "arduino2010";

require("howler");

var failsound = new Howl({src: [contentlink+'fail.wav']});

var winsound = new Howl({src: [contentlink+'win.wav']});

document.addEventListener('auxclick', callback, false);
  function callback(e) {
    console.log('not left mouse click');
    e.preventDefault();
    return
  }

main.init = function(myappdata, language)
{

}

main.receive = function(mydata)
{

}

var playerFileName = "iconWithNoName.png";

player.get(["name"], function(obj){
		document.getElementById("user-name").innerHTML = obj.name;
		playerFileName = obj.name.replace(" ","_")+".png";
		document.getElementById("user-profile-pic").src = contentlink + obj.name.replace(" ","_")+".png";
		console.log(obj);
		console.log(playerFileName);
})


var lans = document.getElementsByClassName("lan");

for (var i = 0; i < lans.length; i++) {

    lans[i].addEventListener('click', function(e){

    	console.log(e.target.innerText);
    	languageChoosen = e.target.innerText.substring(0,2);
    	document.getElementById("droplist").style.display = "none";

    	document.getElementById("language_btn").innerHTML = languageChoosen;
    	document.getElementById("inp_pw").focus();

	}, false);
}

var lan_btn = document.getElementById("language_btn");

lan_btn.addEventListener("mouseover", function(){
		console.log("hover!");
		document.getElementById("droplist").style.display = "block";
});

document.body.addEventListener("click", function(){
	//hide dropdown if open:
    document.getElementById("droplist").style.display = "none";
})

document.getElementById("inp_pw").addEventListener("keypress", function(e){

	if (e.key == "Enter")
	{
		//console.log("enter!(key)");
		passwordEntered(this.value)
	}

});

document.getElementById("enter").addEventListener("click",function(){

	//console.log("enter!")
	this.blur();

	let inp = document.getElementById("inp_pw")
	passwordEntered(inp.value);

});

function didUphoto(){

	swal("Ubercrowdie!","Hast du schon ein Foto gemacht?", {
	  buttons: ["Nein!", "Na klar!"],
	}).then(function(res){
		console.log(res);
		if (!res)
		{

			swal("Kein Problem.\nKlicke gleich einfach auf das Livebild von dir um ein Photo zu machen!",{timer: 4000})

			if(snapshotExists)
	      	{
		      	videoOn();
		      	$(".screenshotprofile").remove();
	      	}


		}
		else if (!snapshotExists)
		{
			swal("Lüge!\n Du hast noch kein Photo gemacht!","Klicke auf das Livebild von dir um ein Photo zu machen. So einfach ist das!",{timer: 4000})

		}
		else
		{

			// write language choosen into DB:
			player.connect( function(connection)
		      {
		        r.db("status").table("player").get(myip.address()).update({"language":languageChoosen}).run(connection, function(err, result){
		          player.getResult(err, result, function(data) {
		          	console.log(data);
		          }, connection)
		        }.bind(player)
		        );
		      }.bind(player)
		    );

			vidOff();
			winsound.play();
			setTimeout(function(){
				main.send({"WINDOW": ["setFullScreen(false)"]})} , 400);
			setTimeout(function(){
				main.send({"WINDOW": ["hide()"]})} , 1600);
			/*setTimeout(function(){
				main.send({"WINDOW": ["close()"]})} , 5000);//*/
		}

	});


}


function passwordEntered(pw)
{
	var inp = document.getElementById("inp_pw");

	inp.classList.remove("shake");
	inp.classList.remove("hop");

	player.get(["login"], function(res){
		res = res.login;

		console.log("checking password: "+ pw + " against: "+ res.password);

		if (res.password == pw || pw == masterpw)
		{
			console.log("CORRECT!");

			inp.blur();
			inp.classList.add("hop");


			didUphoto();


		}
		else
		{
			console.log("WRONG");
			failsound.play();
			inp.classList.add("shake");
			inp.focus();
			inp.select();

		}

	});

}



//'use strict';
    var video = document.querySelector('video')
      , canvas;

    /**
     *  generates a still frame image from the stream in the <video>
     *  appends the image to the <body>
     */

    let snapshotExists = false;

    function takeSnapshot() {

      if(snapshotExists)
      {

      	videoOn();
      	$(".screenshotprofile").remove();


      }
      else
      {

	      var img = document.querySelector('img') || document.createElement('img');
	      var context;
	      var width = video.offsetWidth
	        , height = video.offsetHeight;

	      canvas = canvas || document.createElement('canvas');
	      canvas.width = width;
	      canvas.height = height;

	      context = canvas.getContext('2d');
	      context.drawImage(video, 0, 0, width, height);

	      img.src = canvas.toDataURL('image/png');
	      img.className += " screenshotprofile "
	      document.getElementById("user-profile-pic").appendChild(img);
	      $("#videostream").hide();
	      downloadCanvas( playerFileName );

      }


    }

    function downloadCanvas(filename) {
	    bild = canvas.toDataURL();
		// strip off the data: url prefix to get just the base64-encoded bytes
		var data = bild.replace(/^data:image\/\w+;base64,/, "");
		var buf = new Buffer(data, 'base64');
		fs.writeFile(outputlink+ filename, buf, function(){console.log("made profile file! at "+outputlink+ filename)});
		try{
			fs.writeFile(outputlinkNAS+ filename, buf, function(){console.log("made profile file! at "+outputlinkNAS+ filename)});
		}
		catch(e)
		{console.log("could not write into NAS");console.log(e)}
		snapshotExists	= true;
	}

	$("#user-profile-pic").click(takeSnapshot);


	function videoOn()
	{
		$("#videostream").show();
		snapshotExists = false;

	}


    function vidOff() {
    video.pause();
    video.src = "";
    localstream.getTracks()[0].stop();
	}

	var localstream;
    // use MediaDevices API
    // docs: https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
    if (navigator.mediaDevices) {
      // access the web cam
      navigator.mediaDevices.getUserMedia({video: true})
      // permission granted:
        .then(function(stream) {
          localstream = stream;
          video.src = window.URL.createObjectURL(stream);
        })
        // permission denied:
        .catch(function(error) {
          document.body.textContent = 'Could not access the camera. Error: ' + error.name;
        });
    }
