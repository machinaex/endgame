*Boilerplate*
=========

Basis Code für alle machina ENDGAME Electron Apps.

* [Aufsetzen](#markdown-header-aufsetzen)

* [Aktualisieren](#markdown-header-aktualisieren)

* [Benutzen](#markdown-header-benutzen)

	* [debug](#markdown-header-debug)

	* [main](#markdown-header-main)

	* [player](#markdown-header-player)

	* [API](#markdown-header-api)

--------------------------------------------------------------

Aufsetzen
==========

Den Boilerplate Ordner Kopieren und umbennen.

Die folgende Dateien sollten nun teil des App Projektes sein. Anpassungen in
*config.json* und *package.json* sind obligatorisch. 


###config.json
im file **config.json** muss definiert werden:

> "name": "<appname>"  
> "udpPort": "<port>"  
> "tcpPort": "<port>"  

*optional:*
> WINDOW:{<initialisierungsparameter für das BrowserWindow>}

wenn kein WINDOW in der config angegeben ist, wird das Window mittig mit Größe 800x800 initialisiert.

###package.json
das `"name"` Attribut muss in den Appnamen geändert werden.


###index.html
Wenn weitere script dateien hinugefügt werden, diese unterhalb der *boilerplate.js* anlegen:
```html

	<!-- do not change this line: -->
	<script type="text/javascript" src="boilerplate.js"></script>
	<!-- add your own java script files here: -->
	<script type="text/javascript" src="my_app.js"></script>
	<script type="text/javascript" src="more_app.js"></script>

```

###app.js
Beispielskript, das bereits in *index.html* eingebunden ist.

###boilerplate.js

wenn DEBUG variable in boilerplate.js true gesetzt ist, ist die app immer closable und die Konsole kann geöffnet werden:

```javascript
var net = require('net'); // for tcp
var dgram = require('dgram'); // for udp

var DEBUG = false; // set true when in testing mode
```

Ansonsten sollten hier keine Änderungen vorgenommen werden. Datei wird mit neuer Version überschrieben.

###main.js
hier sollten keine Änderungen vorgenommen werden. Datei wird mit neuer Version überschrieben.

Aktualisieren
==============

*boilerplate.js* und *main.js* sollten mit jedem Update der Boilerplate in jeder App durch die neue Version ersetzt werden.

Benutzen
=========

Nützliche Funktionen, Funktionsaufrufe und Variablen, die in *boileplate.js* angelegt sind.

main
----------

Erlaubt Zugriff auf wichtige Funktionen, Events und Variablen in boilerplate.js, main.js und für die kommunikation mit dem app_handler.

####config object
`main.config.<value>`  
JS Objekt, das die config.js daten enthält. (siehe oben)

Bsp.:
```javascript
console.log("app name is: " + main.config.name + ". udp port is: " + main.config.udpport + ".");
```


####init function call
`main.init = function(<appdata>, <language>){}`  
Wird nach Login Prozess in login App oder bei Neustart der App aufgerufen, wenn der Login Prozess bereits abgeschlossen ist. 
appdata ist `undefined` wenn app nicht in Spieler Objekt gefunden wird. 
language ist **'en'** oder **'de'**

Bsp.:
```javascript
main.init = function(myappdata, mylanguage)
{
  console.log("init with appdata: " + myappdata + " and language: " + mylanguage);
}
```


####receive function call
`main.receive = function(<appaction>){}`  
Wird aufgerufen, wenn app_handler Daten an App schickt die etwa durch einen cue oder eine andere lokale App gesendet wurden.

```javascript
main.receive = function(msg)
{
	console.log("new instruction: " + JSON.stringify(msg));
}
```


####send function
`main.send(<data>)`  
Versendet data an app_handler. Kann WINDOW Property enthalten um eigene Fenstereigenschaften anzupassen.

####send TCP function
`main.sendTCP(<url>, <port>, <message>)`  
TCP message versenden

####send UDP function
`main.sendUDP(<url>, <port>, <message>)`  
UDP message versenden


player
----------

Erlaubt Zugriff auf das dem Laptop zugeordnete Player Objekt in der Datenbank.

####set Appdata function
`player.setAppdata(<jsobject>)`  
erwartet jsobject mit Appdata. Ersetzt App spezifische appdata mit neuem jsobject.

####get Appdata function
`player.getAppdata(<callback>)`  
callback Funktion wird aufgerufen wenn Appdaten aus Datenbank geholt wurden. Callback übergibt appdaten.

Bsp.:
```javascript
player.getAppdata(refreshAppdata)

function refreshAppdata(recentAppdata)
{
	myappdata = recentAppdata;
	console.log(JSON.stringify(myappdata))
}
```

####add Attribute function
`player.addAttribute(<new_attribute>)`  
Fügt der attributes Liste in Spieler Objekt, wenn noch nicht vorhanden, neues Attribut hinzu.

####remove Attribute function
`player.removeAttribute(<some_attribute>)`  
Entfernt, wenn vorhanden, existierendes Attribut aus attributes Liste im Spieler Objekt.

####get function
`player.get([<list of properties>], <callback>)`  
Zeilen aus Player holen. Benötigt Liste von Strings und callback Funktion.

Bsp.:
```javascript
player.get(["score", "attributes"], myfunction);

function myfunction(properties)
{
	console.log("Score is: " + properties.score + " current Attributes are: " + properties.attributes);
}
```



API
----------

*boilerplate.js* und *main.js* filtern Nachrichten an die App nach keywords, die die basisfunktionen betreffen.

####Window property
ist optional: Hier kann als String notiert jede Methode aus github.com/electron/electron/blob/master/docs/api/browser-window.md eingefügt werden um das Fenster zu manipulieren.

Bsp:
`{"WINDOW":["setPosition(0,0)","focus()"]}` : Verschiebt das App Fenster an die Position 0,0 und fokusiert es.
oder
`{"WINDOW": ["focus()","minimize()"]}`

####Screensaver property
`"SCREENSAVER":{}`  
ist optional und funktioniert entweder mit einfachen boolschen wie:  
`"SCREENSAVER":true` (Schwarzer Screen über App) oder  
`"SCREENSAVER":false` (Screensaver aus)  

oder mit Parametern:  
 `"SCREENSAVER":{"image":<linktoimage>, "opacity":<0. - 1.>, "html":<htmlstring>}`  
 alle parameter sind optional. Anwesenheit eines Parameters wird als *true* Befehl zum Anschalten des Screensavers interpretiert.  
 Ein leeres Objekt `"SCREENSAVER:{}` wird interpretiert wie *false* (Screensaver aus)

####Boilerplate property
`"Boilerplate":"init"`  
Sorgt für aufruf der Main.init funktion in boilerplate.js.