Level 3 Inventory
=================

Info Item Inventar, das durch durchsuchen des Fileexplorers gefüllt wird und in dem items für den gemeinsamen *Playground* freigegeben werden können.

Bekommt Nachricht von *Playground* Server Player wenn Tile applied wurde.

##Player

Eintrag in Player Datenbank Bsp.:

```javascript
"inventory": {
  "style": "playerA"
},
```

##Tile Input

####Beispiel 1:
```json
"inventory": {
	{
		"newTile":
		{
		  "endpos": {
		    "x": 3,
		    "y": 5
		  },
		  "match": [
		    "geschaefft"
		  ],
		  "name": "container",
		  "nodes": [
		    {
		      "name": "a",
		      "place": [
		        0,
		        0
		      ],
		      "text": "Krisengespräch Schmidt & Schmidt"
		    },
		    {
		      "name": "b",
		      "place": [
		        0,
		        1
		      ],
		      "text": "<key>12.10.</key> Container verschwunden"
		    }
		  ]
		}
	}
}
```

####Beispiel 2:
```json
"inventory": {
	{
		"newTile":
		{
      "endpos": {
        "x": 5,
        "y": 5
      },
      "match": [
        "container", "transport"
      ],
      "name": "geschaefft",
      "nodes": [
        {
          "name": "b",
          "place": [
            0,
            0
          ],
          "text": "Ware ab <key>12.10.</key> lieferbar"
        },
        {
          "name": "b",
          "place": [
            0,
            1
          ],
          "text": "Geschäfft mit <key>albanischer</key> Firma"
        }
      ]
    }
	}
}


{
	"endpos": {
	  "x": 6,
	  "y": 4
	},
	"match": [
	  "geschaefft", "spendenquittung"
	],
	"name": "transport",
	"nodes": [
	  {
	    "name": "a",
	    "text": "Privattransport von <key>Albanien</key>"
	  },
	  {
	    "name": "b",
	    "text": "Koffer mit <key>2.000.000€</key>"
	  }
	]
}

{
	"endpos": {
	  "x": 8,
	  "y": 4
	},
	"match": [
	  "transport", "kneipengespraech"
	],
	"name": "spendenquittung",
	"nodes": [
	  {
	    "name": "a",
	    "text": "Betrag: <key>2.000.000€<key>"
	  },
	  {
	    "name": "b",
	    "text": "Spendenquittung für <key>Thomas</key>"
	  }
	]
}

{
	"endpos": {
	  "x": 9,
	  "y": 3
	},
	"match": [
	  "spendenquittung", "karl"
	],
	"name": "kneipengespraech",
	"nodes": [
	  {
	    "name": "a",
	    "text": "<key>Thomas</key> bekommt unverdient Anerkennung"
	  },
	  {
	    "name": "b",
	    "text": "Uwe mit <key>Karl</key> im Kneipengespräch"
	  }
	]
}

{
	"endpos": {
	  "x": 10,
	  "y": 2
	},
	"match": [
	  "kneipengespraech"
	],
	"name": "karl",
	"nodes": [
	  {
	    "name": "a",
	    "text": "<key>Karl</key>"
	  }
	]
}
```