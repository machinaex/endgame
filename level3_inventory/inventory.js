jq = require("jquery");
const swal = require('sweetalert');

require('howler');
const SOUNDS = {
  'erhalten': '../../CONTENT/level3_sounds/inventory/tile_erhalten.wav'
}

//////////////////////////////////////////////////////////////////////////////
// quickfix um player dropfields in der Farbe zu ändern
// muss im board.js und(!) im CSS dann auch geändert werden!!!!!
//////////////////////////////////////////////////////////////////////////////
let BG = {
  'playerA': 'rgba(0,0,255,1)',
  'playerB': 'rgba(0,128,0,1)',
  'playerC': 'rgba(0,255,255,1)',
  'playerD': 'rgba(255,0,0,1)',
  'playerE': 'rgba(128,0,128,1)'
}
//////////////////////////////////////////////////////////////////////////////
// ende quickfix
//////////////////////////////////////////////////////////////////////////////

var tilesList = [];

// Collect HTML Elements
var field = document.getElementById("field"); // practically the whole screen div
var board = document.getElementById("board"); // Background/Playground canvas

// Create Grid
var gridwidth = 12;
var gridheight = 1;
var grid = new Grid(gridwidth, gridheight, board, false);

var dropspot = document.getElementById("dropspot");

var droprow = gridwidth-2;
var dropcol = gridheight-1;

// position dropspot
dropspot.style.left = grid.cells[droprow][dropcol].pos.x - parseFloat(jq(dropspot).css('padding'));
dropspot.style.top = grid.cells[droprow][dropcol].pos.y - parseFloat(jq(dropspot).css('padding'));
dropspot.style.height = grid.cell_size;
dropspot.style.width = grid.cell_size*2;

document.addEventListener('auxclick', callback, false);
  function callback(e) {
    console.log('not left mouse click');
    e.preventDefault();
    return
  }

jq('body').on('drop', function(event) { event.preventDefault(); });
//$('body').on('dragstart', function(event) { event.preventDefault(); });

// Allow Drop Tiles on dropspot
dropspot.addEventListener('drop', handleDrop, false);
dropspot.addEventListener('dragover', handleDragOver, false);
dropspot.addEventListener('dragleave', handleDragLeave, false);

var occupied = false;

var team = "";

player.get(["team"], setup);

function setup(data)
{
	team = data.team;
	console.log("Team: "+ team);
}

var playersettings = {};
let LEVEL;

main.init = function(data, language)
{
	LEVEL = data.level;
	console.log(data);
	playersettings = data;

  let color = 'white';
  PlayerStyles.forEach(function(item){
    if(item.player == data.style){color = item.background_color}
  });
  dropspot.style.backgroundColor = color;
}

main.receive = function(data)
{
  console.log(data);
	if (data.applied != undefined)
	{
		removeTile(data.applied);
	}

	if (data.newTile != undefined)
	{
		addTile(data.newTile);
	}

	if('level' in data)
	{
		LEVEL = data.level;
    for (var i = (tilesList.length)-1; i >= 0; i--) {
      removeTile(tilesList[i].name);
    }
	}
}

function removeTile(tilename)
{
  for (let tile of tilesList)
  {
    if(tile.name == tilename)
    {
      tile.hide(field);

      // remove tile from tiles
      let i = tilesList.indexOf(tile);
      if(i != -1) {
        tilesList.splice(i, 1);
      }

      unblockDropspot();
    }
  }

  // Andere Tiles in inventory aufruecken
  console.log(tilesList);
  for (let i = 0; i < tilesList.length; i++)
    {
      console.log(tilesList[i]);
      tilesList[i].enqueue(i*2, 0, field);
    }
}

function addTile(tileObject)
{
	//tileObject.player = {"ip":"", "startpos": {}, "style":""};
  tileObject.player = {"ip":"", "style":""};
	tileObject.player.ip = myip.address();
	//tileObject.player.startpos = playersettings.startpos;
	tileObject.player.style = playersettings.style;
	newtile = new Tile(tileObject);
  newtile.enqueue(tilesList.length*2, 0, field);
	tilesList.push(newtile);
	newtile.div.draggable = true;
	newtile.div.addEventListener('dragstart', handleDragStart, false);
	newtile.div.addEventListener('drop', handleDrop, false);
	newtile.div.addEventListener('dragend', handleDragEnd, false);

  playAudio(SOUNDS.erhalten, {volume: .5});
  player.score(200);
}

function handleDragStart(e) {
  jq(this).addClass("dragging");  // this / e.target is the source node.
  console.log("dragging " + this.id);
  e.dataTransfer.setData("tilename", this.id);

  /* If Tile is being removed from dropzone:
  *  unblock dropzone, revert dropzone style,
  *  prepare tile to be removed from shared screen if it is not being dropped on the zone again
  */
  for (let tile of tilesList)
  {
  	if (tile.name == this.id)
  	{
  		if(tile.status == "dropped")
  		{
  			tile.status = "pending";
  			unblockDropspot();
  		}
  	}
  }
}

function handleDragEnd(e) {

  console.log("drag ended " + this.id);

  for (let tile of tilesList)
  {
  	if (tile.name == this.id)
  	{
  		tile.div.className = tile.player.style;
  		if (tile.status == "pending")
  		{
  			tile.moveTo(tile.queuePos.row, tile.queuePos.col);
        unshareTile(team, tile.properties);
  		}
  	}
  }
}

function handleDrop(e) {
  // this / e.target is current target element.
  droptile = e.dataTransfer.getData("tilename");
  console.log("Dropped " + droptile);
  if (e.stopPropagation) {
    e.stopPropagation(); // stops the browser from redirecting.
  }

  /*
  * If Dropzone is free:
  * block dropzone, change look of dropzone, publish tile to shared Screen
  */
	if(!occupied)
	{
	for (let tile of tilesList)
	{
		if (tile.level != LEVEL && tile.name == droptile) {
			swal({
				text: "OFF TOPIC! Datei beinhaltet keine Informationen zum im Moment gesuchten Themenbereich.",
				button: false,
				timer: 2500//*/
			});
		}
		else if (tile.name == droptile)
		{
				jq(tile.div).removeClass("dragging");
				tile.moveTo(droprow, dropcol);
				tile.status = "dropped";
				occupied = true;
				jq(this).attr("class", "occupied");
				shareTile(team, tile.properties);
			}
		}
	}

  return false;
}

function handleDragOver(e) {
	if (e.preventDefault) {
    e.preventDefault(); // Necessary. Allows us to drop.
  }

  if (!occupied)
  {
  	jq(this).attr("class", "dragover");
  }
}

function handleDragLeave(e) {
	if (!occupied)
  {
  	jq(this).attr("class", "");
  }
}

/*
*
*/
function unblockDropspot()
{
  occupied = false;
  jq(dropspot).attr("class", "");
}


// function um audio objekte zu erzeugen
function playAudio(_file, _options){
  let defaults = {
    src: [_file],
    autoplay: true,
    loop: false,
    volume: 1,
    onend: function() {
    },
    onplay: function() {
    },
    onload: function(){
    }
  };
  return new Howl(Object.assign({}, defaults, _options));
}
