
const swal = require('sweetalert');
$ = require("jquery");
require("howler");

const fs = require('fs');
const dir = './bilder';

const totalpoints = 100;

let bilder;

// forbid drag and drop
$('body').on('dragstart', function(event) { event.preventDefault(); });

document.addEventListener('auxclick', callback, false);
  function callback(e) {
    console.log('not left mouse click');
    e.preventDefault();
    return
  }

fs.readdir(dir, (err, files) => {
	bilder = files;
	if (bilder.indexOf(".DS_Store") > -1) {
		console.log(bilder.indexOf(".DS_Store"))
	    bilder.splice(bilder.indexOf(".DS_Store"), 1);
	}
  	console.log(files);
  	console.log(files.length);
});


var win = new Howl({src: ['./win.mp3']});
var right = new Howl({src: ['./right.mp3']});
var wrong = new Howl({src: ['./wrong.mp3']});
var loose = new Howl({src: ['./loose.mp3']});


let counter = 0;
let correctInputs = 	["FALSCH","FALSCH","FALSCH","FALSCH","WAHR","WAHR","FALSCH","WAHR"];
let incorrectInputs = 	["WAHR","WAHR","WAHR","WAHR","FALSCH","FALSCH","WAHR","FALSCH"];

let correcttext = [
"Richtig, so heißt sie natürlich nicht. \"Die Begegnung\"? Das wäre ja viel zu kompliziert. Kann sich ja kein Mensch merken. Praktischerweise heißt die Bewegung einfach DIE BEWEGUNG.",
"Genau - es sind gerade mal 400. Aber die sind laut, schnell und gefährlich gutaussehend.",
"Du kennst dich wirklich gut mit der BEWEGUNG aus und weißt natürlich, dass er Markus Sintner heißt, 32 Jahre alt ist und einen B.A. in Philosophie hat.",
"Ganz genau! Das ist einfach nur ein Baum.",
"So ist es. Und es werden täglich mehr.",
"Richtig! Nach Eigenaussagen symbolisiert das Minuszeichen den Wunsch nach einem Europa \"ohne Überfremdung\".",
"Exakt. Die Farbe der BEWEGUNG ist orange. Wofür das steht, wissen wir allerdings auch nicht so genau.",
"Richtig! Das ist zumindest unser Ziel. Und mit DEINER Hilfe können wir es erreichen!"
]

let incorrecttext = [
"\"Die Begegnung\"? Das wäre ja viel zu kompliziert. Kann sich ja kein Mensch merken. Praktischerweise heißt die Bewegung einfach DIE BEWEGUNG.",
"Nein - es sind gerade mal 400. Aber die sind laut, schnell und gefährlich gutaussehend.",
"Nein, nein und nein. So einen alten Hernn kann DIE BEWEGUNG doch gar nicht gebrauchen! Richtig wäre gewesen: Markus Sintner, 32 Jahre alt, B.A. in Philosohie.",
"Nein. Das ist einfach nur ein Baum.",
"Nein, mehr sind es im Moment noch nicht. Weniger allerdings auch nicht. Und die Zahl steigt täglich.",
"Leider falsch! Nach Eigenaussagen symbolisiert das Minuszeichen den Wunsch nach einem Europa \"ohne Überfremdung\".",
"Ganz und gar nicht. Die Farbe der BEWEGUNG ist orange. Wofür das steht, wissen wir allerdings auch nicht so genau.",
"Warum so pessimistisch? Wer nicht kämpft, hat schon verloren."
]


main.init = function(myappdata, language)
{
	//correctInputs = myappdata.correctinput;
	//incorrectInputs = myappdata.incorrectinput;
	$("#correct_btn").html(correctInputs[counter]);
	$("#incorrect_btn").html(incorrectInputs[counter]);
	$("#flashcard").attr("src","./bilder/"+bilder[counter]);

}

console.log("should remove now its thingis4");

main.receive = function(mydata)
{
	if ("active" in mydata)
	{
		if (mydata.active == true)
		{
			main.send(
				{
					"questbook":
					{
						"command" : "checkbox",
						"tags" : ["level0 level0_alldone"],
						"content" : ["Beantworte alle Fragen!"]
					}

				}
			);
		}
		else if (mydata.active == false)
	    {
	      console.log("got deactivate!!!");

	      main.send(
	        {
	          "questbook":
	          {
	            "command" : "remove",
	            "tags" : "level0"
	          }

	        }
	      );
	    }
	}
}


$("#correct_btn").click(function(){inputclicked(this.innerHTML)});
$("#incorrect_btn").click(function(){inputclicked(this.innerHTML)});
document.addEventListener("keydown",function(ev){
	if (ev.key =="ArrowRight")
	{
		$("#incorrect_btn").click();
	}
	else if (ev.key =="ArrowLeft")
	{
		$("#correct_btn").click();
	}
})

function inputclicked(input)
{
	//console.log(counter);
	console.log(correctInputs[counter]);
	// check if input in list of correct inputs;

	if (correctInputs[counter] == input)
	{
		right.play();
		player.score(totalpoints/correctInputs.length);
		swal("Good job!", correcttext[counter], "success").then(function(){checkEnd();});

	}
	else
	{
		//swal("WTF!?", "You clicked "+input, "info");
		wrong.play();
		player.score(-(totalpoints/correctInputs.length));
		swal("WTF!?", incorrecttext[counter], "error").then(function(){checkEnd();});

	}


}

function checkEnd()
{
	counter++;
	console.log(counter);
	console.log(correctInputs.length);
	if (counter == correctInputs.length)
	{
		win.play();
		swal("YEEEEEEEEAHHHHH!", "!!!You TOTALLY FINISHED YOUR TASK!!!! ", "success",{timer: 4500});

		main.send(
			{
				"questbook":
				{
					"command" : "set",
					"tags" : "level0_alldone",
					"status" : "checked"
				}

			}
		);

		player.addAttribute("level0_done");

		// end game or repeat (because we don't know shit yet.)
		counter = 0;
		$("#flashcard").attr("src","./ubercrowdlogo_big.png");
		$("#correct_btn").hide()
		$("#incorrect_btn").hide();
		player.get(["score"],function(res){
			console.log(res);
			$("#container_flash").append("<div id=\"endscore\"><h1>U ARE UBERAWESEOME!!!</h1>YOUR SCORE IS: <br>"+res.score);
		})
	}
	else
	{
		$("#flashcard").attr("src","./bilder/"+bilder[counter]);
		let leftisright = Math.floor((Math.random() * 2) + 0);
		console.log("leftis: "+leftisright);
		if (leftisright){
			$("#correct_btn").html(correctInputs[counter]);
			$("#incorrect_btn").html(incorrectInputs[counter]);
		}
		else
		{
			$("#incorrect_btn").html(correctInputs[counter]);
			$("#correct_btn").html(incorrectInputs[counter]);
		}

	}

}
