$ = require("jquery");
require("howler");

var drumbeat = new Howl({
  src: ['./drumbeat.wav']
});

var ping = new Howl({
  src: ['./ping.wav']
});

var log = new Howl({
  src: ['./logged.wav']
});



main.init = function(myappdata, language)
{
	
}

main.receive = function(mydata)
{
	let sound;
	let time;
	if (mydata.sound != undefined)
	{
		sound = mydata.sound;
		if (sound == "ping") {sound = ping;}
		if (sound == "log")  {sound = log;}
		if (sound == "drumbeat")  {sound = drumbeat;}
	}
	else
	{ sound = drumbeat; }

	if (mydata.color != undefined)
	{ $("#colorbox").css("background-color", "rgba("+mydata.color+",1"); }
	else
	{ $("#colorbox").css("background-color", "white"); }
	
	if (mydata.time != undefined)
	{ time = mydata.time;}
	else
	{ time = 200;}

	sound.play();
	$("#colorbox").fadeIn(time,function(){$("#colorbox").fadeOut(time);});


}



document.addEventListener('keypress', (event) => {
	 const key = event.key;
	 console.log(key);
	if (key == "l"){main.receive({"sound":"ping", "color":"0,255,255"})}
	if (key == "k"){main.receive({"sound":"log", "color":"255,0,255"})}
	if (key == "a"){main.receive({"color":"255,0,0"})}
	if (key == "s"){main.receive({"color":"255,255,0"})}
	if (key == " "){main.receive({"color":"255,255,255"})}
	if (key == "Enter"){main.receive({"color":"255,255,255", "time":1000})}
});


